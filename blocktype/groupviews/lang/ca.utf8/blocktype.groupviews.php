<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-groupinfo
 * @author     Liip AG
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2010 Liip AG http://www.liip.ch
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */

defined('INTERNAL') || die();

$string['title'] = 'Pàgines del Grup';
$string['description'] = 'Mostra les pàgines relacionades amb el  grup';
$string['displaygroupviews'] = 'Mostra pàgines del grup';
$string['displaygroupviewsdesc'] = 'Pàgines del Grup - pàgines creades dins del grup';
$string['displaysharedviews'] = 'Mostra pàgines compartides';
$string['displaysharedviewsdesc'] = 'Pàgines compartides -pàgines compartides pels membres del grup des del seu portafolis individual.';
$string['defaulttitledescription'] = 'Si deixeu el camp Títol en blanc es generarà un títol per defecte.';
