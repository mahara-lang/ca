<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-externalvideo
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana: Joan Queralt Gil jqueralt a gmail.com
 */

defined('INTERNAL') || die();

$string['title'] = ' Vídeo extern';
$string['description'] = 'Incrusta vídeos externs';
$string['videourl'] = 'Adreça URL del vídeo';
$string['videourldescription2'] = 'Enganxeu el codi d\'incrustació (<em>embed</em>) o la URL de la pàgina on hi ha el vídeo. <b>Només</b> podeu incrustar vídeos dels següents llocs:';
$string['width'] = 'Amplada';
$string['height'] = 'Alçada';
$string['invalidurl'] = 'URL no vàlida';
?>
