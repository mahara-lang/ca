<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-externalfeeds
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana: Joan Queralt Gil jqueralt a gmail.com
 */

defined('INTERNAL') || die();

$string['title'] = 'Alimentació RSS externa';
$string['description'] = 'Incrusta una alimentació RSS o ATOM externa';
$string['feedlocation'] = 'Adreça de l\'alimentació';
$string['feedlocationdesc'] = 'URL d\'una alimentació RSS o ATOM vàlida';
$string['showfeeditemsinfull'] = 'Voleu mostrar els ítems sencers?';
$string['itemstoshow'] = 'Ítems per mostrar';
$string['itemstoshowdescription'] = 'Entre 1 i 20';
$string['showfeeditemsinfulldesc'] = 'Marqueu per veure el resum i la descripció de cada ítem. El resultat, però, dependrà de com està construïda l\'alimentació en origen';
$string['invalidurl'] = 'La URL no és vàlida. Només podeu veure alimentacions d\'URLs amb http i https.';
$string['invalidfeed'] = 'Sembla que l\'alimentació no és vàlida. L\'error és: %s';
$string['lastupdatedon'] = 'Darrera actualització %s';
$string['defaulttitledescription'] = 'Si ho deixeu en blanc es mostrarà el títol de l\'alimentació.';
?>
