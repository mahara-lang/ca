<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Liip AG, and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-groupmembers
 * @author     Liip AG
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Liip AG, http://liip.ch
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */

defined('INTERNAL') || die();

$string['title'] = 'Membres del Grup';
$string['description'] = 'Mostra la llista de membres d\'aquest  Grup';
$string['show_all'] = 'Mostra tots el membres d\'aquest  Grup';

$string['options_numtoshow_title'] = 'Mostra els members';
$string['options_numtoshow_desc'] = 'Nombre de membres que voleu mostrar.';

$string['options_order_title'] = 'Ordre';
$string['options_order_desc'] = 'Podeu mostrar els darrers membres del grup o una selecció aleatòria.';

$string['Latest'] = 'Darrers';
$string['Random'] = 'Aleatori';

$string['defaulttitledescription'] = 'Si deixeu el camp del Títol en blanc se\'n generarà un per defecte';
