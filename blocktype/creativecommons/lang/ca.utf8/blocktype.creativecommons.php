<?php
/**
 * Creative Commons License Block type for Mahara
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-creativecommons
 * @author     Francois Marier <francois@catalyst.net.nz>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2009 Catalyst IT Ltd
 * @traducció catalana: Joan Queralt Gil jqueralt a gmail.com
 */

defined('INTERNAL') || die();

$string['title'] = 'Llicència Creative Commons';
$string['description'] = 'Associeu a la vostra Pàgina una llicència Creative Commons';
$string['blockcontent'] = 'Bloc de contingut';

$string['alttext'] = 'Llicència Creative Commons';
$string['cclicensename'] = 'Creative Commons %s 3.0 No adaptada';
$string['cclicensestatement'] = "%s de %s està sota llicència %s .";
$string['otherpermissions'] = 'Permisos més enllà de l\'abast d\'aquesta llicència poden estar disponibles a partir de %s.';
$string['sealalttext'] = 'Aquesta llicència és adient per treballs culturals lliures.';

$string['config:noncommercial'] = 'Permeteu usos comercials del vostre treball?';
$string['config:noderivatives'] = 'Permeteu modificacions del vostre treball?';
$string['config:sharealike'] = 'Sí, mentre els altres el comparteixin de la mateixa manera';

$string['by'] = 'Reconeixement o Attribution (by): sempre que es reconegui l\'autoria de l\'obra, aquesta pot ser reproduïda, distribuïda i comunicada públicament.';
$string['by-sa'] = 'Reconeixement - CompartirIgual (by-sa): Es permet l\'ús comercial de l\'obra i de les possibles obres derivades, la distribució de les quals s\'ha de fer amb una llicència igual a la que regula l\'obra original.';
$string['by-nd'] = 'Reconeixement - SenseObraDerivada (by-nd): Es permet l\'ús comercial de l\'obra però no la generació d\'obres derivades.';
$string['by-nc'] = 'Reconeixement - NoComercial (by-nc): no es pot utilitzar l\'obra ni els treballs derivats per a finalitats comercials.';
$string['by-nc-sa'] = 'Reconeixement - NoComercial - CompartirIgual (by-nc-sa):No es permet un ús comercial de l\'obra original ni de les possibles obres derivades, la distribució de les quals s\'ha de fer amb una llicència igual a la que regula l\'obra original.';
$string['by-nc-nd'] = 'Reconeixement - NoComercial - SenseObraDerivada (by-nc-nd): No es permet un ús comercial de l\'obra original ni la generació d\'obres derivades.';
