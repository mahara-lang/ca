<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-wall
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana: Joan Queralt Gil jqueralt a gmail.com
 */

defined('INTERNAL') || die();

$string['title'] = 'El meu mur';
$string['otherusertitle'] = "Mur de %s";
$string['description'] = 'Mostra una àrea on la gent us pot deixar comentaris';
$string['noposts'] = 'No hi ha cap comentari per mostrar';
$string['makeyourpostprivate'] = 'Voleu que el comentari sigui privat?';
$string['viewwall'] = 'Mostra el mur';
$string['backtoprofile'] = 'Torna al perfil';
$string['wall'] = 'Mur';
$string['wholewall'] = 'Veure el mur sencer';
$string['reply'] = 'contesta';
$string['delete'] = 'esborra comentari';
$string['deletepost'] = 'Esborra comentari';
$string['Post'] = 'Comentari';
$string['deletepostsure'] = 'Esteu segur que voleu fer això? Penseu que l\'acció no es podrà desfer.';
$string['deletepostsuccess'] = 'S\'ha esborrat el comentari satisfactòriament';
$string['addpostsuccess'] = 'S\'ha afegit el comentari satisfactòriament';
$string['maxcharacters'] = "Màxim %s caràcters per comentari.";
$string['sorrymaxcharacters'] = "El vostre comentari no pot tenir més de %s caràcters.";
$string['posttextrequired'] = "Aquest camp és obligatori.";

// Config strings
$string['postsizelimit'] = "Límit de la mida dels comentaris";
$string['postsizelimitdescription'] = "Aquí podeu limitar la mida dels comentaris del mur. Tanmateix els comentaris existents no canviaran.";
$string['postsizelimitmaxcharacters'] = "Màxim nombre de caràcters";
$string['postsizelimitinvalid'] = "Aquest número no és vàlid.";
$string['postsizelimittoosmall'] = "El límit no pot ser menor que zero.";

?>
