<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */


defined('INTERNAL') || die();

$string['pluginname'] = 'Diaris';

$string['blog'] = 'Diari';
$string['blogs'] = 'Diaris';
$string['addblog'] = 'Afegeix diari';
$string['addpost'] = 'Afegeix entrada';
$string['alignment'] = 'Alineació';
$string['allowcommentsonpost'] = 'Permeteu comentaris a la vostra entrada.';
$string['allposts'] = 'Totes les entrades';
$string['attach'] = 'Adjunta';
$string['attachedfilelistloaded'] = 'S\'ha carregat la llista de fitxers adjunts';
$string['attachedfiles'] = 'Fitxers adjunts';
$string['attachment'] = 'Adjunció';
$string['attachments'] = 'Adjuncions';
$string['blogcopiedfromanotherview'] = 'Nota: Aquest diari s\'ha copiat d\'una altra Pàgina. Podeu canviar-lo de lloc o esborrar-lo, però no podeu canviar el que %s hi ha posat.';
$string['blogdesc'] = 'Descripció';
$string['blogdescdesc'] = 'per exemple: ‘Registre de les experiències i reflexions de la Carme‘.';
$string['blogdoesnotexist'] = 'Esteu intentant accedir a un diari que no existeix';
$string['blogpostdoesnotexist'] = 'Esteu intentant accedir a una entrada de diari que no existeix';
$string['blogpost'] = 'Entrada de diari';
$string['blogdeleted'] = 'S\'ha esborrat el diari';
$string['blogpostdeleted'] = 'S\'ha esborrat l\'entrada del diari';
$string['blogpostpublished'] = 'S\'ha publicat l\'entrada del diari';
$string['blogpostsaved'] = 'S\'ha desat l\'entrada del diari';
$string['blogsettings'] = 'Configuració del diari';
$string['blogtitle'] = 'Títol';
$string['blogtitledesc'] = 'p.ex, ‘Diari de pràctiques de Química Inorgànica de na Margarida.’.';
$string['border'] = 'Vora';
$string['cancel'] = 'Cancel·la';
$string['createandpublishdesc'] = 'Això crearà les entrades del diari i les posarà a disposició dels altres.';
$string['createasdraftdesc'] = 'Això crearà les entrades del diari, però no seran accessibles als altres fins que decidiu publicar-les.';
$string['createblog'] = 'Crea diari';
$string['dataimportedfrom'] = 'Dades importades des de %s';
$string['defaultblogtitle'] = 'Diari de %s';
$string['delete'] = 'Esborra';
$string['deleteblog?'] = 'Esteu segur que voleu esborrar aquest diari?';
$string['deleteblogpost?'] = 'Esteu segur que voleu esborrar aquesta entrada?';
$string['description'] = 'Descripció';
$string['dimensions'] = 'Dimensions';
$string['draft'] = 'Esborrany';
$string['edit'] = 'Edita';
$string['editblogpost'] = 'Edita les entrades del diari';
$string['entriesimportedfromleapexport'] = 'Entrades importades d\'una exportació en format LEAP, que no van poder ser importats en un altre lloc';
$string['errorsavingattachments'] = 'S\'ha produït un error mentre es desaven les adjuncions a l\'entrada del diari';
$string['horizontalspace'] = 'Espai horitzontal';
$string['insert'] = 'Insereix';
$string['insertimage'] = 'Insereix imatge';
$string['moreoptions'] = 'Més opcions';
$string['mustspecifytitle'] = 'Heu de donar un títol a la vostra entrada';
$string['mustspecifycontent'] = 'Heu d\'escriure algun contingut en la vostra entrada';
$string['name'] = 'Nom';
$string['newattachmentsexceedquota'] = 'La mida total dels fitxers que heu carregat en aquesta entrada excedeix la vostra quota. Podreu desar l\'entrada si esborreu alguns dels adjunts que acabeu d\'afegir.';
$string['newblog'] = 'Diari nou';
$string['newblogpost'] = 'Nova entrada al diari "%s"';
$string['newerposts'] = 'Entrades més noves';
$string['nopostsyet'] = 'Encara no hi ha entrades.';
$string['addone'] = 'Afegiu-ne una!';
$string['noimageshavebeenattachedtothispost'] = 'No s\'han adjuntat imatges a aquesta entrada. Heu de carregar o adjuntar una imatge a l\'entrada abans de poder-la inserir.';
$string['nofilesattachedtothispost'] = 'No hi ha adjuncions';
$string['noresults'] = 'No s\'ha trobat cap entrada';
$string['olderposts'] = 'Entrades més antigues';
$string['post'] = 'entrada';
$string['postbody'] = 'Cos';
$string['postbodydesc'] = ' ';
$string['postedon'] = 'Escrit el';
$string['postedbyon'] = 'Escrit per %s el %s';
$string['posttitle'] = 'Títol';
$string['posts'] = 'entrades';
$string['publish'] = 'Publica';
$string['publishfailed'] = 'S\'ha produït un error i l\'entrada no s\'ha publicat';
$string['publishblogpost?'] = 'Esteu segur que voleu publicar aquesta entrada?';
$string['published'] = 'Publicat';
$string['remove'] = 'Esborra';
$string['save'] = 'Desa';
$string['saveandpublish'] = 'Desa i publica';
$string['saveasdraft'] = 'Desa com a esborrany';
$string['savepost'] = 'Desa';
$string['savesettings'] = 'Desa la configuració';
$string['settings'] = 'Configuració';
$string['thisisdraft'] = 'Aquesta entrada és un esborrany';
$string['thisisdraftdesc'] = 'Mentre la vostra entrada sigui un esborrany només la podreu veure vós.';
$string['title'] = 'Títol';
$string['update'] = 'Actualitza';
$string['verticalspace'] = 'Espai vertical';
$string['viewblog'] = 'Veure el diari';
$string['youarenottheownerofthisblog'] = 'No sou el creador d\'aquest diari';
$string['youarenottheownerofthisblogpost'] = 'No sou el creador d\'aquesta entrada de diari';
$string['cannotdeleteblogpost'] = 'S\'ha produït un error en esborrar l\'entrada del diari.';

$string['baseline'] = 'En la línia del text';
$string['top'] = 'A dalt';
$string['middle'] = 'Al mig';
$string['bottom'] = 'A baix';
$string['texttop'] = 'A dalt del text';
$string['textbottom'] = 'A baix de tot del text';
$string['left'] = 'Esquerra';
$string['right'] = 'Dreta';
$string['src'] = 'URL de la imatge';
$string['image_list'] = 'Imatge adjunta';
$string['alt'] = 'Descripció';

$string['copyfull'] = 'Altres usuaris podran fer la seva còpia del vostre %s';
$string['copyreference'] = 'Altres usuaris podran mostrar el vostre %s a la seva Pàgina';
$string['copynocopy'] = 'Omet completament aquest diari quan es copiï la Pàgina';

$string['viewposts'] = 'Entrades copiades (%s)';
$string['postscopiedfromview'] = 'Entrades copiades de %s';

$string['youhavenoblogs'] = 'No teniu cap diari.';
$string['youhaveoneblog'] = 'Teniu 1 diari.';
$string['youhaveblogs'] = 'Teniu %s diaris.';

$string['feedsnotavailable'] = 'No està disponible l\'alimentació RSS per a aquest tipus d\'artefacte.';
$string['feedrights'] = 'Copyright %s.';

$string['enablemultipleblogstext'] = 'Teniu un diari. Si volguéssiu començar-ne un segon, activeu l\'opció <em>diaris múltiples</em> de la pàgina<a href="%saccount/">Configuració del compte</a>.';
?>
