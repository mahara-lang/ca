<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-gallery
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana: Joan Queralt Gil jqueralt a gmail.com
 */

defined('INTERNAL') || die();

$string['title'] = 'Galeria d\'imatges';
$string['description'] = 'Una col·lecció d\'imatges de la vostra àrea de fitxers.';

$string['select'] = 'Selecció d\'imatges';
$string['selectfolder'] = 'Mostra totes les imatges d\'una carpeta (S\'hi afegiran les imatges que hi carregueu més endavant).';
$string['selectimages'] = 'Escolliu les imatges individuals que s\'hi mostraran';
$string['width'] = 'Amplada';
$string['widthdescription'] = 'Especifiqueu l\'amplada de les imatges (en píxels). Les imatges s\'escalaran a aquesta amplada.';
$string['style'] = 'Estil';
$string['stylethumbs'] = 'Miniatures';
$string['styleslideshow'] = 'Presentació de diapositives';
