<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-internalmedia
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana: Joan Queralt Gil jqueralt a gmail.com
 */

defined('INTERNAL') || die();

$string['title'] = 'Mèdia incrustat';
$string['description'] = 'Seleccioneu els fitxers per vuere\'ls incrustats.';

$string['media'] = 'Mèdia';
$string['flashanimation'] = 'Animació de Flash';

$string['typeremoved'] = 'Aquest bloc apunta a un tipus de mèdia que no està autoritzat per l\'administrador.';
$string['configdesc'] = 'Configureu quin tipus de fitxer pot incrustar l\'usuari en aquest bloc. Fixeu-vos, però, que els tipus de fitxer desactivats pel connector d\'artefactes no es poden activar des d\'aquí. Si desactiveu un tipus de fitxer que ja ha estat usar en aquest bloc, ja no es podrà mostrar més.';
?>
