<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 *
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'Fitxers';

$string['sitefilesloaded'] = 'Fitxers del lloc carregats';
$string['addafile'] = 'Afegeix un fitxer';
$string['archive'] = 'Arxiva';
$string['bytes'] = 'bits';
$string['cannoteditfolder'] = 'No teniu permisos per afegir continguts en aquesta carpeta';
$string['cannoteditfoldersubmitted'] = 'Un cop enviada una Pàgina no podeu afegir contingut a una carpeta.';
$string['cannotremovefromsubmittedfolder'] = 'Un cop enviada una Pàgina no podeu esborrar contingut d\'una carpeta.';
$string['changessaved'] = 'S\'han desat els canvis';
$string['clickanddragtomovefile'] = 'Clica i arrossega per moure %s';
$string['contents'] = 'Continguts';
$string['copyrightnotice'] = 'Avís de Copyright';
$string['create'] = 'Crea';
$string['Created'] = 'S\ha creat';
$string['createfolder'] = 'Crea carpeta';
$string['confirmdeletefile'] = 'Esteu segur que voleu esborrar aquest fitxer?';
$string['confirmdeletefolder'] = 'Esteu segur que voleu esborrar aquesta carpeta?';
$string['confirmdeletefolderandcontents'] = 'Esteu segur que voleu esborrar aquesta carpeta i tot el seu contingut?';
$string['customagreement'] = 'Acord personalitzat';
$string['Date'] = 'Data';
$string['defaultagreement'] = 'Acord per defecte';
$string['defaultquota'] = 'Quota per defecte';
$string['defaultquotadescription'] = 'Podeu determinar la quantitat d\'espai en disc que tindran els nous usuaris. Els usuaris existents mantindran la que tenien.';
$string['maxquotaenabled'] = 'Imposa la quota màxima del lloc';
$string['maxquota'] = 'Quota màxima';
$string['maxquotatoolow'] = 'La quota màxima no pot ser més baixa que la quota per defecte.';
$string['maxquotaexceeded'] = 'Heu especificat una quota superior a la màxima establerta per aquest lloc (%s). Proveu a especificar un valor més baix o contacteu amb l\'administrador del lloc per incrementar la quota màxima.';
$string['maxquotaexceededform'] = 'Especifiqueu una quota de menys de %s.';
$string['maxquotadescription'] = 'Podeu establir la quota màxima que un administrador pot donar a l\'usuari. Les quotes ja donades, però, no es veuran afectades.';
$string['deletingfailed'] =  'No s\'ha pogut esborrar: el fitxer o directori no existeix';
$string['deletefile?'] = 'Esteu segur que voleu esborrar aquest fitxer?';
$string['deletefolder?'] = 'Esteu segur que voleu esborrar aquesta carpeta?';
$string['Description'] = 'Descripció';
$string['destination'] = 'Destinació';
$string['Details'] = 'Detalls';
$string['Download'] = 'Descàrrega';
$string['downloadfile'] = 'Descarrega %s';
$string['downloadoriginalversion'] = 'Descarrega la versió original';
$string['editfile'] = 'Edita fitxer';
$string['editfolder'] = 'Edita carpeta';
$string['editingfailed'] = 'No s\'ha pogut editar: el fitxer o directori no existeix';
$string['emptyfolder'] = 'Carpeta buida';
$string['file'] = 'fitxer';
$string['File'] = 'Fitxer';
$string['fileadded'] = 'S\'ha seleccionat el fitxer ';
$string['filealreadyindestination'] = 'El fitxer que heu mogut ja és en aquesta carpeta';
$string['fileappearsinviews'] = 'Aquest fitxer apareix en una o més de les vostres vistes.';
$string['fileattached'] = 'Aquest fitxer està associat a %s altre(s) item(s) del vostre ePortafolis.';
$string['fileremoved'] = 'S\'ha esborrat el fitxer';
$string['files'] = 'fitxers';
$string['Files'] = 'Fitxers';
$string['fileexists'] = 'El fitxer ja existeix';
$string['fileexistsoverwritecancel'] =  'Ja existeix un fitxer amb aquest nom. Podeu posar-n\'hi altre o sobreescriure el fitxer existent.';
$string['filelistloaded'] = 'S\'ha carregat la llista de fitxers';
$string['filemoved'] = 'S\'ha mogut correctament el fitxer';
$string['filenamefieldisrequired'] = 'Cal que ompliu el camp obligatori Fitxer';
$string['fileinstructions'] = 'Carregueu les vostres imatges, documents o d\'altres fitxers per poder-los incloure, més endavant, a les vostres vistes. Per moure un fitxer o carpeta arrossegueu-lo i deixeu-lo anar sobre la carpeta desitjada.';
$string['filethingdeleted'] = 'S\'ha esborrat %s';
$string['filewithnameexists'] = 'Ja existeix un fitxer o carpeta amb el nom de "%s".';
$string['folder'] = 'Carpeta';
$string['Folder'] = 'Carpeta';
$string['folderappearsinviews'] = 'Aquesta carpeta apareix en una o més de les vostres vistes.';
$string['Folders'] = 'Carpetes';
$string['foldernotempty'] = 'Aquesta carpeta no és buida.';
$string['foldercreated'] = 'S\'ha creat la carpeta';
$string['foldernamerequired'] = 'Escriviu un nom per a la nova carpeta.';
$string['gotofolder'] = 'Vés a %s';
$string['groupfiles'] = 'Fitxers del grup';
$string['home'] = 'Inici';
$string['htmlremovedmessage'] = 'Esteu veient <strong>%s</strong> de <a href="%s">%s</a>. El fitxer ha estat filtrat de continguts maliciosos i és només una previsualització de l\'original.';
$string['htmlremovedmessagenoowner'] = 'Esteu veient <strong>%s</strong>. El fitxer ha estat filtrat de continguts maliciosos i és només una previsualització de l\'original.';
$string['image'] = 'Imatge';
$string['Images'] = 'Imatges';
$string['lastmodified'] = 'Darrera modificació';
$string['myfiles'] = 'Els meus Fitxers';
$string['Name'] = 'Nom';
$string['namefieldisrequired'] = 'El camp Nom és obligatori';
$string['maxuploadsize'] = 'Mida màxima de càrrega';
$string['movefaileddestinationinartefact'] = 'No podeu posar una carpeta dins d\'ella mateixa.';
$string['movefaileddestinationnotfolder'] = 'Només podeu moure els fitxers cap dins les carpetes.';
$string['movefailednotfileartefact'] = 'Només es poden moure fitxers, carpetes i artefactes d\'imatge.';
$string['movefailednotowner'] = 'No teniu permisos per moure el fitxer a aquesta carpeta.';
$string['movefailed'] = 'No s\'ha pogut moure.';
$string['movingfailed'] = 'No s\'ha pogut moure: el fitxer o directori no existeix';
$string['nametoolong'] = 'Aquest nom és massa llarg, n\'heu de posar un de més curt.';
$string['nofilesfound'] = 'No s\'ha trobat cap fitxer';
$string['notpublishable'] = 'No teniu permisos per publicar aquest fitxer';
$string['overwrite'] = 'Sobreescriu';
$string['Owner'] = 'Creador';
$string['parentfolder'] = 'Carpeta arrel';
$string['Preview'] = 'Vista prèvia';
$string['requireagreement'] = 'Es necessita l\'acord.';
$string['removingfailed'] = 'No s\'ha pogut eliminar: el fitxer o carpeta no existeix';
$string['savechanges'] = 'Desa els canvis';
$string['selectafile'] = 'Seleccioneu un fitxer';
$string['selectingfailed'] = 'No s\'ha pogut seleccionar: el fitxer o directori no existeix';
$string['Size'] = 'Mida';
$string['spaceused'] = 'Espai utilitzat';
$string['timeouterror'] = 'No s\'ha completat la càrrega del fitxer. Torneu-ho a provar.';
$string['title'] = 'Nom';
$string['titlefieldisrequired'] = 'El camp Nom és obligatori';
$string['Type'] = 'Tipus';
$string['upload'] = 'Carrega';
$string['uploadagreement'] = 'Acord de càrrega';
$string['uploadagreementdescription'] = 'Marqueu aquesta opció si voleu obligar als usuaris a estar d\'acord amb el text inferior per poder carregar un fitxer al lloc.';
$string['uploadexceedsquota'] = 'Si carregueu aquest fitxer sobrepassareu la vostra quota de disc. Proveu a esborrar alguns fitxers que hagueu carregat abans.';
$string['uploadfile'] =  'Carrega un fitxer';
$string['uploadfileexistsoverwritecancel'] =  'Ja existeix un fitxer amb aquest nom. Podeu canviar el nom al fitxer que voleu carregar o sobreescriure l\'existent.';
$string['uploadingfiletofolder'] =  'Carregant %s a %s';
$string['uploadoffilecomplete'] = 'S\'ha completat la càrrega de %s';
$string['uploadoffilefailed'] =  'No s\'ha pogut carregar %s';
$string['uploadoffiletofoldercomplete'] = 'S\'ha completat la càrrega de %s a %s';
$string['uploadoffiletofolderfailed'] = 'No s\'ha pogut carregar %s a %s';
$string['usecustomagreement'] = 'Utilitza l\'acord personalitzat';
$string['youmustagreetothecopyrightnotice'] = 'Heu d\'acceptar l\'avís de Copyright';
$string['fileuploadedtofolderas'] = '%s carregat a %s com a "%s"';
$string['fileuploadedas'] = '%s carregat com a "%s"';


// File types
$string['ai'] = 'Document Postscript';
$string['aiff'] = 'Fitxer d\'Audio AIFF';
$string['application'] = 'Aplicació desconeguda';
$string['au'] = 'Fitxer d\'Audio AU';
$string['avi'] = 'Fitxer de Vídeo AVI';
$string['bmp'] = 'Imatge Bitmap';
$string['doc'] = 'Document MS Word';
$string['dss'] = 'Digital Speech Standard Sound File';
$string['gif'] = 'Imatge GIF';
$string['html'] = 'Fitxer HTML';
$string['jpg'] = 'Imatge JPEG';
$string['jpeg'] = 'Imatge JPEG';
$string['js'] = 'Javascript File';
$string['latex'] = 'Document LaTeX';
$string['m3u'] = 'Fitxer d\'Audio M3U';
$string['mp3'] = 'Fitxer d\'Audio MP3';
$string['mp4_audio'] = 'Fitxer d\'Audio MP4';
$string['mp4_video'] = 'Fitxer de Vídeo MP4';
$string['mpeg'] = 'Pel·lícula MPEG';
$string['odb'] = 'Base de Dades Openoffice';
$string['odc'] = 'Fitxer de càlcul Openoffice';
$string['odf'] = 'Fitxer de fórmula Openoffice';
$string['odg'] = 'Fitxer de gràfics Openoffice';
$string['odi'] = 'Imatge Openoffice Image';
$string['odm'] = 'Document mestre Openoffice';
$string['odp'] = 'Presentació Openoffice';
$string['ods'] = 'Full de càlcul Openoffice';
$string['odt'] = 'Document de text Openoffice ';
$string['oth'] = 'Document Web Openoffice';
$string['ott'] = 'Plantilla Openoffice';
$string['pdf'] = 'Document PDF ';
$string['png'] = 'Imatge PNG ';
$string['ppt'] = 'Document MS Powerpoint ';
$string['quicktime'] = 'Pel·lícula Quicktime';
$string['ra'] = 'Fitxer Real Audio';
$string['rtf'] = 'Document RTF ';
$string['sgi_movie'] = 'Pel·lícula SGI';
$string['sh'] = 'Shell Script';
$string['tar'] = 'Fitxer TAR';
$string['gz'] = 'Fitxer comprimit Gzip';
$string['bz2'] = 'Fitxer comprimit Bzip2';
$string['txt'] = 'Fitxer de text pla';
$string['wav'] = 'Fitxer d\'Audio WAV';
$string['wmv'] = 'Fitxer de Vídeo WMV';
$string['xml'] = 'Fitxer XML';
$string['zip'] = 'Fitxer comprimit ZIP Archive';
$string['swf'] = 'Pel·lícula Flash SWF';
$string['flv'] = 'Pel·lícula Flash FLV';
$string['mov'] = 'MOV Quicktime movie';
$string['mpg'] = 'MPG Movie';
$string['ram'] = 'RAM Real Player Movie';
$string['rpm'] = 'RPM Real Player Movie';
$string['rm'] = 'RM Real Player Movie';



// Profile icons
$string['profileiconsize'] = 'Mida de la icona del perfil';
$string['cantcreatetempprofileiconfile'] = 'No s\'ha pogut desar la imatge com a icona de perfil temporal a %s';
$string['profileicons'] = 'Icones del perfil';
$string['Default'] = 'Per defecte';
$string['deleteselectedicons'] = 'Esborra les icones seleccionades';
$string['profileicon'] = 'Icona del perfil';
$string['noimagesfound'] = 'No s\'ha trobat cap imatge';
$string['uploadedprofileiconsuccessfully'] = 'S\'ha carregat correctament la nova icona del perfil';
$string['profileiconsetdefaultnotvalid'] = 'No es pot definir la icona del perfil per defecte, l\'opció no és vàlida';
$string['profileiconsdefaultsetsuccessfully'] = 'S\'ha definit correctament la Icona del perfil per defecte';
$string['profileiconsdeletedsuccessfully'] = 'S\'ha(n) esborrat correctament la(es) icona(es) del perfil';
$string['profileiconsnoneselected'] = 'No hi ha cap icona seleccionada per esborrar';
$string['onlyfiveprofileicons'] = 'Només podeu carregar 5 icones del perfil';
$string['or'] = 'o';
$string['profileiconuploadexceedsquota'] = 'Si carregueu aquesta icona del perfil excedireu la vostra quota d\'espai al disc. Mireu d\'esborrar alguns fitxers carregats que no us serveixin';
$string['profileiconimagetoobig'] = 'La imatge que heu carregat és massa gran (%sx%s píxels). No ha de superar els %sx%s píxels';
$string['uploadingfile'] = 's\'està carregant el fitxer...';
$string['uploadprofileicon'] = 'Carrega la Icona del perfil';
$string['profileiconsiconsizenotice'] = 'Aquí podeu carregar fins a <strong>cinc</strong> icones del perfil i escollir-ne només una per mostrar com a icona per defecte. La mida de les icones ha d\'estar entre 16x16 i %sx%s píxels.';
$string['setdefault'] = 'Marca-la com per defecte';
$string['Title'] = 'Títol';
$string['imagetitle'] = 'Títol de la imatge';
$string['usenodefault'] = 'Usa una diferent a per defecte';
$string['usingnodefaultprofileicon'] = 'Esteu utilitzant una Icona de perfil diferent a la "per defecte"';
$string['wrongfiletypeforblock'] = 'El fitxer que heu carregat no és del tipus correcte per a aquest bloc.';

// Unzip
$string['Contents'] = 'Continguts';
$string['Continue'] = 'Continua';
$string['extractfilessuccess'] = 'S\'han creat %s carpetes i %s fitxers.';
$string['filesextractedfromarchive'] = 'Fitxers extrets de l\'arxiu';
$string['filesextractedfromziparchive'] = 'Fitxers extrets de l\'arxiu zip';
$string['fileswillbeextractedintofolder'] = 'S\'extrauran els fitxers a %s';
$string['insufficientquotaforunzip'] = 'La vostra quota d\'espai restant és massa petita per poder-hi descomprimir aquest fitxer.';
$string['invalidarchive'] = 'S\'ha produït un error al llegir l\'arxiu.';
$string['pleasewaitwhileyourfilesarebeingunzipped'] = 'Espereu mentre es descomprimeixen els fitxers.';
$string['spacerequired'] = 'Espai necessari';
$string['Unzip'] = 'Descomprimeix';
$string['unzipprogress'] = 'S\'han creat %s fitxers/carpetes.';

// Group file permissions
$string['filepermission.view'] = 'Veure';
$string['filepermission.edit'] = 'Edita';
$string['filepermission.republish'] = 'Publica';


?>
