<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */

defined('INTERNAL') || die();

/* Plans */
$string['canteditdontownplan'] = 'No podeu editar aquest Pla perquè no us pertany.';
$string['description'] = 'Descripció';
$string['deleteplanconfirm'] = 'Esteu segur que voleu esborrar aquest pla? Si l\'esborreu esborrareu també les tasques que contingui.';
$string['deleteplan'] = 'Esborra el pla';
$string['deletethisplan'] = 'Esborra el pla: \'%s\'';
$string['editplan'] = 'Edita el pla';
$string['editingplan'] = 'S\'està editant el pla';
$string['managetasks'] = 'Gestiona tasques';

$string['newplan'] = 'Pla nou';
$string['noplansaddone'] = 'Encara no hi ha plans. %sAfegiu-ne un%s!';
$string['noplans'] = 'No hi ha cap pla per mostrar';
$string['plan'] = 'Pla';
$string['plans'] = 'Plans';
$string['Plans'] = 'Plans';
$string['plandeletedsuccessfully'] = 'S\'ha esborrat correctament el pla.';
$string['plannotdeletedsuccessfully'] = 'S\'ha produït un error a l\'esborrar el pla.';
$string['plannotsavedsuccessfully'] = 'S\'ha produït un error a l\'enviar el formulari. Comproveu el camps destacats i torneu-ho a provar.';
$string['plansavedsuccessfully'] = 'S\'ha desat correctament el pla .';
$string['planstasks'] = 'Tasques del pla \'%s\'';
$string['planstasksdesc'] = 'Per començar a construir el vostre pla afegiu-hi tasques des d\'aquí sota o des del botó de la dreta.';
$string['saveplan'] = 'Desa el pla';
$string['title'] = 'Títol';
$string['titledesc'] = 'El títol de la tasca serveix per mostrar cada tasca al bloc <em>Els meus plans</em>.';

/* Tasks */
$string['alltasks'] = 'Totes les tasques';
$string['canteditdontowntask'] = 'No podeu editar aquesta Tasca perquè no us pertany.';
$string['completed'] = 'Completada';
$string['completiondate'] = 'Data de compleció';
$string['completeddesc'] = 'Marqueu la tasca com a completada.';
$string['deletetaskconfirm'] = 'Esteu segur que voleu esborrar aquesta tasca?';
$string['deletetask'] = 'Esborra la tasca';
$string['deletethistask'] = 'Esborra la tasca: \'%s\'';
$string['edittask'] = 'Edita la tasca';
$string['editingtask'] = 'S\'està editant la tasca';
$string['mytasks'] = 'Les meves Tasques';
$string['newtask'] = 'Tasca nova';
$string['notasks'] = 'No hi ha cap tasca per mostrar.';
$string['notasksaddone'] = 'Encara no hi ha tasques. %sAfegiu-ne una%s!';
$string['savetask'] = 'Desa la tasca';
$string['task'] = 'tasca';
$string['tasks'] = 'tasques';
$string['taskdeletedsuccessfully'] = 'S\'ha esborrat la tasca satisfactòriament.';
$string['tasksavedsuccessfully'] = 'S\'ha desat la tasca satisfactòriament.';


?>
