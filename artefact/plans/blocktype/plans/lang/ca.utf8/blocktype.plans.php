<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-plans
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */

defined('INTERNAL') || die();

$string['title'] = 'Els vostres plans';
$string['description'] = 'Mostra la llista dels vostres plans';
$string['defaulttitledescription'] = 'Si ho deixeu en blanc s\'utilitzarà el títol del pla.';
$string['newerplans'] = 'Plans recents';
$string['noplansaddone'] = 'Encara no hi ha cap pla. %sAfegiu-ne un%s!';
$string['olderplans'] = 'Plans antics';
$string['planstoshow'] = 'Pla a mostrar';
?>
