<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */


defined('INTERNAL') || die();

$string['pluginname'] = 'Perfil';

$string['profile'] = 'Perfil';


$string['mandatory'] = 'Obligatori';
$string['public'] = 'Públic';

$string['aboutdescription'] = 'Escriviu el nom i cognoms reals. Si voleu mostrar un altre nom a les persones del lloc, poseu-lo com a Nom preferit.';
$string['infoisprivate'] = 'Aquesta informació serà privada fins que la inclogueu en una Pàgina que serà compartida amb altres usuaris.';
$string['viewmyprofile'] = 'Veure El Meu Perfil';

// profile categories
$string['aboutme'] = 'Sobre mi';
$string['contact'] = 'Informació de contacte';
$string['messaging'] = 'Missatgeria';
$string['general'] = 'General';

// profile fields
$string['firstname'] = 'Nom';
$string['lastname'] = 'Cognom';
$string['fullname'] = 'Nom complet';
$string['institution'] = 'Institució';
$string['studentid'] = 'ID estudiant';
$string['preferredname'] = 'Nom preferit';
$string['introduction'] = 'Introducció';
$string['email'] = 'Adreça de correu';
$string['maildisabled'] = 'Correu electrònic desactivat';
$string['officialwebsite'] = 'Pàgina web oficial';
$string['personalwebsite'] = 'Pàgina web personal';
$string['blogaddress'] = 'Adreça del blog';
$string['address'] = 'Adreça postal';
$string['town'] = 'Ciutat';
$string['city'] = 'Comarca/País';
$string['country'] = 'Estat';
$string['homenumber'] = 'Telèfon preferit';
$string['businessnumber'] = 'Telèfon de la feina';
$string['mobilenumber'] = 'Segon telèfon';
$string['faxnumber'] = 'Fax';
$string['icqnumber'] = 'ICQ';
$string['msnnumber'] = 'MSN';
$string['aimscreenname'] = 'Nom en pantalla AIM';
$string['yahoochat'] = 'Chat de Yahoo ';
$string['skypeusername'] = 'Skype';
$string['jabberusername'] = 'Jabber';
$string['occupation'] = 'Ocupació';
$string['industry'] = 'Professió';

// Field names for view user and search user display
$string['name'] = 'Nom';
$string['principalemailaddress'] = 'Correu preferit';
$string['emailaddress'] = 'Segon correu';

$string['saveprofile'] = 'Desa perfil';
$string['profilesaved'] = 'S\'ha desat correctament el perfil';
$string['profilefailedsaved'] = 'No s\'ha pogut desar el perfil';


$string['emailvalidation_subject'] = 'Validació del correu';
$string['emailvalidation_body'] = <<<EOF
Hola %s,

L'adreça de correu %s s'ha afegit al vostre compte d'usuari de Mahara. Visiteu l'enllaç següent per activar-la.

%s
EOF;


$string['validationemailwillbesent'] = 'S\'enviarà un missatge de validació quan deseu el vostre perfil';
$string['validationemailsent'] = 'S\'ha enviat un correu de validació';
$string['emailactivation'] = 'Activació del correu';
$string['emailactivationsucceeded'] = 'S\'ha activat correctament el correu';
$string['emailalreadyactivated'] = 'Ja s\'ha activat el correu';
$string['emailactivationfailed'] = 'No s\'ha activat el correu';
$string['verificationlinkexpired'] = 'Ha vençut l\'enllaç de verificació';
$string['invalidemailaddress'] = 'L\'adreça de correu no és vàlida';
$string['unvalidatedemailalreadytaken'] = 'L\'adreça de correu que esteu provant de validar ja és utilitzada per un altre usuari';
$string['emailactivationdeclined'] = 'S\'ha refusat correctament l\'activació del correu electrònic';
$string['emailingfailed'] = 'S\'ha desat el perfil, però no s\'han enviat els missatges a: %s';

$string['loseyourchanges'] = 'Voleu descartar els canvis?';
$string['addbutton'] = 'Afegeix';

$string['Title'] = 'Títol';

$string['Created'] = 'Creat';
$string['Description'] = 'Descripció';
$string['Download'] = 'Descarrega';

$string['lastmodified'] = 'Darrera modificació';
$string['Owner'] = 'Titular';
$string['Preview'] = 'Vista prèvia';
$string['Size'] = 'Mida';
$string['Type'] = 'Tipus';

$string['profileinformation'] = 'Informació del perfil';
$string['profilepage'] = 'Pàgina del perfil';
$string['viewprofilepage'] = 'Veure la pàgina del perfil';
$string['viewallprofileinformation'] = 'Veure tota la informació del perfil';
?>
