<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-profileinfo
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana: Joan Queralt Gil jqueralt a gmail.com
 */


defined('INTERNAL') || die();

$string['title'] = 'Informació del perfil';
$string['description'] = 'Escolliu quina Informació del perfil voleu mostrar.';

$string['aboutme'] = 'Sobre mi';
$string['fieldstoshow'] = 'Camps a mostrar';
$string['introtext'] = 'Text d\'introducció';
$string['useintroductioninstead'] = 'Podeu deixar aquest camp en blanc i, en canvi, activar el camp d\'Introducció del vostre perfil.';
$string['dontshowprofileicon'] = "No mostris l'icona del perfil";
$string['dontshowemail'] = "No mostris l'adreça de correu electrònic.";
$string['uploadaprofileicon'] = "No teniu cap icona de perfil. <a href=\"%sartefact/file/profileicons.php\" target=\"_blank\">Carregueu-ne una</a>";
?>
