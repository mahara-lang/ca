<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'Comentari';
$string['Comment'] = 'Comentari';
$string['Comments'] = 'Comentaris';
$string['comment'] = 'comentari';
$string['comments'] = 'comentaris';

$string['Allow'] = 'Permet';
$string['allowcomments'] = 'Permet comentaris';
$string['approvalrequired'] = 'Els comentaris són moderats, així que si trieu fer públic aquest comentari, no serà visible pels altres fins que no estigui aprovat pel seu titular.';
$string['attachfile'] = "Adjunta un fitxer";
$string['Attachments'] = "Adjuncions";
$string['cantedithasreplies'] = 'Només podeu editar el comentari més recent';
$string['canteditnotauthor'] = 'No sou l\'autor d\'aquest comentari';
$string['cantedittooold'] = 'Només podeu editar els comentaris fets fa menys de %d minuts';
$string['commentmadepublic'] = "S\'ha fet públic el comentari";
$string['commentdeletedauthornotification'] = "El vostre comentari a %s s\'ha eliminat:\n%s";
$string['commentdeletednotificationsubject'] = 'S\'ha eliminat el comentari a %s';
$string['commentnotinview'] = 'El comentari %d no és a la Pàgina  %d';
$string['commentratings'] = 'Activa puntuacions dels comentaris';
$string['commentremoved'] = 'S\'ha eliminat el comentari';
$string['commentremovedbyauthor'] = 'Comentari eliminat per l\'autor';
$string['commentremovedbyowner'] = 'Comentari eliminat pel titular';
$string['commentremovedbyadmin'] = 'Comentari eliminat per l\'administrador';
$string['commentupdated'] = 'S\'ha actualitzat el comentari';
$string['editcomment'] = 'Edita el comentari';
$string['editcommentdescription'] = 'Podeu actualitzar els vostres comentaris si els heu fet fa menys de %d minuts i no tenen cap resposta més recents.  A partir d\'aquest moment, però, encara podreu eliminar-los i afegir-ne de nous.';
$string['entriesimportedfromleapexport'] = 'Entrades importades d\'una exportació LEAP que no van poder ser importades en un altre lloc';
$string['feedback'] = 'Retroacció';
$string['feedbackattachdirname'] = 'commentfiles';
$string['feedbackattachdirdesc'] = 'Fitxers adjunts als comentaris del vostre eportafolis';
$string['feedbackattachmessage'] = 'S\'han afegit al vostre directori %s el(s) fitxer(s) adjunt(s)';
$string['feedbackonviewbyuser'] = 'Retroacció a %s de %s';
$string['feedbacksubmitted'] = 'S\'ha enviat la retroacció';
$string['lastcomment'] = 'Últim comentari';
$string['makepublic'] = 'Fes públic';
$string['makepublicnotallowed'] = 'No teniu permisos per fer públic aquest comentari';
$string['makepublicrequestsubject'] = 'Sol·licitud per canviar un comentari privat a públic';
$string['makepublicrequestbyauthormessage'] = '%s ha sol·licitat que feu públic el seu comentari.';
$string['makepublicrequestbyownermessage'] = '%s ha sol·licitat que feu públic el vostre comentari.';
$string['makepublicrequestsent'] = 'S\'ha enviat un missatge a %s per demanar-li que es faci públic el comentari.';
$string['messageempty'] = 'El missatge és buit i els missatges buits només són permesos si hi adjunteu un fitxer.';
$string['Moderate'] = 'Modera';
$string['moderatecomments'] = 'Modera comentaris';
$string['moderatecommentsdescription'] = 'Els comentaris romandran privats fins que els aproveu.';
$string['newfeedbacknotificationsubject'] = 'Retroacció nova a  %s';
$string['placefeedback'] = 'Feu una retroacció';
$string['rating'] = 'Puntuacions';
$string['reallydeletethiscomment'] = 'Esteu segur que voleu esborrar aquest comentari?';
$string['thiscommentisprivate'] = 'Aquest comentari és privat';
$string['typefeedback'] = 'Retroacció';
$string['viewcomment'] = 'Veure comentari';
$string['youhaverequestedpublic'] = 'Heu sol·licitat que es faci públic aquest comentari.';

$string['feedbacknotificationhtml'] = "<div style=\"padding: 0.5em 0; border-bottom: 1px solid #999;\"><strong>%s ha comentat a %s</strong><br>%s</div>

<div style=\"margin: 1em 0;\">%s</div>

<div style=\"font-size: smaller; border-top: 1px solid #999;\">
<p><a href=\"%s\">Contesteu a aquest comentari</a></p>
</div>";
$string['feedbacknotificationtext'] = "%s ha comentat a %s
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------
Per veure i contestar aquest comentari seguiu aquest enllaç:
%s";
$string['feedbackdeletedhtml'] = "<div style=\"padding: 0.5em 0; border-bottom: 1px solid #999;\"><strong>S\'ha esborrat un comentari a %s</strong><br>%s</div>

<div style=\"margin: 1em 0;\">%s</div>

<div style=\"font-size: smaller; border-top: 1px solid #999;\">
<p><a href=\"%s\">%s</a></p>
</div>";
$string['feedbackdeletedtext'] = "S\'ha esborrat un comentari a %s
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------
Per veure %s seguiu aquest enllaç:
%s";

$string['artefactdefaultpermissions'] = 'Permisos per defecte dels comentaris';
$string['artefactdefaultpermissionsdescription'] = 'Quan es creïn els tipus d\'artefactes seleccionats tindran activats els comentaris.  Tanmateix els usuaris poden anular individualment aquesta configuració dels artefactes.';


?>
