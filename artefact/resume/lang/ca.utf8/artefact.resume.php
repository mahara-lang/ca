<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-resume
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */


defined('INTERNAL') || die();

$string['pluginname'] = 'CV';

// Tabs
$string['introduction'] = 'Introducció';
$string['educationandemployment'] = 'Formació i Ocupació';
$string['achievements'] = 'Assoliments';
$string['goals'] = 'Objectius';
$string['skills'] = 'Habilitats';
$string['interests'] = 'Interessos';


$string['mygoals'] = 'Els meus objectius';
$string['myskills'] = 'Les meves habilitats';
$string['coverletter'] = 'Portada';
$string['interest'] = 'Interessos';
$string['contactinformation'] = 'Informació de contacte';
$string['personalinformation'] = 'Informació personal';
$string['dateofbirth'] = 'Data de naixement';
$string['placeofbirth'] = 'Lloc de naixement';
$string['citizenship'] = 'Nacionalitat';
$string['visastatus'] = 'Situació legal';
$string['female'] = 'Dona';
$string['male'] = 'Home';
$string['gender'] = 'Gènere';
$string['maritalstatus'] = 'Estat civil';
$string['resumesaved'] = 'S\'ha desat el vostre CV';
$string['resumesavefailed'] = 'No s\'ha pogut actualitzar el vostre CV';
$string['educationhistory'] = 'Historial acadèmic';
$string['employmenthistory'] = 'Historial laboral';
$string['certification'] = 'Certificacions, Acreditacions i Premis';
$string['book'] = 'Llibres i publicacions';
$string['membership'] = 'Associacions professionals';
$string['startdate'] = 'Data d\'inici';
$string['enddate'] = 'Data de finalització';
$string['date'] = 'Data';
$string['position'] = 'Posició';
$string['qualification'] = 'Qualificació';
$string['title'] = 'Títol';
$string['description'] = 'Descripció';
$string['employer'] = 'Empresa';
$string['jobtitle'] = 'Feina';
$string['jobdescription'] = 'Descripció de la feina';
$string['institution'] = 'Institució';
$string['qualtype'] = 'Tipus de qualificació';
$string['qualname'] = 'Nom de la qualificació';
$string['qualdescription'] = 'Descripció de la qualificació';
$string['contribution'] = 'Contribució';
$string['detailsofyourcontribution'] = 'Detalls de la vostra contribució';
$string['compositedeleteconfirm'] = 'Esteu segur que ho voleu esborrar?';
$string['compositesaved'] = 'S\'ha desat correctament';
$string['compositesavefailed'] = 'S\'ha  produït un error en desar.';
$string['compositedeleted'] = 'S\'ha esborrat correctament';

$string['personalgoal'] = 'Objectius personals';
$string['academicgoal'] = 'Objectius acadèmics';
$string['careergoal'] = 'Objectius professionals';
$string['defaultpersonalgoal'] = '';
$string['defaultacademicgoal'] = '';
$string['defaultcareergoal'] = '';
$string['personalskill'] = 'Habilitats personals';
$string['academicskill'] = 'Habilitats acadèmiques';
$string['workskill'] = 'Habilitats laborals';
$string['goalandskillsaved'] = 'S\'ha desat correctament';
$string['resume'] = 'CV';
$string['current'] = 'Actual';
$string['moveup'] = 'Puja';
$string['movedown'] = 'Baixa';
$string['viewyourresume'] = 'Veure el vostre CV';
$string['resumeofuser'] = 'CV de %s';
$string['employeraddress'] = 'Adreça de l\'empresa';
$string['institutionaddress'] = 'Adreça de la Institució';
?>
