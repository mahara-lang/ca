<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */

defined('INTERNAL') || die();

// IMAP
$string['host'] = 'Nom o adreça del host';
$string['host'] = 'Nom del servidor o adreça';
$string['wwwroot'] = 'Arrel WWW';

$string['port'] = 'Número de port';
$string['protocol'] = 'Protocol';
$string['changepasswordurl'] = 'URL de canvi de contrasenya';
$string['cannotremove']  = "No es pot esborrar aquest connector d\'autenticació perquè és l'únic connector que té aquesta institució.";
$string['cannotremoveinuse']  = "No es pot esborrar aquest connector d\'autenticació perquè l'utilitzen alguns usuaris.\nHeu d'actualitzar les seves dades abans d'esborrar el connector.";
$string['saveinstitutiondetailsfirst'] = 'Deseu els detalls d la institució abans de configurar els connectors d\'autenticació.';

$string['editauthority'] = 'Edita una autoritat';
$string['addauthority']  = 'Afegeix una autoritat';

$string['updateuserinfoonlogin'] = 'Actualitza la informació de l\'usuari en entrar al lloc';
$string['updateuserinfoonlogindescription'] = 'Recupera la informació de l\'usuari del servidor remot i actualitza el seu registre local cada cop que l\'usuari entra al lloc.';
$string['xmlrpcserverurl'] = 'URL del servidor XML-RPC';
$string['ipaddress'] = 'Adreça IP';
$string['shortname'] = 'Nom curt del vostre lloc';
$string['name'] = 'Nom del lloc';
$string['nodataforinstance'] = 'No s\'han trobat dades per la instància d\'autenticació ';
$string['authname'] = 'Nom de l\'autoritat';
$string['weautocreateusers'] = 'Aquí es crearan els usuaris automàticament';
$string['theyautocreateusers'] = 'Allà es crearan els usuaris automàticament';
$string['parent'] = 'Autoritat pare';
$string['wessoout'] = 'Aquí desactivem SSO (out)';
$string['weimportcontent'] = 'Importem continguts (només d\'algunes aplicacions)';
$string['weimportcontentdescription'] = '(només algunes aplicacions)';
$string['theyssoin'] = 'Allà activen SSO (in)';
$string['authloginmsg'] = "Escriviu el missatge que es mostrarà quan l\'usuari intenti registrar-se des del formulari de Mahara";
$string['authloginmsg'] = "Escriviu el missatge que veurà l\'usuari quan provi d\identificar-se via formulari d\'entrada de Mahara";
$string['application'] = 'Aplicació';
$string['cantretrievekey'] = 'S\'ha produït un error en recuperar la clau pública del servidor remot.<br> Assegureu-vos que els camps Aplicació i Arrel WWW són correctes i que s\'ha activat la xarxa al servidor remot.';

$string['errorcertificateinvalidwwwroot'] = 'Aquest certificat afirma ser per a %s, però esteu provant d\'usar-lo per a %s.';
$string['errorcouldnotgeneratenewsslkey'] = 'No s\'ha pogut generar una clau SSL nova. Assegureu-vos que tant openssl com el mòdul PHP per openssl estan instal·lats en aquesta màquina.';
$string['errnoauthinstances']   = 'Sembla que no hi ha configurada cap instància de connector d\'autenticació pel host a ';
$string['errornotvalidsslcertificate'] = 'Aquest no és un Certificat SSL vàlid';
$string['errnoxmlrpcinstances'] = 'Sembla que no hi ha configurada cap instància del connector d\'autenticació XMLRPC pel host a %s';
$string['errnoxmlrpcwwwroot']   = 'Sembla que no hi ha cap registre per a cap host a %s';
$string['errnoxmlrpcuser']      = "Ara mateix no us podem autenticar. Les possibles causes poden ser:

    * Ha expirat la vostra sessió SSO. Torneu a l'altre aplicació i cliqueu sobre l'enllaç per tornar a entrar a Mahara.
    * No esteu autoritzat per entrar a Mahara per SSO. Poseu-vos en contacte amb el vostre administrador si penseu que hi hauríeu d'estar.";

$string['unabletosigninviasso'] = 'No és possible registrar-se a través de SSO';
$string['xmlrpccouldnotlogyouin'] = 'Ho sentim, però no podeu entrar :(';
$string['xmlrpccouldnotlogyouindetail'] = 'Ho sentim, però no podeu entrar ara mateix. Proveu d\'aquí a una estona i, si el problema continua, poseu-vos en contacte amb l\'administrador.';

$string['requiredfields'] = 'Camps del perfil requerits';
$string['requiredfieldsset'] = 'Configurats els camps de perfil requerits';
$string['noauthpluginconfigoptions'] = 'No hi ha cap opció de configuració associada a aquest connector';

$string['hostwwwrootinuse'] = 'El directori arrel WWW ja està essent utilitzat per una altra institució (%s)';

// Error messages for external authentication usernames
$string['duplicateremoteusername'] = 'Aquest nom d\'usuari d\'autenticació externa ja està essent usat per %s. Els nom d\'usuari d\'autenticació externa han de ser únics en un mètode d\'autenticació.';
$string['duplicateremoteusernameformerror'] = 'Els nom d\'usuari d\'autenticació externa han de ser únics en un mètode d\'autenticació.';


?>
