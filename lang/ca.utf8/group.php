<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */


defined('INTERNAL') || die();

// my groups
$string['groupname'] = 'Nom del grup';
$string['creategroup'] = 'Crea grup';
$string['groupmemberrequests'] = 'Sol·licituds d\'inscripció pendents';
$string['membershiprequests'] = 'Sol·licituds d\'inscripció';
$string['sendinvitation'] = 'Envia invitació';
$string['invitetogroupsubject'] = 'Esteu convidat a unir-vos al grup';
$string['invitetogroupmessage'] = '%s us ha convidat a unir-vos al grup \'%s\'. Cliqueu sobre l\'enllaç per obtenir més informació.';
$string['inviteuserfailed'] = 'No s\'ha pogut convidar l\'usuari';
$string['userinvited'] = 'S\'ha enviat la invitació';
$string['addedtogroupsubject'] = 'Se us ha afegit al grup';
$string['addedtogroupmessage'] = '%s us ha afegit al grup \'%s\'.  Cliqueu sobre l\'enllaç per veure\'l.';
$string['adduserfailed'] = 'No s\'ha pogut afegir l\'usuari';
$string['useradded'] = 'S\'ha afegit l\'usuari';
$string['editgroup'] = 'Edita grup';
$string['savegroup'] = 'Desa grup';
$string['groupsaved'] = 'S\'ha desat correctament el grup';
$string['invalidgroup'] = 'Aquest grup no existeix';
$string['canteditdontown'] = 'No podeu editar aquest grup perquè no us pertany';
$string['groupdescription'] = 'Descripció del grup';
$string['membershiptype'] = 'Tipus de pertinença al grup';
$string['membershiptype.controlled'] = 'Pertinença controlada';
$string['membershiptype.invite']     = 'Només per invitació';
$string['membershiptype.request']    = 'Sol·licitud de Pertinença';
$string['membershiptype.open']       = 'Pertinença oberta';
$string['membershiptype.abbrev.controlled'] = 'Controlada';
$string['membershiptype.abbrev.invite']     = 'Invitació';
$string['membershiptype.abbrev.request']    = 'Sol·licitud';
$string['membershiptype.abbrev.open']       = 'Oberta';
$string['pendingmembers']            = 'Membres pendents';
$string['reason']                    = 'Raó';
$string['approve']                   = 'Aprova';
$string['reject']                    = 'Rebutja';
$string['groupalreadyexists'] = 'Ja hi ha un grup amb aquest nom';
$string['Created'] = 'Creat';
$string['groupadmins'] = 'Administradors del grup';
$string['Admin'] = 'Administrador';
$string['grouptype'] = 'Tipus de grup';
$string['publiclyviewablegroup'] = 'Visibilitat pública del grup?';
$string['publiclyviewablegroupdescription'] = 'Permet veure aquest grup i els seus fòrums a tothom, fins i tot les persones que no són usuàries del lloc?';
$string['Type'] = 'Tipus';
$string['publiclyvisible'] = 'Visible públicament';
$string['Public'] = 'Públic';
$string['usersautoadded'] = 'Addició automàtica de nous usuaris?';
$string['usersautoaddeddescription'] = 'Voleu que els nous usuaris pertanyin automàticament a aquest grup?';
$string['groupcategory'] = 'Categoria del grup';
$string['allcategories'] = 'Totes les categories';
$string['groupoptionsset'] = 'S\'han actualitzat les opcions del grup.';
$string['nocategoryselected'] = 'No s\'ha seleccionat cap categoria';
$string['categoryunassigned'] = 'No assignar categoria';
$string['hasrequestedmembership'] = 'ha sol·licitat pertànyer al grup';
$string['hasbeeninvitedtojoin'] = 'ha estat convidat a unir-se al grup';
$string['groupinvitesfrom'] = 'Invitat a unir-se:';
$string['requestedmembershipin'] = 'Sol·licitada la pertinença a:';
$string['viewnotify'] = 'Veure notificacions';
$string['viewnotifydescription'] = 'Si marqueu la casella s\'enviarà una notificació als membres del grup cada vegada que un d\'ells comparteixi una de les seves pàgines amb la resta del grup. Quan el grup és molt gran habilitar aquesta opció  generarà un gran nombre de notificacions.';

$string['editgroupmembership'] = 'Edita la pertinença del grup';
$string['editmembershipforuser'] = 'Edita la pertinença de %s';
$string['changedgroupmembership'] = 'S\'ha actualitzat correctament la pertinença al grup.';
$string['changedgroupmembershipsubject'] = 'S\'ha canviat la pertinença al vostre grup';
$string['addedtogroupsmessage'] = "%s us ha afegit al(s) grup(s):\n\n%s\n\n";
$string['removedfromgroupsmessage'] = "%s us ha tret del(s) grup(s):\n\n%s\n\n";
$string['cantremoveuserisadmin'] = "El Tutor no pot treure ni als administradors i ni als altres tutors.";
$string['cantremovemember'] = "El Tutor no pot treure membres.";
$string['current'] = "Actual";
$string['requests'] = "Sol·licituds";
$string['invites'] = "Invitacions";

// Used to refer to all the members of a group - NOT a "member" group role!
$string['member'] = 'membre';
$string['members'] = 'membres';
$string['Members'] = 'Membres';

$string['memberrequests'] = 'Sol·licituds de Pertinença';
$string['declinerequest'] = 'Declina sol·licitud';
$string['submittedviews'] = 'Pàgines enviades';
$string['releaseview'] = 'Publica la pàgina';
$string['invite'] = 'Invita';
$string['remove'] = 'Esborra';
$string['updatemembership'] = 'Actualitza els membres';
$string['memberchangefailed'] = 'No s\'ha pogut actualitzar alguna informació sobre els membres.';
$string['memberchangesuccess'] = 'S\'ha actualitzat correctament l\'estatus de Pertinença.';
$string['viewreleasedsubject'] = 'S\'ha publicat la pàgina';
$string['viewreleasedmessage'] = '%s us ha retornat la pàgina que havíeu publicat pel grup %s';
$string['viewreleasedsuccess'] = 'S\'ha publicat correctament la pàgina';
$string['groupmembershipchangesubject'] = 'Pertinença al grup: %s';
$string['groupmembershipchangedmessagetutor'] = 'Heu estat promocionat a tutor d\'aquest grup';
$string['groupmembershipchangedmessagemember'] = 'Heu estat destituït com a tutor d\'aquest grup';
$string['groupmembershipchangedmessageremove'] = 'Heu estat esborrat d\'aquest grup';
$string['groupmembershipchangedmessagedeclinerequest'] = 'S\'ha declinat la vostra sol·licitud de Pertinença a aquest grup';
$string['groupmembershipchangedmessageaddedtutor'] = 'Se us ha afegit com a tutor d\'aquest grup';
$string['groupmembershipchangedmessageaddedmember'] = 'Se us ha afegit com a membre d\'aquest grup';
$string['leavegroup'] = 'Abandona aquest grup';
$string['joingroup'] = 'Uneix-te a aquest grup';
$string['requestjoingroup'] = 'Sol·licita unir-te a aquest grup';
$string['grouphaveinvite'] = 'Heu estat convidats a unir-vos a aquest grup';
$string['grouphaveinvitewithrole'] = 'Heu estat convidats a unir-vos a aquest grup amb el rol de';
$string['groupnotinvited'] = 'No heu estat convidats a unir-vos a aquest grup';
$string['groupinviteaccepted'] = 'S\'ha acceptat la invitació. Ara sou membre d\'aquest grup.';
$string['groupinvitedeclined'] = 'S\'ha declinat la invitació.!';
$string['acceptinvitegroup'] = 'Accepto';
$string['declineinvitegroup'] = 'Declino';
$string['leftgroup'] = 'Heu deixat aquest grup';
$string['leftgroupfailed'] = 'No s\'ha completat la operació de deixar aquest grup';
$string['couldnotleavegroup'] = 'No podeu abandonar aquest grup';
$string['joinedgroup'] = 'Ara sou membre del grup';
$string['couldnotjoingroup'] = 'No us podeu unir a aquest grup';
$string['grouprequestsent'] = 'S\'ha enviat la sol·licitud de Pertinença al grup';
$string['couldnotrequestgroup'] = 'No s\'ha pogut enviar la sol·licitud de Pertinença al grup';
$string['cannotrequestjoingroup'] ='No podeu sol·licitar la Pertinença a aquest grup';
$string['groupjointypeopen'] = 'La Pertinença a aquest grup és oberta. Uniu-vos-hi!';
$string['groupjointypecontrolled'] = 'La Pertinença a aquest grup és controlada. No us podeu unir a aquest grup.';
$string['groupjointypeinvite'] = 'La Pertinença a aquest grup és només per invitació.';
$string['groupjointyperequest'] = 'La Pertinença a aquest grup és només per sol·licitud.';
$string['grouprequestsubject'] = 'Sol·licitud de Pertinença al grup';
$string['grouprequestmessage'] = '%s vol unir-se al vostre grup %s';
$string['grouprequestmessagereason'] = "%s vol unir-se al vostre grup %s. La seva raó és:\n\n%s";
$string['cantdeletegroup'] = 'No podeu esborrar aquest grup';
$string['groupconfirmdelete'] = 'Esteu segur que voleu esborrar aquest grup?';
$string['deletegroup'] = 'S\'ha esborrat correctament el grup';
$string['deletegroup1'] = 'Esborra el Grup';
$string['allmygroups'] = 'Tots els meus grups';
$string['groupsimin']  = 'Tots els grups als que pertanyo';
$string['groupsiown']  = 'Grups que tinc';
$string['groupsiminvitedto'] = 'Grups on estic convidat';
$string['groupsiwanttojoin'] = 'Grups als que em vull unir';
$string['requestedtojoin'] = 'Heu demanat unir-vos a aquest grup';
$string['groupnotfound'] = 'No s\'ha trobat el grup amb id %s';
$string['groupconfirmleave'] = 'Esteu segur que voleu abandonar aquest grup?';
$string['cantleavegroup'] = 'No podeu abandonar aquest grup';
$string['usercantleavegroup'] = 'Aquest usuari no pot abandonar aquest grup';
$string['usercannotchangetothisrole'] = 'L\'usuari no pot accedir a aquest rol';
$string['leavespecifiedgroup'] = 'Deixa el grup \'%s\'';
$string['memberslist'] = 'Membres: ';
$string['nogroups'] = 'Cap grup';
$string['deletespecifiedgroup'] = 'Esborra el grup \'%s\'';
$string['requestjoinspecifiedgroup'] = 'Sol·licitud per unir-se al grup \'%s\'';
$string['youaregroupmember'] = 'Sou membre d\'aquest grup';
$string['youowngroup'] = 'Aquest grup és vostre';
$string['youaregrouptutor'] = 'Sou tutor en aquest grup';
$string['youaregroupadmin'] = 'Sou administrador en aquest grup';
$string['groupsnotin'] = 'Grups als que no pertanyo';
$string['allgroups'] = 'Tots els grups';
$string['allgroupmembers'] = 'Tots els membres del grup';
$string['trysearchingforgroups'] = 'Proveu a %scercar grups%s on unir-vos.';
$string['nogroupsfound'] = 'No s\'ha trobat cap grup :(';
$string['group'] = 'grup';
$string['Group'] = 'Grup';
$string['groups'] = 'grups';
$string['notamember'] = 'No sou membre d\'aquest grup';
$string['notmembermayjoin'] = 'Per poder veure aquesta pàgina cal que us uniu al grup \'%s\' .';
$string['declinerequestsuccess'] = 'S\'ha declinat la sol·licitud de pertinença.';
$string['notpublic'] = 'Aquest grup no és públic.';
$string['moregroups'] = 'Més grups';

// Bulk add, invite
$string['addmembers'] = 'Afegeix membres';
$string['invitationssent'] = 'S\'han enviat %d invitacions';
$string['newmembersadded'] = 'S\'han afegit %d membres nous';
$string['potentialmembers'] = 'Membres potencials';
$string['sendinvitations'] = 'Envia invitacions';
$string['userstobeadded'] = 'Usuaris per afegir';
$string['userstobeinvited'] = 'Usuaris per invitar';

// friendslist
$string['reasonoptional'] = 'Raó (opcional)';
$string['request'] = 'Sol·licita';

$string['friendformaddsuccess'] = 'S\'ha afegit %s a la vostra llista d\'amics';
$string['friendformremovesuccess'] = 'S\'ha esborrat a %s de la vostra llista d\'amics';
$string['friendformrequestsuccess'] = 'Envia una sol·licitud d\'amistat a %s';
$string['friendformacceptsuccess'] = 'Sol·licitud d\'amistat acceptada';
$string['friendformrejectsuccess'] = 'Sol·licitud d\'amistat rebutjada';

$string['addtofriendslist'] = 'Afegeix a amics';
$string['requestfriendship'] = 'Sol·licitud d\'amistat';

$string['addedtofriendslistsubject'] = 'Amic nou';
$string['addedtofriendslistmessage'] = 'S\'ha afegit %s com amic vostre. Això vol dir que %s és ara a la vostra llista d\'amics. '
    . ' Feu clic al següent enllaç per veure el seu perfil';

$string['requestedfriendlistsubject'] = 'Sol·licitud de nova amistat';
$string['requestedfriendlistmessage'] = '%s us demana que l\'afegiu com amic.  '
    .' Ho podeu fer des de l\'enllaç següent o des de la pàgina amb la llista d\'amics.';

$string['requestedfriendlistmessagereason'] = '%s us ha demanat que l\'afegiu com amic.'
    . ' Ho podeu fer des de l\'enllaç següent o des de la pàgina amb la llista d\'amics.'
    . ' La seva raó és:
    ';

$string['removefromfriendslist'] = 'Esborra\'l com amic';
$string['removefromfriends'] = 'Esborra %s de la llista d\'amics';
$string['confirmremovefriend'] = 'Esteu segur que voleu esborrar aquest usuari de la llista d\'amics?';
$string['removedfromfriendslistsubject'] = 'Esborrat de la llista d\'amics';
$string['removedfromfriendslistmessage'] = '%s us ha esborrat de la seva llista d\'amics.';
$string['removedfromfriendslistmessagereason'] = '%s us ha esborrat de la seva llista d\'amics. La raó és: ';
$string['cantremovefriend'] = 'No podeu esborrar aquest usuari de la llista d\'amics';

$string['friendshipalreadyrequested'] = 'Heu sol·licitat ser afegit a la llista d\'amics de %s';
$string['friendshipalreadyrequestedowner'] = '%s ha sol·licitat ser afegit a la vostra llista d\'amics';
$string['rejectfriendshipreason'] = 'Raó per rebutjar la sol·licitud';
$string['alreadyfriends'] = 'Ja sou amic de %s';

$string['friendrequestacceptedsubject'] = 'S\'ha acceptat la sol·licitud d\'amistat';
$string['friendrequestacceptedmessage'] = '%s ha acceptat la vostra sol·licitud d\'amistat i ara sou a la seva llista d\'amics';
$string['friendrequestrejectedsubject'] = 'S\'ha rebutjat la vostra sol·licitud d\'amistat';
$string['friendrequestrejectedmessage'] = '%s ha rebutjat la vostra sol·licitud d\'amistat.';
$string['friendrequestrejectedmessagereason'] = '%s ha rebutjat la vostra sol·licitud d\'amistat. La raó és: ';

$string['allfriends']     = 'Tots els amics';
$string['currentfriends'] = 'Amics actuals';
$string['pendingfriends'] = 'Amics pendents';
$string['backtofriendslist'] = 'Torna a la llista d\'amics';
$string['findnewfriends'] = 'Cerca nous amics';
$string['Views']          = 'Pàgines del Grup';// $string['Views']          = 'Pàgines';
$string['Files']          = 'Fitxers del Grup';
$string['seeallviews']    = 'Veure totes les pàgines de %s';
$string['noviewstosee']   = 'No hi ha res que pugueu veure :(';
$string['whymakemeyourfriend'] = 'Aquesta és la raó per la qual m\'hauríeu de fer amistat:';
$string['approverequest'] = 'Aprova la sol·licitud';
$string['denyrequest']    = 'Denega la sol·licitud';
$string['pending']        = 'pendent';
$string['trysearchingforfriends'] = 'Proveu a %scercar amics%s per eixamplar la vostra xarxa';
$string['nobodyawaitsfriendapproval'] = 'Ningú espera la vostra aprovació per ser amic.';
$string['sendfriendrequest'] = 'Envia sol·licitud d\'amistat';
$string['addtomyfriends'] = 'Afegeix a Els meus amics';
$string['friendshiprequested'] = 'S\'ha sol·licitat amistat';
$string['existingfriend'] = 'Aquest usuari ja és amic';
$string['nosearchresultsfound'] = 'No s\'ha trobat cap resultat :(';
$string['friend'] = 'amic';
$string['friends'] = 'amics';
$string['user'] = 'usuari';
$string['users'] = 'usuaris';
$string['Friends'] = 'Amics';

$string['friendlistfailure'] = 'No s\'ha pogut modificar la vostra llista d\'amics';
$string['userdoesntwantfriends'] = 'Aquest usuari no desitja cap més amic';
$string['cannotrequestfriendshipwithself'] = 'No podeu demanar-vos amistat a vós mateix';
$string['cantrequestfriendship'] = 'No podeu demanar amistat amb aquest usuari';

// Messaging between users
$string['messagebody'] = 'Envia missatge'; // wtf
$string['sendmessage'] = 'Envia missatge';
$string['messagesent'] = 'Missatge enviat';
$string['messagenotsent'] = 'No s\'ha pogut enviar el missatge';
$string['newusermessage'] = 'Teniu un nou missatge de %s';
$string['newusermessageemailbody'] = '%s us ha enviat un missatge. Per veure\'l visiteu

%s';
$string['sendmessageto'] = 'Envia un missatge a %s';
$string['viewmessage'] = 'Veure missatge';
$string['Reply'] = 'Respon';

$string['denyfriendrequest'] = 'Denegueu la sol·licitud d\'amistat';
$string['sendfriendshiprequest'] = 'Envia a %s una sol·licitud d\'amistat';
$string['cantdenyrequest'] = 'Això no és una sol·licitud d\'amistat vàlida';
$string['cantmessageuser'] = 'No podeu enviar un missatge a aquest usuari';
$string['cantviewmessage'] = 'No podeu veure aquest missatge';
$string['requestedfriendship'] = 'S\'ha sol·licitat amistat';
$string['notinanygroups'] = 'No en cap grup';
$string['addusertogroup'] = 'Afegeix a ';
$string['inviteusertojoingroup'] = 'Convida a ';
$string['invitemembertogroup'] = 'Convida a %s a unir-se a \'%s\'';
$string['useralreadyinvitedtogroup'] = 'Aquest usuari o ja ha sigut convidat o bé ja és membre d\'aquest grup.';
$string['cannotinvitetogroup'] = 'No podeu convidar aquest usuari a aquest grup';
$string['removefriend'] = 'Amic esborrat';
$string['denyfriendrequestlower'] = 'Denega la sol·licitud d\'amistat';

// Group interactions (activities)
$string['groupinteractions'] = 'Activitats del grup';
$string['nointeractions'] = 'No hi ha activitats en aquest grup';
$string['notallowedtoeditinteractions'] = 'No esteu autoritzat a afegir o editar les activitats d\'aquest grup';
$string['notallowedtodeleteinteractions'] = 'No esteu autoritzat a esborrar activitats d\'aquest grup';
$string['interactionsaved'] = 'S\'ha desat correctament %s';
$string['deleteinteraction'] = 'Esborra %s \'%s\'';
$string['deleteinteractionsure'] = 'Esteu segur que voleu fer això? Aquesta acció no es podrà desfer.';
$string['interactiondeleted'] = 'S\'ha esborrat correctament %s';
$string['addnewinteraction'] = 'Afegeix nou %s';
$string['title'] = 'Títol';
$string['Role'] = 'Rol';
$string['changerole'] = 'Canvia de rol';
$string['changeroleofuseringroup'] = 'Canvia el rol de %s a %s';
$string['currentrole'] = 'Rol actual';
$string['changeroleto'] = 'Canvia el rol a';
$string['rolechanged'] = 'S\'ha canviat el rol';
$string['removefromgroup'] = 'Esborra del grup';
$string['userremoved'] = 'S\'ha esborrat l\'usuari';
$string['About'] = 'Sobre';
$string['aboutgroup'] = 'Sobre %s';

$string['Joined'] = 'Adherit';

$string['membersdescription:invite'] = 'Aquest és un grup de <em>només-per-invitació</em>. Podeu convidar-hi usuaris a través de la seva pàgina del perfil o <a href="%s">enviant invitacions a tots de cop</a>.';
$string['membersdescription:controlled'] = 'Aquest és un grup de <em>pertinença controlada</em>. Podeu afegir-hi usuaris a través de la seva pàgina del perfil o <a href="%s">afegint usuaris de cop</a>.';

// View submission
$string['submit'] = 'Tramet';
$string['allowssubmissions'] = 'Permet trameses';

?>
