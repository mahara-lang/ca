<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana: Joan Queralt Gil jqueralt a gmail.com
 */

defined('INTERNAL') || die();

$string['allmydata'] = 'Totes les meves dades';
$string['chooseanexportformat'] = 'Escolliu un format d\'exportació';
$string['clicktopreview'] = 'Feu clic per previsualitzar';
$string['collectionstoexport'] = 'Col·leccions per exportar';
$string['creatingzipfile'] = 'S\'està creant el fitxer comprimit';
$string['Done'] = 'Fet';
$string['Export']     = 'Exporta';
$string['exportgeneratedsuccessfully'] = 'S\'ha generat correctament el fitxer d\'exportació. %sCliqueu aquí per descarregar-lo%s';
$string['exportgeneratedsuccessfullyjs'] = 'S\'ha generat correctament el fitxer d\'exportació. %sContinua%s';
$string['exportingartefactplugindata'] = 'S\'estan exportant les dades dels artefactes connectors';
$string['exportingartefacts'] = 'S\'estan exportant els artefactes';
$string['exportingartefactsprogress'] = 'S\'estan exportant els artefactes: %s/%s';
$string['exportingfooter'] = 'S\'està exportant el peu';
$string['exportingviews'] = 'S\'estan exportant les pàgines';
$string['exportingviewsprogress'] = 'S\'estan exportant les pàgines: %s/%s';
$string['exportpagedescription'] = 'Des d\'aquí podeu exportar el vostre ePortafolis. Aquesta eina exportarà tot la informació del vostre ePortafolis i les Pàgines, però no exportarà les vostres preferències per al lloc.';
$string['exportyourportfolio'] = 'Exporteu el vostre ePortafolis';
$string['generateexport'] = 'Exporta';
$string['noexportpluginsenabled'] = 'L\'administrador del lloc no ha configurat cap connector d\'exportació així que no podreu utilitzar aquesta característica';
$string['justsomecollections'] = 'Només algunes col·leccions';
$string['justsomeviews'] = 'Només algunes Pàgines';
$string['nonexistentfile'] = "S\'ha provat d\'afegir un fitxer que no existeix: '%s'";
$string['pleasewaitwhileyourexportisbeinggenerated'] = 'Espereu mentre s\'exporta...';
$string['reverseselection'] = 'Inverteix la selecció';
$string['selectall'] = 'Selecciona-ho tot';
$string['setupcomplete'] = 'Configuració completa';
$string['Starting'] = 'Començant';
$string['unabletoexportportfoliousingoptions'] = 'El vostre ePortafolis no es pot exportar amb aquestes opcions';
$string['unabletogenerateexport'] = 'No es pot realitzar l\'exportació';
$string['viewstoexport'] = 'Pàgines per exportar';
$string['whatdoyouwanttoexport'] = 'Què voleu exportar?';
$string['writingfiles'] = 'S\'estan escrivint els fitxers';
$string['youarehere'] = 'Sou aquí';
$string['youmustselectatleastonecollectiontoexport'] = 'Heu de seleccionar com a mínim una col·lecció per exportar';
$string['youmustselectatleastoneviewtoexport'] = 'Heu de seleccionar com a mínim una Pagina per exportar';
$string['zipnotinstalled'] = 'El vostre sistema no té el comandament de compressió zip. Instal·leu-lo per activar aquesta característica.';

?>
