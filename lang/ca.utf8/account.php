<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage core
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */


defined('INTERNAL') || die();

$string['changepassworddesc'] = 'Escriviu aquí la vostra informació si voleu canviar la contrasenya';
$string['changepasswordotherinterface'] = 'Podeu  <a href="%s">canviar la contrasenya</a> des d\'una altra interfície</a>';
$string['oldpasswordincorrect'] = 'Aquesta no és la contrasenya actual';

$string['changeusernameheading'] = 'Canvi de nom d\'usuari';
$string['changeusername'] = 'Nou nom d\'usuari';
$string['changeusernamedesc'] = 'El nom d\'usuari que usareu per entrar a %s.  Els noms d\'usuari han de tenir entre 3 i 30 caràcters i poden contenir lletres, xifres i els símbols més comuns, tret de l\'espai en blanc.';

$string['usernameexists'] = 'Aquest nom d\'usuari  ja existeix. Trieu-ne un altre.';

$string['accountoptionsdesc'] = 'Aquí podeu configurar les opcions generals del compte';
$string['friendsnobody'] = 'Ningú no em podrà afegir com amic';
$string['friendsauth'] = 'Els amics nous necessiten la meva autorització';
$string['friendsauto'] = 'Els amics nous són autoritzats automàticament';
$string['friendsdescr'] = 'Control d\'amics';
$string['updatedfriendcontrolsetting'] = 'Actualitza el control d\'amics';

$string['wysiwygdescr'] = 'Editor HTML';
$string['on'] = 'Activat';
$string['off'] = 'Desactivat';
$string['disabled'] = 'Deshabilitat';
$string['enabled'] = 'Habilitat';

$string['messagesdescr'] = 'Missatges d\'altres usuaris';
$string['messagesnobody'] = 'No permetis que ningú m\'enviï missatges';
$string['messagesfriends'] = 'Permet que només les persones de la llista d\'Amics m\'enviïn missatges';
$string['messagesallow'] = 'Permet que tothom m\'enviï missatges';

$string['language'] = 'Llengua';

$string['showviewcolumns'] = 'Quan editi un Pàgina mostra els controls per afegir o treure columnes';

$string['tagssideblockmaxtags'] = 'Nombre màxim d\'etiquetes al núvol';
$string['tagssideblockmaxtagsdescription'] = 'Nombre màxim d\'etiquetes per mostrar al vostre Núvol d\'etiquetes';

$string['enablemultipleblogs'] = 'Activa més d\un Diari';
$string['enablemultipleblogsdescription']  = 'Per defecte teniu un Diari. Si en voleu més d\'un marqueu aquesta opció.';
$string['disablemultipleblogserror'] = 'No podeu deshabilitar l\'opció <em>Més d\'un diari</em> a no ser que només tinguéssiu un sol diari';

$string['hiderealname'] = 'Amaga el nom real';
$string['hiderealnamedescription'] = 'Marqueu aquesta opció si heu triat mostrar un altre nom i no voleu que altres usuaris us puguin trobar pel nom real quan facin una cerca.';

$string['showhomeinfo'] = 'Mostra informació sobre l\'eportafolis Mahara a la pàgina d\'inici';

$string['mobileuploadtoken'] = 'Token per la càrrega des del Mòbil';
$string['mobileuploadtokendescription'] = 'Escriviu el token aquí i al telèfon mòbil per activar les càrregues <br /><b>Nota</b>: canviarà de forma automàtica després de cada càrrega <br/> Si teniu problemes - simplement restabliu-lo aquí i al telèfon.';

$string['prefssaved']  = 'S\'han desat les preferències';
$string['prefsnotsaved'] = 'No s\'han pogut desar les preferències!';

$string['maildisabled'] = 'Correu electrònic deshabilitat';
$string['maildisabledbounce'] =<<< EOF
S\'ha deshabilitat l\'enviament de missatges a la vostra bústia de correu electrònic perquè eren retornats al servidor.
Comproveu que el vostre correu electrònic està funcionant correctament abans de tornar a habilitar aquesta opció a les vostres preferències.
EOF;
$string['maildisableddescription'] = 'S\'ha deshabilitat l\'enviament de missatges a la vostra bústia de correu electrònic. Podeu <a href="%s">tornar a habilitar el correu</a> des de la pàgina de preferències del compte.';

$string['deleteaccount']  = 'Elimina el compte';
$string['deleteaccountdescription']  = 'Si elimineu el vostre propi compte els altres usuaris ja no podran veure ni la vostra informació del perfil ni les vistes. Tanmateix les entrades de fòrum que haguéssiu pogut fer encara es veuran però sense el nom de l\'autor.';
$string['accountdeleted']  = 'S\'ha eliminat el vostre compte.';
?>
