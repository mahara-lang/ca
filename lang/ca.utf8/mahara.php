<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */


defined('INTERNAL') || die();

// General form strings
$string['add']     = 'Afegeix';
$string['cancel']  = 'Cancel·la';
$string['delete']  = 'Esborra';
$string['edit']    = 'Edita';
$string['editing'] = 'Editant';
$string['save']    = 'Desa';
$string['submit']  = 'Envia';
$string['update']  = 'Actualitza';
$string['change']  = 'Canvia';
$string['send']    = 'Envia';
$string['go']      = 'Ves a';
$string['default'] = 'Per defecte';
$string['upload']  = 'Carrega';
$string['complete']  = 'Completa';
$string['Failed']  = 'Ha fallat';
$string['loading'] = 'Carregant ...';
$string['showtags'] = 'Mostra les meves etiquetes';
$string['errorprocessingform'] = 'S\'ha produït un error enviant aquest formulari. Si us plau comproveu els camps marcats i torneu a provar-ho.';
$string['description'] = 'Descripció';
$string['remove']  = 'Esborra';
$string['Close'] = 'Tanca';
$string['Help'] = 'Ajuda';
$string['applychanges'] = 'Aplica els canvis';

$string['no']     = 'No';
$string['yes']    = 'Sí';
$string['none']   = 'Cap';
$string['at'] = 'a';
$string['From'] = 'De';
$string['To'] = 'A';
$string['All'] = 'Tot';

$string['enable'] = 'Activa';
$string['disable'] = 'Desactiva';
$string['show'] = 'Mostra';
$string['hide'] = 'Amaga';
$string['pluginenabled'] = 'Connector activat';
$string['plugindisabled'] = 'Connector desactivat';
$string['pluginnotenabled'] = 'Connector no activat. Primer heu d\'activar el connector %s.';
$string['pluginexplainaddremove'] = 'A Mahara els connectors estan sempre instal·lats i s\'hi pot accedir si l\'usuari coneix la URL. En lloc d\'habilitar i deshabilitar la funcionalitat, els plugins estan amagats o es fan visibles tot fent clic a l\'enllaç <i>Amaga</i> o <i>Mostra</i> al costat dels següents connectors.';
$string['pluginexplainartefactblocktypes'] = 'Quan s\'amaga un connector del tipus <i>artefacte</i> el sistema de Mahara també deixa de mostrar els blocs relacionats amb ell.';

$string['next']      = 'Següent';
$string['nextpage']  = 'Pàgina següent';
$string['previous']  = 'Anterior';
$string['prevpage']  = 'Pàgina anterior';
$string['first']     = 'Primera';
$string['firstpage'] = 'Primera pàgina';
$string['last']      = 'Última';
$string['lastpage']  = 'Última pàgina';

$string['accept'] = 'Accepta';
$string['memberofinstitutions'] = 'Membre de %s';
$string['reject'] = 'Rebutja';
$string['sendrequest'] = 'Envia sol·licitud';
$string['reason'] = 'Raó';
$string['select'] = 'Selecciona';

// Tags
$string['tags'] = 'Etiquetes';
$string['tagsdesc'] = 'Escriviu els noms de les etiquetes separats per comes.';
$string['tagsdescprofile'] = 'Escriviu els noms de les etiquetes separats per comes. A la barra lateral veureu els ítems etiquetats amb \'profile\' .';
$string['youhavenottaggedanythingyet'] = 'Encara no heu etiquetat res';
$string['mytags'] = 'Les meves etiquetes';
$string['Tag'] = 'Etiqueta';
$string['itemstaggedwith'] = 'Ítems etiquetats amb "%s"';
$string['numitems'] = '%s ítems';
$string['searchresultsfor'] = 'Cerca resultats per';
$string['alltags'] = 'Totes les etiquetes';
$string['sortalpha'] = 'Ordena alfabèticament les etiquetes';
$string['sortfreq'] = 'Ordena les etiquetes per freqüència';
$string['sortresultsby'] = 'Ordena els resultats per:';
$string['date'] = 'Data';
$string['dateformatguide'] = 'Usa el format AAAA/MM/DD';
$string['datetimeformatguide'] = 'Usa el format AAAA/MM/DD HH:MM';
$string['filterresultsby'] = 'Filtra els resultats per:';
$string['tagfilter_all'] = 'Tot';
$string['tagfilter_file'] = 'Fitxers';
$string['tagfilter_image'] = 'Imatges';
$string['tagfilter_text'] = 'Text';
$string['tagfilter_view'] = 'Pàgines';
$string['edittags'] = 'Edita etiquetes';
$string['selectatagtoedit'] = 'Selecciona una etiqueta per editar';
$string['edittag'] = 'Edita <a href="%s">%s</a>';
$string['editthistag'] = 'Edita aquesta etiqueta';
$string['edittagdescription'] = 'S\'actualitzaran tots els ítems de l\'eportafolis etiquetats amb "%s"';
$string['deletetag'] = 'Esborra <a href="%s">%s</a>';
$string['confirmdeletetag'] = 'Esteu segur que voleu treure aquesta etiqueta de tot el contingut del vostre eportafolis?';
$string['deletetagdescription'] = 'Treu aquesta etiqueta de tot el contingut del meu eportafolis';
$string['tagupdatedsuccessfully'] = 'S\'ha actualitzat correctament l\'etiqueta';
$string['tagdeletedsuccessfully'] = 'S\'ha esborrat correctament l\'etiqueta';

$string['selfsearch'] = 'Cerca al Meu ePortafolis';

// Quota strings
$string['quota'] = 'Quota';
$string['quotausage'] = 'Esteu fent servir <span id="quota_used">%s</span> dels vostres <span id="quota_total">%s</span> de quota.';

$string['updatefailed'] = 'Na s\'ha pogut actualitzar';

$string['strftimenotspecified']  = 'No s\'ha especificat';

// profile sideblock strings
$string['invitedgroup'] = 'grup invitat a';
$string['invitedgroups'] = 'grups invitats a';
$string['logout'] = 'Sortida';
$string['pendingfriend'] = 'amic pendent';
$string['pendingfriends'] = 'amics pendents';
$string['profile'] = 'perfil';
$string['views'] = 'Pàgines';

// Online users sideblock strings
$string['onlineusers'] = 'Usuaris connectats';
$string['lastminutes'] = 'Darrers %s minuts';

// Links and resources sideblock
$string['linksandresources'] = 'Enllaços i recursos';

// auth
$string['accessforbiddentoadminsection'] = 'No esteu autoritzat a entrar en la secció d\'administració';
$string['accountdeleted'] = 'S\'ha eliminat el vostre compte';
$string['accountexpired'] = 'El vostre compte ha caducat';
$string['accountcreated'] = '%s: Compte nou';
$string['accountcreatedtext'] = 'Apreciat/da %s,

S\'ha creat un compte nou per vos  a %s amb aquestes dades:

Nom d\'usuari: %s
Contrasenya: %s

Aneu a %s i entreu-hi.

Salutacions, %s Administrador del lloc';
$string['accountcreatedchangepasswordtext'] = 'Apreciat/da %s,

S\'ha creat un compte nou per vos  a %s amb aquestes dades:

Nom d\'usuari: %s
Contrasenya: %s

Un cop us hi hagueu identificat per primera vegada haureu de canviar la contrasenya.

Aneu a %s i entreu-hi.

Salutacions, %s Administrador del lloc';
$string['accountcreatedhtml'] = '<p>Apreciat/da %s,</p>

<p>S\'ha creat un compte nou per vos  a <a href="%s">%s</a> amb aquestes dades:</p>

<ul>
    <li><strong>Nom d\'usuari:</strong> %s</li>
    <li><strong>Contrasenya:</strong> %s</li>
</ul>

<p>Aneu a <a href="%s">%s</a> i entreu-hi.</p>

<p>Salutacions, %s Administrador del lloc</p>
';
$string['accountcreatedchangepasswordhtml'] = '<p>Apreciat/da %s,</p>

<p>S\'ha creat un compte nou per vos  a <a href="%s">%s</a> amb aquestes dades:</p>

<ul>
    <li><strong>Nom d\'usuari:</strong> %s</li>
    <li><strong>Contrasenya:</strong> %s</li>
</ul>

<p>Un cop us hi hagueu identificat per primera vegada haureu de canviar la contrasenya.</p>

<p>Aneu a <a href="%s">%s</a> i entreu-hi.</p>

<p>Salutacions, %s Administrador del lloc</p>
';
$string['accountexpirywarning'] = 'Avís de venciment del compte';
$string['accountexpirywarningtext'] = 'Apreciat/da %s,

El vostre compte a  %s vencerà en %s.

Us recomanem que deseu el contingut del vostre eportafolis amb l\'ajut de l\'eina d\'Exportació. A la  Guia de l\'usuari  trobareu instruccions sobre com utilitzar-la.

Si voleu allargar el vostre accés al compte o necessiteu algun aclariment sobre aquest missatge, contacteu amb nosaltres:

%s

Salutacions, %s Administrador del lloc';
$string['accountexpirywarninghtml'] = '<p>Apreciat/da %s,</p>

<p>El vostre compte a  %s vencerà en %s.</p>

<p>Us recomanem que deseu el contingut del vostre portafolis amb l\'ajut de l\'eina d\'Exportació. A la  Guia de l\'usuari  trobareu instruccions sobre com utilitzar-la.</p>

<p>Si voleu allargar el vostre accés al compte o necessiteu algun aclariment sobre aquest missatge, <a href="%s">contacteu amb nosaltres </a>.</p>

<p>Salutacions, %s Administrador del lloc</p>';
$string['institutionexpirywarning'] = 'Institution membership expiry warning';
$string['institutionexpirywarningtext'] = 'Dear %s,

Your membership of %s on %s will expire within %s.

If you wish to extend your membership or have any questions regarding the above, please feel free to contact us:

%s

Regards, %s Site Administrator';
$string['institutionexpirywarninghtml'] = '<p>Dear %s,</p>

<p>Your membership of %s on %s will expire within %s.</p>

<p>If you wish to extend your membership or have any questions regarding the above, please feel free to <a href="%s">Contact Us</a>.</p>

<p>Regards, %s Site Administrator</p>';
$string['accountinactive'] = 'Sorry, your account is currently inactive';
$string['accountinactivewarning'] = 'Account inactivity warning';
$string['accountinactivewarningtext'] = 'Dear %s,

Your account on %s will become inactive within %s.

Once inactive, you will not be able to log in until an administrator re-enables your account.

You can prevent your account from becoming inactive by logging in.

Regards, %s Site Administrator';
$string['accountinactivewarninghtml'] = '<p>Dear %s,</p>

<p>Your account on %s will become inactive within %s.</p>

<p>Once inactive, you will not be able to log in until an administrator re-enables your account.</p>

<p>You can prevent your account from becoming inactive by logging in.</p>

<p>Regards, %s Site Administrator</p>';

































$string['accountsuspended'] = 'El vostre compte ha estat suspès a partir de %s a causa de:<blockquote>%s</blockquote>';
$string['youraccounthasbeensuspended'] = 'El vostre compte ha estat suspès';
$string['youraccounthasbeenunsuspended'] = 'S\'ha aixecat la suspensió del vostre compte';
$string['changepasswordinfo'] = 'Heu de canviar la contrasenya per poder continuar.';
$string['chooseusernamepassword'] = 'Escolliu un nom d\'usuari i contrasenya';
$string['chooseusernamepasswordinfo'] = 'Us cal un nom d\'usuari i una contrasenya per registrar-vos a  %s.  Escolliu-los ara.';
$string['confirmpassword'] = 'Confirmeu la contrasenya';
$string['javascriptnotenabled'] = 'El vostre navegador no té activat el Javascript per aquest lloc web. Cal que l\'activeu per poder-hi entrar.';
$string['cookiesnotenabled'] = 'El vostre navegador no té les cookies activades o està blocant les cookies d\'aquest lloc. Cal que activeu les cookies per poder-hi entrar.';
$string['institution'] = 'Institució';
$string['loggedoutok'] = 'Heu sortit satisfactòriament';
$string['login'] = 'Entrada';
$string['loginfailed'] = 'No heu donat la clau correcta per entrar. Comproveu que el vostre nom d\'usuari i contrasenya són els correctes.';
$string['loginto'] = 'He entrat a %s';
$string['newpassword'] = 'Contrasenya nova';
$string['nosessionreload'] = 'Actualitzeu la pàgina per entrar';
$string['oldpassword'] = 'Contrasenya actual';
$string['password'] = 'Contrasenya';
$string['passworddescription'] = ' ';
$string['passwordhelp'] = 'La contrasenya que useu normalment per entrar al sistema';
$string['passwordnotchanged'] = 'No heu canviat la contrasenya, heu de triar-ne una de nova';
$string['passwordsaved'] = 'S\'ha desat la nova contrasenya';
$string['passwordsdonotmatch'] = 'Les contrasenyes no coincideixen';
$string['passwordtooeasy'] = 'Aquesta contrasenya és massa fàcil. Trieu-ne una de més complexa';
$string['register'] = 'Registra';
$string['sessiontimedout'] = 'La vostra sessió ha expirat. Heu de tornar a identificar-vos per poder continuar';
$string['sessiontimedoutpublic'] = 'La vostra sessió ha expirat. Heu de tornar a <a href="%s">identificar-vos</a> per poder continuar';
$string['sessiontimedoutreload'] = 'La vostra sessió ha expirat. Actualitzeu aquesta pàgina per tornar-vos a identificar';
$string['username'] = 'Nom d\'usuari';
$string['preferredname'] = 'Nom preferit';
$string['usernamedescription'] = ' ';
$string['usernamehelp'] = 'El nom d\'usuari que heu donat per entrar al sistema.';
$string['youaremasqueradingas'] = 'Us esteu fent passar per %s.';
$string['yournewpassword'] = 'Contrasenya nova';
$string['yournewpasswordagain'] = 'Repetiu la contrasenya nova';
$string['invalidsesskey'] = 'Invalid session key';
$string['cannotremovedefaultemail'] = 'No podeu esborrar l\'adreça de correu principal';
$string['emailtoolong'] = 'L\'adreça de correu no pot superar els 255 caràcters';
$string['mustspecifyoldpassword'] = 'Heu d\'escriure la vostra contrasenya actual';
$string['Site'] = 'Lloc';

// Misc. register stuff that could be used elsewhere
$string['emailaddress'] = 'Adreça de correu electrònic';
$string['emailaddressdescription'] = ' ';
$string['firstname'] = 'Nom';
$string['firstnamedescription'] = ' ';
$string['lastname'] = 'Cognoms';
$string['lastnamedescription'] = ' ';
$string['studentid'] = 'número ID';
$string['displayname'] = 'Mostra el nom';
$string['fullname'] = 'Nom complet';
$string['registerwelcome'] = 'Benvingut/da! Per gaudir d\'aquest lloc primer cal que us hi registreu.';
$string['registeragreeterms'] = 'També heu d\'acceptar els <a href="terms.php">termes i condicions</a>.';
$string['registerprivacy'] = 'Les dades que recollim es guardaran d\'acord a la nostra <a href="privacy.php">política de privacitat</a>.';
$string['registerstep3fieldsoptional'] = '<h3>Trieu una imatge per al vostre perfil</h3><p>Ara ja esteu correctament registrats a ' . get_config('sitename') . ' Ara podeu escollir una icona opcional que es mostrarà com el vostre avatar.</p>';
$string['registerstep3fieldsmandatory'] = '<h3>Ompliu els camps obligatoris del perfil</h3><p>Els següents camps són obligatoris, els heu d\'omplir per poder completar el registre. </p>';
$string['registeringdisallowed'] = 'Ara no us podeu registrar al sistema';
$string['membershipexpiry'] = 'La inscripció expira';
$string['institutionfull'] = 'La institució que heu triat no accepta més inscripcions.';
$string['registrationnotallowed'] = 'La institució que heu triat no permet les inscripcions.';
$string['registrationcomplete'] = 'Gràcies per registrar-vos a %s';
$string['language'] = 'Idioma';

// Forgot password
$string['cantchangepassword'] = 'No podeu canviar la contrasenya des d\'aquesta interfície. Utilitzeu la de la vostra institució';
$string['forgotusernamepassword'] = 'Heu oblidat el vostre nom d\'usuari o contrasenya?';
$string['forgotusernamepasswordtext'] = 'Si heu oblidat el vostre nom d\'usuari o contrasenya escriviu l\'adreça de correu principal del vostre perfil on rebreu una clau que us permetrà escollir una nova contrasenya';

$string['lostusernamepassword'] = 'Nom d\'usuari o contrasenya perduts';
$string['emailaddressorusername'] = 'Adreça de correu electrònic o nom d\'usuari';
$string['pwchangerequestsent'] = 'Ben aviat rebreu un correu amb un enllaç des d\'on canviar la contrasenya del vostre compte';
$string['forgotusernamepasswordemailsubject'] = 'Username/Password details for %s';
$string['forgotusernamepasswordemailmessagetext'] = 'Dear %s,

A username/password request has been made for your account on %s.

Your username is %s.

If you wish to reset your password, please follow the link below:

%s

If you did not request a password reset, please ignore this email.

If you have any questions regarding the above, please feel free to contact us:

%s

Regards, %s Site Administrator';
$string['forgotusernamepasswordemailmessagehtml'] = '<p>Dear %s,</p>

<p>A username/password request has been made for your account on %s.</p>

<p>Your username is <strong>%s</strong>.</p>

<p>If you wish to reset your password, please follow the link below:</p>

<p><a href="%s">%s</a></p>

<p>If you did not request a password reset, please ignore this email.</p>

<p>If you have any questions regarding the above, please feel free to <a href="%s">contact us</a>.</p>

<p>Regards, %s Site Administrator</p>';
$string['forgotpassemailsendunsuccessful'] = 'A causa de problemes tècnics no s\'ha pogut enviar el missatge de correu. Si us plau, torneu a prova-ho d\'aquí a una estona';
$string['forgotpassemailsentanyway'] = 'S\'ha enviat un correu a l\'adreça registrada de l\'usuari, però o bé l\'adreça no és correcta o bé el servidor està retornant els coreus. Poseu-vos en contacte amb l\'administrador de Mahara per reinicir la contrasenya en cas que no rebessiu el correu.';
$string['forgotpassnosuchemailaddressorusername'] = 'L\'adreça de correu o nom d\'usuari que heu escrit no coincideix amb cap usuari del lloc.';
$string['forgotpasswordenternew'] = 'Escriviu la nova contrasenya per continuar';
$string['nosuchpasswordrequest'] = 'No hi ha cap sol·licitud de contrasenya';
$string['passwordchangedok'] = 'S\'ha canviat correctament la contrasenya';

// Reset password when moving from external to internal auth.
$string['noinstitutionsetpassemailsubject'] = '%s: Inscripció a %s';
$string['noinstitutionsetpassemailmessagetext'] = 'Dear %s,

You are no longer a member of %s.
You may continue to use %s with your current username %s, but you must set a new password for your account.

Please follow the link below to continue the reset process.

%sforgotpass.php?key=%s

If you have any questions regarding the above, please feel free to contact
us.

%scontact.php

Regards, %s Site Administrator

%sforgotpass.php?key=%s';
$string['noinstitutionsetpassemailmessagehtml'] = '<p>Dear %s,</p>

<p>You are no longer a member of %s.</p>
<p>You may continue to use %s with your current username %s, but you must set a new password for your account.</p>

<p>Please follow the link below to continue the reset process.</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>

<p>If you have any questions regarding the above, please feel free to <a href="%scontact.php">contact us</a>.</p>

<p>Regards, %s Site Administrator</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>';
$string['debugemail'] = 'AVÍS: Aquest correu anava adreçat a %s <%s> però n\'heu rebut una còpia a causa de l\'opció de configuració "sendallemailto".';
$string['divertingemailto'] = 'Desvia el correu a %s';


// Expiry times
$string['noenddate'] = 'Sense data límit';
$string['day']       = 'dia';
$string['days']      = 'dies';
$string['weeks']     = 'setmanes';
$string['months']    = 'mesos';
$string['years']     = 'anys';
// Boolean site option

// Site content pages
$string['sitecontentnotfound'] = 'no s\'ha trobat el text %s ';

// Contact us form
$string['name']                     = 'Nom';
$string['email']                    = 'Adreça de correu';
$string['subject']                  = 'Tema';
$string['message']                  = 'Missatge';
$string['messagesent']              = 'S\'ha enviat el missatge';
$string['nosendernamefound']        = 'No s\'ha enviat el nom del remitent';
$string['emailnotsent']             = 'No s\'ha pogut enviat el missatge de contacte a causa d\'aquest error: "%s"';

// mahara.js
$string['namedfieldempty'] = 'El camp obligatori "%s" és buit';
$string['processing']     = 'Processant';
$string['requiredfieldempty'] = 'Hi ha un camp obligatori buit';
$string['unknownerror']       = 'S\'ha produït un error desconegut (0x20f91a0)';

// menu
$string['home']        = 'Inici';
$string['Content']     = 'Contingut';
$string['myportfolio'] = 'El meu Portafolis';
$string['settings']    = 'Configuració';
$string['myfriends']          = 'Els meus amics';
$string['findfriends']        = 'Cerca amics';
$string['groups']             = 'Grups';
$string['mygroups']           = 'Els meus grups';
$string['findgroups']         = 'Cerca grups';
$string['returntosite']       = 'Torna al lloc';
$string['siteadministration'] = 'Administració del lloc';
$string['institutionadministration'] = 'Administració de la institució';

$string['unreadmessages'] = 'missatges no llegits';
$string['unreadmessage'] = 'missatge no llegit';

$string['siteclosed'] = 'Aquest lloc resta tancat temporalment mentre dura l\'actualització de la base de dades. Només hi poden entrar els administradors.';
$string['siteclosedlogindisabled'] = 'Aquest lloc resta tancat temporalment mentre dura l\'actualització de la base de dades.  <a href="%s">Executa ara l\'actualització.</a>';

// footer
$string['termsandconditions'] = 'Termes i condicions';
$string['privacystatement']   = 'Política de privacitat';
$string['about']              = 'Sobre';
$string['contactus']          = 'Contacte';

// my account
$string['account'] =  'El meu compte';
$string['accountprefs'] = 'Preferències';
$string['preferences'] = 'Preferències';
$string['activityprefs'] = 'Preferències d\'activitat';
$string['changepassword'] = 'Canvi de contrasenya';
$string['notifications'] = 'Notificacions';
$string['inbox'] = 'Safata d\'entrada';
$string['gotoinbox'] = 'Vés a la safata d\'entrada';
$string['institutionmembership'] = 'Pertanyença a Institucions';
$string['institutionmembershipdescription'] = 'Si pertanyeu a alguna institució, el nom apareixerà en la següent llista.';
$string['youareamemberof'] = 'Sou membre de %s';
$string['leaveinstitution'] = 'Abandonar la institució';
$string['reallyleaveinstitution'] = 'Esteu segur que voleu abandonar aquesta institució?';
$string['youhaverequestedmembershipof'] = 'Heu sol·licitat ser membre de  %s';
$string['cancelrequest'] = 'Cancel·la la sol·licitud';
$string['youhavebeeninvitedtojoin'] = 'Esteu convidat a unir-vos a %s';
$string['confirminvitation'] = 'Confirmeu la invitació';
$string['joininstitution'] = 'Uniu-vos a la institució';
$string['decline'] = 'Declina';
$string['requestmembershipofaninstitution'] = 'Demaneu ser membre d\'una institució';
$string['optionalinstitutionid'] = 'Institució ID (opcional)';
$string['institutionmemberconfirmsubject'] = 'Confirmació de la inscripció a la institució';
$string['institutionmemberconfirmmessage'] = 'Ara ja sou membre de %s.';
$string['institutionmemberrejectsubject'] = 'S\'ha declinat la sol·licitud d\'inscripció a la institució';
$string['institutionmemberrejectmessage'] = 'S\'ha declinat la vostra sol·licitud d\'inscripció a la institució %s.';
$string['Memberships'] = 'Membre de';
$string['Requests'] = 'Sol·licituds';
$string['Invitations'] = 'Invitacions';

$string['config'] = 'Configuració';

$string['sendmessage'] = 'Envia missatge';
$string['spamtrap'] = 'Trampa d\'spam';
$string['formerror'] = 'S\'ha produït un error en processar la vostra sol·licitud. Torneu-ho a provar.';
$string['formerroremail'] = 'Contacteu amb nosaltres a %s si continueu tenint problemes.';
$string['blacklisteddomaininurl'] = 'Una url en aquest camp conté el domini bloquejat  %s.';

$string['notinstallable'] = 'No es pot instal·lar';
$string['installedplugins'] = 'Connectors Instal·lats';
$string['notinstalledplugins'] = 'Connectors no instal·lats';
$string['plugintype'] = 'Tipus de connector';

$string['settingssaved'] = 'Configuració desada';
$string['settingssavefailed'] = 'S\'ha produït un error en desar la configuració';

$string['width'] = 'Amplada';
$string['height'] = 'Alçada';
$string['widthshort'] = 'a';
$string['heightshort'] = 'alç';
$string['filter'] = 'Filtre';
$string['expand'] = 'Expandeix';
$string['collapse'] = 'Redueix';
$string['more...'] = 'Més ...';
$string['nohelpfound'] = 'No s\'ha trobat ajuda per aquest ítem';
$string['nohelpfoundpage'] = 'No s\'ha trobat ajuda per aquesta pàgina';
$string['couldnotgethelp'] = 'S\'ha produït un error en accedir a la pàgina d\'ajuda';
$string['profileimage'] = 'Imatge del perfil';
$string['primaryemailinvalid'] = 'L\'adreça de correu principal no és vàlida';
$string['addemail'] = 'Afegiu l\'adreça de correu';

// Search
$string['search'] = 'Cerca';
$string['searchusers'] = 'Cerca usuaris';
$string['Query'] = 'Consulta';
$string['query'] = 'Consulta';
$string['querydescription'] = 'Paraules que s\'han de cercar';
$string['result'] = 'resultat';
$string['results'] = 'resultats';
$string['Results'] = 'Resultats';
$string['noresultsfound'] = 'No s\'ha trobat cap resultat';
$string['users'] = 'Usuaris';

// artefact
$string['artefact'] = 'artefacte';
$string['Artefact'] = 'Artefacte';
$string['Artefacts'] = 'Artefactes';
$string['artefactnotfound'] = 'No s\'ha trobat cap artefacte que contingui %s';
$string['artefactnotrendered'] = 'Artefacte no processat';
$string['nodeletepermission'] = 'No teniu permís per esborrar aquest artefacte';
$string['noeditpermission'] = 'No teniu permís per editar aquest artefacte';
$string['Permissions'] = 'Permisos';
$string['republish'] = 'Publica';
$string['view'] = 'Pàgina';
$string['artefactnotpublishable'] = 'L\'artefact %s no es pot publicar a la pàgina %s';

$string['belongingto'] = 'Que pertanyi a';
$string['allusers'] = 'Tots els usuaris';
$string['attachment'] = 'Adjunt';

// Upload manager
$string['quarantinedirname'] = 'quarantena';
$string['clammovedfile'] = 'S\'ha mogut el fitxer al directori de quarantena.';
$string['clamdeletedfile'] = 'S\'ha esborrat el fitxer';
$string['clamdeletedfilefailed'] = 'No es pot esborrar el fitxer';
$string['clambroken'] = 'L\'administrador del sistema ha activat l\'antivirus per a la càrrega de fitxers i  no s\'ha pogut completar la càrrega del vostre fitxer.  S\'ha enviat un avís a l\'administrador.  Proveu a carregar-lo més endavant.';
$string['clamemailsubject'] = '%s :: notificació de Clam AV';
$string['clamlost'] = 'Clam AV is configured to run on file upload, but the path supplied to Clam AV, %s, is invalid.';
$string['clamfailed'] = 'Clam AV has failed to run.  The return error message was %s. Here is the output from Clam:';
$string['clamunknownerror'] = 'S\'ha produït un error desconegut a Clam.';
$string['image'] = 'Imatge';
$string['filenotimage'] = 'El fitxer que heu pujat no és una imatge vàlida. Ha de tenir l\'extensió PNG, JPEG o GIF.';
$string['uploadedfiletoobig'] = 'Fitxer massa gran. Demaneu més informació a l\'administrador.';
$string['notphpuploadedfile'] = 'El fitxer s\'ha perdut durant el procés de càrrega. Demaneu més informació a l\'administrador.';
$string['virusfounduser'] = 'El fitxer que heu provat de carregar, %s, s\'ha escanejat i conté un virus, per això no s\'ha desat. Poseu-vos en contacte amb l\'administrador';
$string['fileunknowntype'] = 'No s\'ha pogut determinar el tipus de fitxer que heu carregat. Pot ser que estigui corrupte o bé que hi hagi un problema de configuració. Poseu-vos en contacte amb l\'administrador.';
$string['virusrepeatsubject'] = 'Avís: %s és un carregador múltiple de virus.Poseu-vos en contacte amb l\'administrador.';
$string['virusrepeatmessage'] = 'L\'usuari %s ha carregat diferents  fitxers infectats per virus.';

$string['phpuploaderror'] = 'S\'ha produït un error mentre es carregava el fitxer %s (Codi d\'error %s)';
$string['phpuploaderror_1'] = 'El fitxer carregat excedeix la directiva upload_max_filesize de php.ini.';
$string['phpuploaderror_2'] = 'El fitxer carregat excedeix la directiva MAX_FILE_SIZE especificacda al formulari html.';
$string['phpuploaderror_3'] = 'El fitxer només s\'ha pogut carregar parcialment.';
$string['phpuploaderror_4'] = 'No s\'ha carregat cap fitxer.';
$string['phpuploaderror_6'] = 'S\'ha perdut un directori temporal.';
$string['phpuploaderror_7'] = 'No s\'ha pogut escriure el fitxer al disc.';
$string['phpuploaderror_8'] = 'S\'ha aturat la càrrega a causa de l\'extensió del fitxer.';
$string['adminphpuploaderror'] = 'L\'error de càrrega del fitxer probablement és degut a la configuració del servidor.';

$string['youraccounthasbeensuspendedtext2'] = '%s ha suspès el vostre compte a %s.'; // @todo: more info?
$string['youraccounthasbeensuspendedreasontext'] = "%s ha suspès el vostre compte a %s. La raó:\n\n%s";
$string['youraccounthasbeenunsuspendedtext2'] = 'S\'ha aixecat la suspensió del vostre compte a %s. Ara podeu tornar a entrar al lloc i utilitzar-lo.'; // can't provide a login link because we don't know how they log in - it might be by xmlrpc

// size of stuff
$string['sizemb'] = 'MB';
$string['sizekb'] = 'KB';
$string['sizegb'] = 'GB';
$string['sizeb'] = 'b';
$string['bytes'] = 'bytes';

// countries

$string['country.af'] = 'Afghanistan';
$string['country.ax'] = 'Åland Islands';
$string['country.al'] = 'Albania';
$string['country.dz'] = 'Algeria';
$string['country.as'] = 'American Samoa';
$string['country.ad'] = 'Andorra';
$string['country.ao'] = 'Angola';
$string['country.ai'] = 'Anguilla';
$string['country.aq'] = 'Antarctica';
$string['country.ag'] = 'Antigua and Barbuda';
$string['country.ar'] = 'Argentina';
$string['country.am'] = 'Armenia';
$string['country.aw'] = 'Aruba';
$string['country.au'] = 'Australia';
$string['country.at'] = 'Austria';
$string['country.az'] = 'Azerbaijan';
$string['country.bs'] = 'Bahamas';
$string['country.bh'] = 'Bahrain';
$string['country.bd'] = 'Bangladesh';
$string['country.bb'] = 'Barbados';
$string['country.by'] = 'Belarus';
$string['country.be'] = 'Belgium';
$string['country.bz'] = 'Belize';
$string['country.bj'] = 'Benin';
$string['country.bm'] = 'Bermuda';
$string['country.bt'] = 'Bhutan';
$string['country.bo'] = 'Bolivia';
$string['country.ba'] = 'Bosnia and Herzegovina';
$string['country.bw'] = 'Botswana';
$string['country.bv'] = 'Bouvet Island';
$string['country.br'] = 'Brazil';
$string['country.io'] = 'British Indian Ocean Territory';
$string['country.bn'] = 'Brunei Darussalam';
$string['country.bg'] = 'Bulgaria';
$string['country.bf'] = 'Burkina Faso';
$string['country.bi'] = 'Burundi';
$string['country.kh'] = 'Cambodia';
$string['country.cm'] = 'Cameroon';
$string['country.ca'] = 'Canada';
$string['country.cv'] = 'Cape Verde';
$string['country.ct'] = 'Catalunya';
$string['country.ky'] = 'Cayman Islands';
$string['country.cf'] = 'Central African Republic';
$string['country.td'] = 'Chad';
$string['country.cl'] = 'Chile';
$string['country.cn'] = 'China';
$string['country.cx'] = 'Christmas Island';
$string['country.cc'] = 'Cocos (Keeling) Islands';
$string['country.co'] = 'Colombia';
$string['country.km'] = 'Comoros';
$string['country.cg'] = 'Congo';
$string['country.cd'] = 'Congo, The Democratic Republic of The';
$string['country.ck'] = 'Cook Islands';
$string['country.cr'] = 'Costa Rica';
$string['country.ci'] = 'Cote D\'ivoire';
$string['country.hr'] = 'Croatia';
$string['country.cu'] = 'Cuba';
$string['country.cy'] = 'Cyprus';
$string['country.cz'] = 'Czech Republic';
$string['country.dk'] = 'Denmark';
$string['country.dj'] = 'Djibouti';
$string['country.dm'] = 'Dominica';
$string['country.do'] = 'Dominican Republic';
$string['country.ec'] = 'Ecuador';
$string['country.eg'] = 'Egypt';
$string['country.sv'] = 'El Salvador';
$string['country.gq'] = 'Equatorial Guinea';
$string['country.er'] = 'Eritrea';
$string['country.ee'] = 'Estonia';
$string['country.et'] = 'Ethiopia';
$string['country.fk'] = 'Falkland Islands (Malvinas)';
$string['country.fo'] = 'Faroe Islands';
$string['country.fj'] = 'Fiji';
$string['country.fi'] = 'Finland';
$string['country.fr'] = 'France';
$string['country.gf'] = 'French Guiana';
$string['country.pf'] = 'French Polynesia';
$string['country.tf'] = 'French Southern Territories';
$string['country.ga'] = 'Gabon';
$string['country.gm'] = 'Gambia';
$string['country.ge'] = 'Georgia';
$string['country.de'] = 'Germany';
$string['country.gh'] = 'Ghana';
$string['country.gi'] = 'Gibraltar';
$string['country.gr'] = 'Greece';
$string['country.gl'] = 'Greenland';
$string['country.gd'] = 'Grenada';
$string['country.gp'] = 'Guadeloupe';
$string['country.gu'] = 'Guam';
$string['country.gt'] = 'Guatemala';
$string['country.gg'] = 'Guernsey';
$string['country.gn'] = 'Guinea';
$string['country.gw'] = 'Guinea-bissau';
$string['country.gy'] = 'Guyana';
$string['country.ht'] = 'Haiti';
$string['country.hm'] = 'Heard Island and Mcdonald Islands';
$string['country.va'] = 'Holy See (Vatican City State)';
$string['country.hn'] = 'Honduras';
$string['country.hk'] = 'Hong Kong';
$string['country.hu'] = 'Hungary';
$string['country.is'] = 'Iceland';
$string['country.in'] = 'India';
$string['country.id'] = 'Indonesia';
$string['country.ir'] = 'Iran, Islamic Republic of';
$string['country.iq'] = 'Iraq';
$string['country.ie'] = 'Ireland';
$string['country.im'] = 'Isle of Man';
$string['country.il'] = 'Israel';
$string['country.it'] = 'Italy';
$string['country.jm'] = 'Jamaica';
$string['country.jp'] = 'Japan';
$string['country.je'] = 'Jersey';
$string['country.jo'] = 'Jordan';
$string['country.kz'] = 'Kazakhstan';
$string['country.ke'] = 'Kenya';
$string['country.ki'] = 'Kiribati';
$string['country.kp'] = 'Korea, Democratic People\'s Republic of';
$string['country.kr'] = 'Korea, Republic of';
$string['country.kw'] = 'Kuwait';
$string['country.kg'] = 'Kyrgyzstan';
$string['country.la'] = 'Lao People\'s Democratic Republic';
$string['country.lv'] = 'Latvia';
$string['country.lb'] = 'Lebanon';
$string['country.ls'] = 'Lesotho';
$string['country.lr'] = 'Liberia';
$string['country.ly'] = 'Libyan Arab Jamahiriya';
$string['country.li'] = 'Liechtenstein';
$string['country.lt'] = 'Lithuania';
$string['country.lu'] = 'Luxembourg';
$string['country.mo'] = 'Macao';
$string['country.mk'] = 'Macedonia, The Former Yugoslav Republic of';
$string['country.mg'] = 'Madagascar';
$string['country.mw'] = 'Malawi';
$string['country.my'] = 'Malaysia';
$string['country.mv'] = 'Maldives';
$string['country.ml'] = 'Mali';
$string['country.mt'] = 'Malta';
$string['country.mh'] = 'Marshall Islands';
$string['country.mq'] = 'Martinique';
$string['country.mr'] = 'Mauritania';
$string['country.mu'] = 'Mauritius';
$string['country.yt'] = 'Mayotte';
$string['country.mx'] = 'Mexico';
$string['country.fm'] = 'Micronesia, Federated States of';
$string['country.md'] = 'Moldova, Republic of';
$string['country.mc'] = 'Monaco';
$string['country.mn'] = 'Mongolia';
$string['country.ms'] = 'Montserrat';
$string['country.ma'] = 'Morocco';
$string['country.mz'] = 'Mozambique';
$string['country.mm'] = 'Myanmar';
$string['country.na'] = 'Namibia';
$string['country.nr'] = 'Nauru';
$string['country.np'] = 'Nepal';
$string['country.nl'] = 'Netherlands';
$string['country.an'] = 'Netherlands Antilles';
$string['country.nc'] = 'New Caledonia';
$string['country.nz'] = 'New Zealand';
$string['country.ni'] = 'Nicaragua';
$string['country.ne'] = 'Niger';
$string['country.ng'] = 'Nigeria';
$string['country.nu'] = 'Niue';
$string['country.nf'] = 'Norfolk Island';
$string['country.mp'] = 'Northern Mariana Islands';
$string['country.no'] = 'Norway';
$string['country.om'] = 'Oman';
$string['country.pk'] = 'Pakistan';
$string['country.pw'] = 'Palau';
$string['country.ps'] = 'Palestinian Territory, Occupied';
$string['country.pa'] = 'Panama';
$string['country.pg'] = 'Papua New Guinea';
$string['country.py'] = 'Paraguay';
$string['country.pe'] = 'Peru';
$string['country.ph'] = 'Philippines';
$string['country.pn'] = 'Pitcairn';
$string['country.pl'] = 'Poland';
$string['country.pt'] = 'Portugal';
$string['country.pr'] = 'Puerto Rico';
$string['country.qa'] = 'Qatar';
$string['country.re'] = 'Reunion';
$string['country.ro'] = 'Romania';
$string['country.ru'] = 'Russian Federation';
$string['country.rw'] = 'Rwanda';
$string['country.sh'] = 'Saint Helena';
$string['country.kn'] = 'Saint Kitts and Nevis';
$string['country.lc'] = 'Saint Lucia';
$string['country.pm'] = 'Saint Pierre and Miquelon';
$string['country.vc'] = 'Saint Vincent and The Grenadines';
$string['country.ws'] = 'Samoa';
$string['country.sm'] = 'San Marino';
$string['country.st'] = 'Sao Tome and Principe';
$string['country.sa'] = 'Saudi Arabia';
$string['country.sn'] = 'Senegal';
$string['country.cs'] = 'Serbia and Montenegro';
$string['country.sc'] = 'Seychelles';
$string['country.sl'] = 'Sierra Leone';
$string['country.sg'] = 'Singapore';
$string['country.sk'] = 'Slovakia';
$string['country.si'] = 'Slovenia';
$string['country.sb'] = 'Solomon Islands';
$string['country.so'] = 'Somalia';
$string['country.za'] = 'South Africa';
$string['country.gs'] = 'South Georgia and The South Sandwich Islands';
$string['country.es'] = 'Spain';
$string['country.lk'] = 'Sri Lanka';
$string['country.sd'] = 'Sudan';
$string['country.sr'] = 'Suriname';
$string['country.sj'] = 'Svalbard and Jan Mayen';
$string['country.sz'] = 'Swaziland';
$string['country.se'] = 'Sweden';
$string['country.ch'] = 'Switzerland';
$string['country.sy'] = 'Syrian Arab Republic';
$string['country.tw'] = 'Taiwan, Province of China';
$string['country.tj'] = 'Tajikistan';
$string['country.tz'] = 'Tanzania, United Republic of';
$string['country.th'] = 'Thailand';
$string['country.tl'] = 'Timor-leste';
$string['country.tg'] = 'Togo';
$string['country.tk'] = 'Tokelau';
$string['country.to'] = 'Tonga';
$string['country.tt'] = 'Trinidad and Tobago';
$string['country.tn'] = 'Tunisia';
$string['country.tr'] = 'Turkey';
$string['country.tm'] = 'Turkmenistan';
$string['country.tc'] = 'Turks and Caicos Islands';
$string['country.tv'] = 'Tuvalu';
$string['country.ug'] = 'Uganda';
$string['country.ua'] = 'Ukraine';
$string['country.ae'] = 'United Arab Emirates';
$string['country.gb'] = 'United Kingdom';
$string['country.us'] = 'United States';
$string['country.um'] = 'United States Minor Outlying Islands';
$string['country.uy'] = 'Uruguay';
$string['country.uz'] = 'Uzbekistan';
$string['country.vu'] = 'Vanuatu';
$string['country.ve'] = 'Venezuela';
$string['country.vn'] = 'Viet Nam';
$string['country.vg'] = 'Virgin Islands, British';
$string['country.vi'] = 'Virgin Islands, U.S.';
$string['country.wf'] = 'Wallis and Futuna';
$string['country.eh'] = 'Western Sahara';
$string['country.ye'] = 'Yemen';
$string['country.zm'] = 'Zambia';
$string['country.zw'] = 'Zimbabwe';
$string['nocountryselected'] = 'No hi ha cap país seleccionat';

// general stuff that doesn't really fit anywhere else
$string['system'] = 'Sistema';
$string['done'] = 'Fet';
$string['back'] = 'Enrere';
$string['backto'] = 'Enrere a %s';
$string['alphabet'] = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z';
$string['formatpostbbcode'] = 'Podeu formatar la vostra entrada amb el codi BBCode. %sSaber-ne més%s';
$string['Created'] = 'Creat';
$string['Updated'] = 'Actualitzat';
$string['Total'] = 'Total';
$string['Visits'] = 'Visites';
$string['after'] = 'després de';
$string['before'] = 'abans de';

// import related strings (maybe separated later)
$string['importedfrom'] = 'Importat de %s';
$string['incomingfolderdesc'] = 'Fitxers importats des d\'altres servidors de la xarxa';
$string['remotehost'] = 'Servidor remot %s';

$string['Copyof'] = 'Còpia de %s';

// Profile views
$string['loggedinusersonly'] = 'Només usuaris registrats';
$string['allowpublicaccess'] = 'Permeteu l\'accés públic a tots els usuaris no registrats';
$string['thisistheprofilepagefor'] = 'Aquesta és la pàgina del perfil de %s';
$string['viewmyprofilepage']  = 'Mostra la pàgina del perfil';
$string['editmyprofilepage']  = 'Edita la pàgina del perfil';
$string['usersprofile'] = "Perfil de %s";
$string['profiledescription'] = 'La pàgina del vostre perfil és el que veuran els altres quan cliquin sobre el vostre nom o icona del perfil.';

// Dashboard views
$string['mydashboard'] = 'La meva Consola';
$string['editdashboard'] = 'Edita';
$string['usersdashboard'] = "Consola de %s";
$string['dashboarddescription'] = 'La pàgina <strong>Consola</strong> és el primer que veieu quan entreu a l\'eportafolis. Només vós hi teniu accés.';
$string['topicsimfollowing'] = "Temes que segueixo";
$string['recentactivity'] = 'Activitat recent ';
$string['mymessages'] = 'Els meus Missatges';

$string['pleasedonotreplytothismessage'] = "No contesteu a aquest missatge.";
$string['deleteduser'] = 'Usuari esborrat';

$string['theme'] = 'Tema';
$string['choosetheme'] = 'Escolliu tema...';

// Home page info block
$string['Hide'] = 'Amaga';
$string['createcollect'] = 'Crea i recull';
$string['createcollectsubtitle'] = 'Desenvolupeu el vostre eportafolis';
$string['updateyourprofile'] = 'Actualitzeu el <a href="%s">Perfil</a>';
$string['uploadyourfiles'] = 'Carregueu <a href="%s">Fitxers</a>';
$string['createyourresume'] = 'Creeu el vostre <a href="%s">C.V.</a>';
$string['publishablog'] = 'Publiqueu un <a href="%s">Diari</a>';
$string['Organise'] = 'Organitza';
$string['organisesubtitle'] = 'Mostreu el vostre eportafolis amb les <em>Pàgines</em>';
$string['organisedescription'] = 'Organitzeu el vostre eportafolis amb les <a href="%s">Pàgines.</a>  Creeu diferents pàgines per a diferents audiències -  escolliu quins elements hi poseu.';
$string['sharenetwork'] = 'Comparteix i crea Xarxa';
$string['sharenetworksubtitle'] = 'Feu amics i uniu-vos a grups';
$string['findfriendslinked'] = 'Trobeu <a href="%s">Amics</a>';
$string['joingroups'] = 'Uniu-vos a <a href="%s">Grups</a>';
$string['sharenetworkdescription'] = 'Podeu afinar molt sobre qui tindrà accés a cada Pàgina i per quant de temps.';
$string['howtodisable'] = 'Heu amagat la caixa informativa. Controleu la seva visibilitat des de <a href="%s">Configuració</a>.';

// Blocktype
$string['setblocktitle'] = 'Establiu un bloc de títol';


?>
