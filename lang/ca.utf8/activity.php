<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage notification-internal
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */


defined('INTERNAL') || die();

$string['typemaharamessage'] = 'Missatge del sistema';
$string['typeusermessage'] = 'Missatge d\'altres usuaris';
$string['typewatchlist'] = 'Llista de seguiment';
$string['typeviewaccess'] = 'Accés a Pàgina nova';
$string['typecontactus'] = 'Contacta\'ns';
$string['typeobjectionable'] = 'Objecte amb objeccions';
$string['typevirusrepeat'] = 'Càrrega repetida per virus';
$string['typevirusrelease'] = 'Marcat amb senyal de Virus';
$string['typeadminmessages'] = 'Missatge de l\'Administració';
$string['typeinstitutionmessage'] = 'Missatge de la Institució';
$string['typegroupmessage'] = 'Missatge del grup';

$string['type'] = 'Tipus d\'activitat';
$string['attime'] = 'a';
$string['prefsdescr'] = 'Encara que seleccioneu alguna de les opcions del correu, les notificacions apareixeran a l\'informe de notificacions tot i que, automàticament, quedaran marcades com a llegides.';

$string['subject'] = 'Tema';
$string['date'] = 'Data';
$string['read'] = 'Llegit';
$string['unread'] = 'No llegit';

$string['markasread'] = 'Marca\'l com a llegit';
$string['selectall'] = 'Selecciona-ho tot';
$string['recurseall'] = 'Torna-hi'; //recurse all???
$string['alltypes'] = 'Tots els tipus';

$string['markedasread'] = 'Totes les notificacions s\'han marcat com a llegides';
$string['failedtomarkasread'] = 'No s\'han pogut marcar com a llegides totes les notificacions';

$string['deletednotifications'] = 'S\'han esborrat %s notificacions';
$string['failedtodeletenotifications'] = 'No s\'han pogut esborrar les notificacions';

$string['stopmonitoring'] = 'Deixa de seguir';
$string['artefacts'] = 'Artefactes';
$string['groups'] = 'Grups';
$string['monitored'] = 'En seguiment';

$string['stopmonitoringsuccess'] = 'S\'ha deixat de fer el seguiment';
$string['stopmonitoringfailed'] = 'No s\'ha pogut deixar de fer el seguiment';

$string['newwatchlistmessage'] = 'Nova activitat a la llista de seguiment';
$string['newwatchlistmessageview'] = '%s ha canviat la seva Pàgina "%s"';

$string['newviewsubject'] = 'S\'ha creat una pàgina nova';
$string['newviewmessage'] = '%s ha creat una pàgina nova "%s"';

$string['newcontactusfrom'] = 'Nou ens contacte nou des de';
$string['newcontactus'] = 'Nou ens contacta';

$string['newviewaccessmessage'] = 'Heu sigut afegit a la Pàgina anomenada "%s" per %s '; //contrib Juan Segarra Montesinos, UJI
$string['newviewaccessmessagenoowner'] = 'Heu sigut admès per accedir a la llista  "%s"';
$string['newviewaccesssubject'] = 'Accés a la nova Pàgina';

$string['viewmodified'] = 'ha canviat la seva pàgina';
$string['ongroup'] = 'al Grup';
$string['ownedby'] = 'que pertany a';

$string['objectionablecontentview'] = '%s anuncia un objecte amb objeccions a la pàgina "%s"';
$string['objectionablecontentviewartefact'] = 'Hi ha contingut amb objeccions a la Pàgina "%s" de "%s" avisat per %s';

$string['objectionablecontentviewhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Hi ha contingut amb objeccions a "%s" avisat per %s<strong></strong><br>%s</div>

<div style="margin: 1em 0;">%s</div>

<div style="font-size: smaller; border-top: 1px solid #999;">
<p>La queixa es refereix a: <a href="%s">%s</a></p>
<p>Avisat per: <a href="%s">%s</a></p>
</div>';
$string['objectionablecontentviewtext'] = 'Hi ha contingut amb objeccions a "%s" ravisat per %s
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------
Per veure la pàgina seguiu aquest enllaç:
%s
Per veure el perfil de la persona que ha donat l\'avís seguiu aquest enllaç:
%s';

$string['objectionablecontentviewartefacthtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Hi ha contingut amb objeccions a "%s" in "%s" avisat per %s<strong></strong><br>%s</div>

<div style="margin: 1em 0;">%s</div>

<div style="font-size: smaller; border-top: 1px solid #999;">
<p>La queixa es refereix a : <a href="%s">%s</a></p>
<p>Avisada per: <a href="%s">%s</a></p>
</div>';
$string['objectionablecontentviewartefacttext'] = 'Hi ha contingut amb objeccions a "%s" in "%s" avisat per %s
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------
Per veure la pàgina seguiu aquest enllaç:
%s
Per veure el perfil de la persona que ha donat l\'avís seguiu aquest enllaç:
%s';

$string['newgroupmembersubj'] = '%s ja és membre del grup';
$string['removedgroupmembersubj'] = '%s ha deixat de ser membre del grup';

$string['addtowatchlist'] = 'Afegeix a la llista de seguiment';
$string['removefromwatchlist'] = 'Esborra de la llista de seguiment';

$string['missingparam'] = 'Al tipus d\'activitat %s li manca el paràmetre obligatori %s';

$string['institutionrequestsubject'] = '%s ha sol·licitat ser membre de %s.';
$string['institutionrequestmessage'] = 'Podeu afegir usuaris a les institucions des de la pàgina Membres d\'institucions:';

$string['institutioninvitesubject'] = 'Esteu convidat a ser membre de la institució %s.';
$string['institutioninvitemessage'] = 'Podeu confirmar la inscripció a aquesta institució des de la vostra pàgina de configuració d\'institucions:';

$string['deleteallnotifications'] = 'Esborra totes les notificacions';
$string['reallydeleteallnotifications'] = 'Esteu segur que voleu esborrar totes les notificacions?';

$string['viewsubmittedsubject'] = 'Pàgina enviada a %s';
$string['viewsubmittedmessage'] = '%s ha enviat la seva Pàgina "%s" a %s';

$string['adminnotificationerror'] = 'L\'error de notificació probablement ha estat causat per la configuració del vostre servidor.';










?>
