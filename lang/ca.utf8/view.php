<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */


defined('INTERNAL') || die();

$string['createview']             = 'Crea pàgina';
$string['edittitle']              = 'Edita el títol';
$string['edittitleanddescription'] = 'Edita el títol i la descripció';
$string['editcontent']            = 'Edita el contingut';
$string['editcontentandlayout']   = 'Edita el contingut i la disposició';
$string['editaccess']             = 'Edita l\'accés';
$string['editlayout']             = 'Edita la disposició';
$string['next']                   = 'Següent';
$string['back']                   = 'Anterior';
$string['title']                  = 'Títol de la pàgina';
$string['description']            = 'Descripció de la pàgina';
$string['startdate']              = 'Inici de l\'accés Data/Hora';
$string['stopdate']               = 'Final de l\'accés Data/Hora';
$string['stopdatecannotbeinpast'] = 'La data final no pot estar en el passat';
$string['startdatemustbebeforestopdate'] = 'La data d\'inici ha de ser anterior a la de final';
$string['unrecogniseddateformat'] = 'No s\'ha pogut reconèixer el format de la data';
$string['allowcommentsonview']    = 'Si marqueu els usuaris estaran autoritzats a deixar-vos-hi comentaris.';
$string['ownerformat']            = 'Format del vostre nom';
$string['ownerformatdescription'] = 'Com voleu que vegin el vostre nom les persones que vegin aquesta pàgina?';
$string['profileviewtitle']       = 'pàgina del perfil';
$string['dashboardviewtitle']  = 'pàgina <strong>Consola</strong>';
$string['editprofileview']        = 'Edita la pàgina del perfil';
$string['grouphomepageviewtitle'] = 'pàgina <em>Pàgina d\'inici</em> del grup';

// my views
$string['artefacts'] = 'Artefactes';
$string['groupviews'] = 'Vistes del grup';
$string['institutionviews'] = 'Vistes de la institució';
$string['reallyaddaccesstoemptyview'] = 'La pàgina no conté cap artefacte. Esteu segur que voleu donar accés a la pàgina a aquests usuaris?';
$string['viewdeleted'] = 'S\'ha esborrat la pàgina';
$string['viewsubmitted'] = 'S\'ha enviat la pàgina';
$string['deletethisview'] = 'Esborra la pàgina';
$string['submitthisviewto'] = 'Envia aquesta pàgina a';
$string['forassessment'] = 'per avaluació';
$string['accessfromdate2'] = 'Ningú més no pot veure aquesta pàgina abans de %s';
$string['accessuntildate2'] = 'Ningú més no pot veure aquesta pàgina després de %s';
$string['accessbetweendates2'] = 'Ningú més no pot veure aquesta pàgina abans de %s o després de %s';
$string['artefactsinthisview'] = 'Artefactes d\'aquesta pàgina';
$string['whocanseethisview'] = 'Qui pot veure aquesta pàgina';
$string['view'] = 'pàgina';
$string['views'] = 'pàgines';
$string['View'] = 'Pàgina';
$string['Views'] = 'Pàgines';
$string['viewsubmittedtogroup'] = 'S\'ha enviat aquesta pàgina a <a href="' . get_config('wwwroot') . 'group/view.php?id=%s">%s</a>';
$string['viewsubmittedtogroupon'] = 'S\'ha tramès aquesta pàgina a <a href="%s">%s</a> el %s';
$string['nobodycanseethisview2'] = 'Aquesta pàgina només la podeu veure vós';
$string['noviews'] = 'No teniu cap pàgina.';
$string['youhavenoviews'] = 'No teniu cap pàgina.';
$string['youhaventcreatedanyviewsyet'] = "Encara no heu creat cap pàgina.";
$string['youhaveoneview'] = 'Teniu 1 pàgina.';
$string['youhaveviews']   = 'Teniu %s Vistes.';
$string['viewsownedbygroup'] = 'Vistes pertanyents a aquest grup';
$string['viewssharedtogroup'] = 'Vistes compartides per aquest grup';
$string['viewssharedtogroupbyothers'] = 'Vistes compartides amb aquest grup per altres';
$string['viewssubmittedtogroup'] = 'Vistes enviades a aquest grup';
$string['submitaviewtogroup'] = 'Envieu una pàgina al grup';
$string['youhavesubmitted'] = 'Heu enviat <a href="%s">%s</a> al grup';
$string['youhavesubmittedon'] = 'Heu enviat <a href="%s">%s</a> al grup el %s';

// access levels
$string['public'] = 'Públic';
$string['loggedin'] = 'Usuaris identificats';
$string['friends'] = 'Amics';
$string['groups'] = 'Grups';
$string['users'] = 'Usuaris';
$string['tutors'] = 'tutors';
$string['everyoneingroup'] = 'Tothom del grup';

// secret url
$string['token'] = 'URL secreta';
$string['editsecreturlaccess'] = 'Edita l\'accés a la  URL secreta';
$string['newsecreturl'] = 'URL secreta nova';
$string['reallydeletesecreturl'] = 'Esteu segur que voleu esborrar aquesta URL?';
$string['secreturldeleted'] = 'S\'ha eliminat laURL secreta';
$string['secreturlupdated'] = 'S\'ha actualitzat la URL secreta';
$string['generatesecreturl'] = 'Genera una nova URL secreta per %s';
$string['secreturls'] = 'URLs secretes';

// view user
$string['inviteusertojoingroup'] = 'Convida a aquest usuari a unir-se a un grup';
$string['addusertogroup'] = 'Afegeix aquest usuari a un grup';

// view view
$string['addedtowatchlist'] = 'S\'ha afegit aquesta pàgina a la vostra llista de seguiment';
$string['attachment'] = 'Adjunt';
$string['removedfromwatchlist'] = 'S\'ha tret aquesta pàgina de la vostra llista de seguiment';
$string['addtowatchlist'] = 'Afegeix pàgina a la llista de seguiment';
$string['removefromwatchlist'] = 'Esborra pàgina de la llista de seguiment';
$string['alreadyinwatchlist'] = 'Aquesta pàgina ja és a la vostra llista de seguiment';
$string['attachedfileaddedtofolder'] = "S\'ha afegit el fitxer adjunt %s a la carpeta '%s'.";
$string['complaint'] = 'Queixa';
$string['date'] = 'Data';
$string['notifysiteadministrator'] = 'Notifiqueu a l\'administrador del lloc';
$string['notifysiteadministratorconfirm'] = 'Esteu segur que voleu notificar que aquesta pàgina conté material amb objecccions?';
$string['print'] = 'Imprimeix';
$string['reportobjectionablematerial'] = 'Informeu de material amb objeccions';
$string['reportsent'] = 'S\'ha enviat el vostre informe';
$string['viewobjectionableunmark'] = 'S\'ha informat que aquesta pàgina, o alguna part d\'ella, mostra contingut amb objeccions. Si no fos el cas cliqueu el botó per esborrar aquest avís i notificar-ho als administradors del lloc.';
$string['notobjectionable'] = 'No té objeccions';
$string['viewunobjectionablesubject'] = 'La pàgina %s ja no té objeccions segons %s';
$string['viewunobjectionablebody'] = '%s ha revisat %s de %s i ha comprovat que ja no té materials amb objeccions.';
$string['updatewatchlistfailed'] = 'No s\'ha pogut actualitzar la llista de seguiment';
$string['watchlistupdated'] = 'S\'ha actualitzat la llista de seguiment';
$string['viewvisitcount'] = 'Aquesta pàgina ha rebut %d visita(es) des de %s fins a %s';

$string['friend'] = 'Amic';
$string['profileicon'] = 'Icona del perfil';

// general views stuff
$string['Added'] = 'Afegit';
$string['share'] = 'Comparteix';
$string['sharewith'] = 'Comparteix amb';
$string['accesslist'] = 'Llista d\'accés';
$string['sharewithmygroups'] = 'Comparteix amb els meus Grups';
$string['shareview'] = 'Comparteix la pàgina';
$string['otherusersandgroups'] = 'Comparteix amb altres usuaris i grups';
$string['moreoptions'] = 'Opcions avançades';
$string['allviews'] = 'Totes les pàgines';

$string['submitviewconfirm'] = 'Si envieu \'%s\' a \'%s\' per a ser avaluat, no podreu editar la pàgina fins que el vostre tutor hagi acabat de marcar la pàgina. Esteu segur que voleu ara enviar aquesta pàgina?';
$string['viewsubmitted'] = 'S\'ha enviat la pàgina';
$string['submitviewtogroup'] = 'Envia \'%s\' a \'%s\' per a ser avaluat';
$string['cantsubmitviewtogroup'] = 'No podeu enviar aquesta pàgina a aquest grup per a ser avaluada';

$string['cantdeleteview'] = 'No podeu esborrar aquesta pàgina';
$string['deletespecifiedview'] = 'Esborra la pàgina "%s"';
$string['deleteviewconfirm'] = 'Esteu segur que voleu esborrar aquesta pàgina? Aquesta acció no es podrà desfer.';
$string['deleteviewconfirmnote'] = '<p><strong>Nota:</strong> no s\'esborraran els blocs de contingut afegits a la pàgina. Tanmateix sí s\'esborrarà qualsevol retroacció que s\'hi hagi fet. Abans d\'esborrar-la val la pena que considereu el fet de fer una còpia de seguretat de la pàgina tot exportant-la .</p>';

$string['editaccessdescription'] = '<p>Per defecte només vós podeu veure %s. Però podeu compartir %s amb altres tot afegint normes d\'accés a la pàgina.</p>
<p>Un cop ho hagueu fet baixeu per la pàgina i per continuar cliqueu .</p>';

$string['overridingstartstopdate'] = 'Ignora les dades d\'Inici/Final';
$string['overridingstartstopdatesdescription'] = 'Si voleu podeu configurar una data d\'inici o final que pot ser ignorada. Les altres persones no podran veure la pàgina fins la data d\'inici o després del final malgrat l\'accés que heu garantit.';

$string['emptylabel'] = 'Feu clic aquí per escriure el text d\'aquesta etiqueta';
$string['empty_block'] = 'Seleccioneu un artefacte de l\'arbre de l\'esquerra per posar aquí.';

$string['viewinformationsaved'] = 'S\'ha desat correctament la informació de la pàgina.';

$string['canteditdontown'] = 'No podeu editar aquesta pàgina perquè no us pertany';
$string['canteditdontownfeedback'] = 'No podeu editar aquesta retroacció perquè no us pertany';
$string['canteditsubmitted'] = 'No podeu editar aquesta pàgina perquè s\'ha enviat per a ser avaluada al grup "%s". Haureu d\'esperar fins que la persona tutora n\'alliberi la pàgina.';
$string['feedbackchangedtoprivate'] = 'S\'ha canviat la retroacció a Privada.';

$string['addtutors'] = 'Afegeix tutors';
$string['viewcreatedsuccessfully'] = 'S\'ha creat correctament la pàgina';
$string['viewaccesseditedsuccessfully'] = 'S\'ha desat correctament l\'accés a la pàgina';
$string['viewsavedsuccessfully'] = 'S\'ha desat correctament la pàgina';
$string['updatedaccessfornumviews'] = 'S\'han actualitzat els permisos d\'accés de  %d pàgina(es)';

$string['invalidcolumn'] = 'Columna %s fora de rang';

$string['confirmcancelcreatingview'] = 'No s\'ha completat la pàgina. Esteu segurs que voleu cancel·lar?';

// view control stuff

$string['editblockspagedescription'] = '<p>Escolliu a les pestanyes següents quins blocs voleu mostrar a la pàgina. Podeu Arrossegar i deixar anar els blocs a la plantilla de Vistes de la part inferior. Cliqueu la icona ? per obtenir més informació.</p>';
$string['displaymyview'] = 'Mostra la meva pàgina';
$string['editthisview'] = 'Edita aquesta pàgina';

$string['success.addblocktype'] = 'S\'ha afegit correctament el bloc';
$string['err.addblocktype'] = 'No es pot afegir el bloc a la vostra pàgina';
$string['success.moveblockinstance'] = 'S\'ha mogut correctament el bloc';
$string['err.moveblockinstance'] = 'No s\'ha pogut moure el bloc a la posició demanada';
$string['success.removeblockinstance'] = 'S\'ha eliminat correctament el bloc';
$string['err.removeblockinstance'] = 'No s\'ha pogut eliminar el bloc';
$string['success.addcolumn'] = 'S\'ha afegit correctament la columna';
$string['err.addcolumn'] = 'No s\'ha pogut afegir la noca columna';
$string['success.removecolumn'] = 'S\'ha eliminat correctament la columna';
$string['err.removecolumn'] = 'No s\'ha pogut eliminar la columna';
$string['success.changetheme'] = 'S\'ha  actualitzat el tema correctament';
$string['err.changetheme'] = 'No s\'ha pogut actualitzar el tema';

$string['confirmdeleteblockinstance'] = 'Esteu segurs que voleu eliminar aquest bloc?';
$string['blockinstanceconfiguredsuccessfully'] = 'S\'ha configurat correctament el bloc';
$string['blockconfigurationrenderingerror'] = 'Ha fallat la configuració perquè no s\'ha pogut renderitzar el bloc.';

$string['blocksintructionnoajax'] = 'Seleccioneu un bloc i trieu en quin punt de la vostra pàgina el voleu afegir. Podeu moure un bloc amb l\'ajuda dels botons amb fletxes de la barra de títols.';
$string['blocksinstructionajax'] = 'Per defecte trobareu 3 columnes, però en podeu esborrar o afegir alguna a través dels botons. Arrossegueu els blocs en algun punt de les columnes de la pantalla i afegiu-lo, així, a la vostra pàgina.';

$string['addnewblockhere'] = 'Afegeix aquí un nou bloc';
$string['add'] = 'Afegeix';
$string['addcolumn'] = 'Afegeix columna';
$string['remove'] = 'Elimina';
$string['removecolumn'] = 'Elimina aquesta columna';
$string['moveblockleft'] = 'Mou el bloc %s a l\'esquerra';
$string['movethisblockleft'] = "Mou aquest bloc a l\'esquerra";
$string['moveblockdown'] = 'Mou avall el bloc %s';
$string['movethisblockdown'] = "'Mou avall aquest bloc";
$string['moveblockup'] = "Mou amunt el bloc %s";
$string['movethisblockup'] = "Mou amunt aquest bloc";
$string['moveblockup'] = 'Mou amunt aquest bloc';
$string['moveblockright'] = 'Mou el bloc %s a la dreta';
$string['Configure'] = 'Configura';
$string['configureblock'] = 'Configura el bloc %s';
$string['configurethisblock'] = 'Configura aquest bloc';
$string['removeblock'] = 'Elimina el bloc %s';
$string['removethisblock'] = 'Elimina aquest bloc';
$string['blocktitle'] = 'Títol del bloc';

$string['changemyviewlayout'] = 'Canvia la disposició de la pàgina';
$string['viewcolumnspagedescription'] = 'Primer, seleccioneu el número de columnes de la pàgina. En el pas següent podreu canviar l\'amplada de les columnes.';
$string['viewlayoutpagedescription'] = 'Seleccioneu com voleu disposar les columnes a la pàgina.';
$string['changeviewlayout'] = 'Canvia la disposició de la meva pàgina';
$string['backtoyourview'] = 'Torna a la meva pàgina';
$string['viewlayoutchanged'] = 'S\'ha canviat la disposició de la pàgina';
$string['numberofcolumns'] = 'Número de columnes';


$string['by'] = 'de';
$string['viewtitleby'] = '%s de <a href="%s">%s</a>';
$string['in'] = 'a';
$string['noblocks'] = 'No hi ha cap bloc en aquesta categoria :(';
$string['timeofsubmission'] = 'Moment de la tramesa';

$string['column'] = 'columna';
$string['columns'] = 'columnes';
$string['100'] = $string['50,50'] = $string['33,33,33'] = $string['25,25,25,25'] = $string['20,20,20,20,20'] = 'Mateixa amplada';
$string['67,33'] = 'Columna esquerra més ampla';
$string['33,67'] = 'Columna dreta més ampla';
$string['25,50,25'] = 'Columna central més ampla';
$string['15,70,15'] = 'Columna central molt més ampla';
$string['20,30,30,20'] = 'Columna central bastant més ampla';
$string['noviewlayouts'] = 'No hi ha plantilla de pàgina per a %s columnes. Reduïu-ne el nombre.';
$string['cantaddcolumn'] = 'No podeu afegir més columnes a aquesta pàgina';
$string['cantremovecolumn'] = 'No podeu eliminar la última columna d\'aquesta pàgina';

$string['blocktypecategory.feeds'] = 'Alimentació RSS externa';
$string['blocktypecategory.fileimagevideo'] = 'Fitxers, imatges i vídeo';
$string['blocktypecategory.general'] = 'General';

$string['notitle'] = 'Sense títol';
$string['clickformoreinformation'] = 'Feu clic per incloure més informació i espai per a la retroacció';

$string['Browse'] = 'Navega';
$string['Search'] = 'Cerca';
$string['noartefactstochoosefrom'] = 'No hi ha cap artefacte per escollir';

$string['access'] = 'Accés';
$string['noaccesstoview'] = 'No teniu permisos per accedir a aquesta pàgina';

$string['changeviewtheme'] = 'El tema que heu seleccionat ja no està disponible. Trieu-ne un altre.';

// Templates
$string['Template'] = 'Plantilla';
$string['allowcopying'] = 'Permet copiar-la';
$string['templatedescription'] = 'Marqueu aquesta opció si voleu que qui pot veure la pàgina pugui fer-se\'n una còpia.';
$string['templatedescriptionplural'] = 'Marqueu aquesta opció si voleu que qui pot veure les vostres Vistes pugui fer-se\'n còpies juntament amb els directoris i fitxers que hi hagueu inclòs.';
$string['choosetemplatepagedescription'] = '<p>Des d\'aquí podeu cercar entre les Vistes a les que teniu accés i copiar-ne alguna com a punt de partida per fer-ne una de nova. Podeu veure una previsualització de cada pàgina clicant sobre el nom. Un cop hagueu trobat al pàgina que voleu copiar, cliqueu sobre el botó "Copia la pàgina" per fer-ne una còpia i, en acabat, personalitzar-la.</p>';
$string['choosetemplategrouppagedescription'] = '<p>Des d\'aquí podeu cercar entre les Vistes a les que té accés aquest grup i copiar-ne alguna com a punt de partida per fer-ne una de nova. Podeu veure una previsualització de cada pàgina clicant sobre el nom. Un cop hagueu trobat al pàgina que voleu copiar, cliqueu sobre el botó "Copia la pàgina" per fer-ne una còpia i, en acabat, personalitzar-la.</p><p><strong>Nota:</strong> Ara per ara els Grups no poden fer còpies ni de Blogs ni d\'entrades de blogs.</p>';
$string['choosetemplateinstitutionpagedescription'] = '<p>Des d\'aquí podeu cercar entre les Vistes a les que té accés aquesta institució i copiar-ne alguna com a punt de partida per fer-ne una de nova. Podeu veure una previsualització de cada pàgina clicant sobre el nom. Un cop hagueu trobat al pàgina que voleu copiar, cliqueu sobre el botó "Copia la pàgina" per fer-ne una còpia i, en acabat, personalitzar-la.</p><p><strong>Nota:</strong> Ara per ara les Institucions no poden fer còpies ni de Blogs ni d\'entrades de blogs.</p>';
$string['copiedblocksandartefactsfromtemplate'] = 'S\'han copiat %d blocs i  %d artefactes de %s';
$string['filescopiedfromviewtemplate'] = 'Fitxers copiats de %s';
$string['viewfilesdirname'] = 'fitxersdevistes';
$string['viewfilesdirdesc'] = 'Fitxers precedents de les Vistes copiades';
$string['thisviewmaybecopied'] = 'La còpia està permesa';
$string['copythisview'] = 'Copia aquesta pàgina';
$string['copyview'] = 'Copia la pàgina';
$string['createemptyview'] = 'Crea una pàgina buida';
$string['copyaview'] = 'Copia una pàgina';
$string['Untitled'] = 'Sense títol';
$string['copyfornewusers'] = 'Copia pels usuaris nous';
$string['copyfornewusersdescription'] = 'Cada cop que es creï un usuari nou, fes automàticament una còpia d\'aquesta pàgina a l\'ePortafolis de l\'usuari.';
$string['copyfornewmembers'] = 'Copia pels nous membres de la institució';
$string['copyfornewmembersdescription'] = 'Fes automàticament una còpia d\'aquesta pàgina per a tots els membres de %s.';
$string['copyfornewgroups'] = 'Copia pels grups nous';
$string['copyfornewgroupsdescription'] = 'Fes una còpia d\'aquesta pàgina a tots els Grups nous amb aquests Tipus de grup:';
$string['searchviews'] = 'Cerca Vistes';
$string['searchowners'] = 'Cerca propietaris';
$string['owner'] = 'propietari';
$string['Owner'] = 'Propietari';
$string['owners'] = 'propietaris';
$string['show'] = 'Mostra';
$string['searchviewsbyowner'] = 'Cerca Vistes per propietari:';
$string['selectaviewtocopy'] = 'Seleccioneu la pàgina que voleu copiar:';
$string['listviews'] = 'Llista de Vistes';
$string['nocopyableviewsfound'] = 'No hi ha cap pàgina que pugueu copiar';
$string['noownersfound'] = 'No s\'han trobat propietaris';
$string['Preview'] = 'Previsualització';
$string['viewscopiedfornewusersmustbecopyable'] = 'Heu de permetre la còpia abans de deixar que una pàgina pugui ser copiada per nous usuaris.';
$string['viewscopiedfornewgroupsmustbecopyable'] = 'Heu de permetre la còpia abans de deixar que una pàgina pugui ser copiada per nous grups.';
$string['copynewusergroupneedsloggedinaccess'] = 'Les Vistes copiades per usuaris o grups nous han de permetre l\'accés dels usuaris registrats.';
$string['viewcopywouldexceedquota'] = 'Si copieu aquesta pàgina excedireu la vostra quota d\'espai de fitxers.';

$string['blockcopypermission'] = 'Permisos de còpia del bloc';
$string['blockcopypermissiondesc'] = 'Podeu escollir com es copiarà aquest bloc en cas que permeteu la còpia d\'aquesta pàgina a altres usuaris.';

// View types
$string['dashboard'] = 'Consola';
$string['profile'] = 'Perfil';
$string['portfolio'] = 'ePortafolis';
$string['grouphomepage'] = 'Pàgina d\'inici del grup';

$string['grouphomepagedescription'] = 'La pàgina <strong>Pàgina d\'inici del grup</strong> és el contingut que apareix a la pestanya <em>Sobre</em> d\'aquest grup.';

// Shared views
$string['sharedviews'] = 'Pàgines compartides';
$string['titleanddescription'] = 'Títol, descripció, etiquetes';
$string['tagsonly'] = 'Només etiquetes';
$string['sharedviewsdescription'] = 'Aquesta pàgina mostra les pàgines compartides amb vós modificades darrerament o amb comentaris recents. Poden haver estat compartides directament amb vós, amb els amics o amb algun dels grups on pertanyeu.';

?>
