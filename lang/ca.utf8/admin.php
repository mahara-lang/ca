<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */


defined('INTERNAL') || die();

$string['administration'] = 'Administració';

// Installer
$string['installation'] = 'Instal·lació';
$string['release'] = 'Versió %s (%s)';
$string['copyright'] = 'Copyright &copy; 2006 endavant, Catalyst IT Ltd';
$string['installmahara'] = 'Instal·la Mahara';
$string['component'] = 'Component o connector';
$string['continue'] = 'Continua';
$string['coredata'] = 'nucli de dades';
$string['coredatasuccess'] = 'S\'ha instal·lat correctament el nucli de dades';
$string['fromversion'] = 'De de la  versió';
$string['information'] = 'Informació';
$string['installsuccess'] = 'S\'ha instal·lat correctament la versió ';
$string['toversion'] =  'a la versió';
$string['localdatasuccess'] = 'S\'han instal·lat correctament les particularitats locals';
$string['notinstalled'] = 'No instal·lat';
$string['nothingtoupgrade'] = 'No hi ha res per actualitzar';
$string['performinginstallation'] = 'Instal·lant i actualitzant...';
$string['performingupgrades'] = 'Performing upgrades...';
$string['runupgrade'] = 'Actualitza';
$string['successfullyinstalled'] = 'S\'ha instal·lat correctament Mahara';
$string['thefollowingupgradesareready'] = 'Estan preparades les següents actualitzacions:';
$string['registerthismaharasite'] = 'Registreu aquest lloc Mahara';
$string['upgradeloading'] = 'Carregant...';
$string['upgrades'] = 'Actualitzacions';
$string['upgradesuccess'] = 'S\'ha actualitzat correctament';
$string['upgradesuccesstoversion'] = 'S\'ha actualitzat correctament a la versió ';
$string['upgradefailure'] = 'No s\'ha pogut actualitzar';
$string['noupgrades'] = 'No hi ha res per actualitzar. Esteu completament actualitzats';
$string['youcanupgrade'] = 'Podeu actualitzar Mahara de de %s (%s) a %s (%s)';
$string['Plugin'] = 'Connector';
$string['jsrequiredforupgrade'] = 'Heu d\'activar javascript per poder instal·lar o actualitzar.';
$string['dbnotutf8warning'] = 'No esteu usant una base de dades UTF-8. Mahara emmagatzema internament les dades com a UTF-8. Encara podeu provar a actualitzar però és molt recomanable que convertiu la vostra base de dades a UTF-8.';
$string['dbcollationmismatch'] = 'Una columna de la base de dades està utilitzant una compaginació que no és la mateixa que la que usa la base de dades per defecte. Assegureu-vos que totes les columnes usen la mateixa paginació que la base de dades.';

// Admin navigation menu
$string['adminhome']      = 'Inici d\'Administració';
$string['configsite']  = 'Configuració del lloc';
$string['configusers'] = 'Gestió d\'usuaris';
$string['configextensions']   = 'Administració d\'extensions';
$string['manageinstitutions'] = 'Gestió d\'Institucions';
$string['groups'] = 'Grups';
$string['managegroups'] = 'Gestió de Grups';
$string['Extensions']   = 'Extensions';

// Admin homepage strings
$string['siteoptions']    = 'Opcions del lloc';
$string['siteoptionsdescription'] = 'Configureu les opcions bàsiques del lloc, com nom, llenguatge i tema';
$string['editsitepages']     = 'Edició de les pàgines del lloc';
$string['editsitepagesdescription'] = 'Editeu el contingut de diferents pàgines del lloc';
$string['linksandresourcesmenu'] = 'Menú Enllaços i Recursos';
$string['linksandresourcesmenudescription'] = 'Gestioneu els enllaços i recursos del menú Enllaços i Recursos';
$string['sitefiles']          = 'Fitxers del lloc';
$string['sitefilesdescription'] = 'Carregueu i administreu els fitxers que es poden posar al menú Enllaços i recursos i a les Vistes del lloc';
$string['siteviews']          = 'Vistes del lloc';
$string['siteviewsdescription'] = 'Creeu i administreu Vistes i Plantilles de vista per tot el lloc';
$string['networking']          = 'Xarxa';
$string['networkingdescription'] = 'Configureu la xarxa de Mahara';

$string['staffusers'] = 'Usuaris de plantilla';
$string['staffusersdescription'] = 'Assigneu permisos als usuaris de plantilla';
$string['adminusers'] = 'Usuaris administradors';
$string['adminusersdescription'] = 'Assigneu drets d\'accés a l\'administrador del lloc';
$string['institution']   = 'Institució';
$string['institutions']   = 'Institucions';
$string['institutiondetails']   = 'Detalls de la institució';
$string['institutionauth']   = 'Autoritats de la institució';
$string['institutionsdescription'] = 'Instal·leu i gestioneu institucions instal·lades';
$string['adminnotifications'] = 'Notificacions d\'administració';
$string['adminnotificationsdescription'] = 'Vista prèvia de les notificacions que reben els administradors';
$string['uploadcsv'] = 'Afegeix usuaris CSV';
$string['uploadcsvdescription'] = 'Carrega un fitxer CSV amb usuaris nous';
$string['usersearch'] = 'Cerca usuaris';
$string['usersearchdescription'] = 'Cerca entre tots els usuaris i hi realitza tasques administratives';
$string['usersearchinstructions'] = 'Podeu cercar usuaris clicant a les inicials del nom o del cognom o bé escrivint el nom, el cognom o l\'adreça de correu al formulari de cerca.';

$string['administergroups'] = 'Administreu Grups';
$string['administergroupsdescription'] = 'Nomeneu els administradors del grup i els grups elimineu grups';
$string['groupcategoriesdescription'] = 'Afegiu i editeu categories de grups';

$string['institutionmembersdescription'] = 'Associeu usuaris amb institucions';
$string['institutionstaffdescription'] = 'Assigneu permisos als usuaris de plantilla';
$string['institutionadminsdescription'] = 'Assigneu drets d\'accés a l\'administrador de la institució';
$string['institutionviews']          = 'Vistes de la institució';
$string['institutionviewsdescription'] = 'Creeu i administreu les Vistes i Plantilles de vista d\'una institució';
$string['institutionfiles']          = 'Fitxers de la institució';
$string['institutionfilesdescription'] = 'Carregueu i gestioneu fitxers per a usar-los a les Vistes de la institució';

$string['pluginadmin'] = 'Connector d\'Administració';
$string['pluginadmindescription'] = 'Instal·la i configura connectors';
$string['missingplugin'] = 'No s\'ha pogut trobar el connector instal·lat  (%s) ';
$string['installedpluginsmissing'] = 'aquests connectors estan instal·lats però ja no es poden trobar:';
$string['ensurepluginsexist'] = 'Assegureu-vos que tots els connectors instal·lats són accesibles a %s i llegibles pel servidor.';

$string['htmlfilters'] = 'Filtres HTML';
$string['htmlfiltersdescription'] = 'Activeu nous filtres per netejar l\'HTML';
$string['newfiltersdescription'] = 'Si descarregueu un nou joc de filtres HTML els podeu instal·lar si els descomprimiu al directori %s i cliqueu el botó';
$string['filtersinstalled'] = 'S\'han instal·lat els filtres.';
$string['nofiltersinstalled'] = 'No hi ha cap filtre html instal·lat.';

// sanity check warnings
$string['warnings'] = 'Avís';

// Group management
$string['groupcategories'] = 'Categories de grups';
$string['allowgroupcategories'] = 'Permet categories de grups';
$string['enablegroupcategories'] = 'Habilita categories de grups';
$string['addcategories'] = 'Afegeix categories';
$string['allowgroupcategoriesdescription'] = 'Si està marcat els administradors podran crear categories per a que els usuaris puguin categoritzar el seus grups.';
$string['groupoptionsset'] = 'S\'han actualitzat les opcions dels grups.';
$string['groupcategorydeleted'] = 'S\'ha esborrat la categoria';
$string['confirmdeletecategory'] = 'Esteu segur que voleu esborrar aquesta categoria?';
$string['groupcategoriespagedescription'] = 'Les categories de la llista següent es poden assignar a grups durant la creació de grups i utilitzar-les per filtrar grups quan es fa una cerca.';
$string['groupadminsforgroup'] = "Administradors de grups de '%s'";
$string['potentialadmins'] = 'Administradors potencials';
$string['currentadmins'] = 'Administradors actuals';
$string['groupadminsupdated'] = 'S\'ha actualitzat el grup d\'administradors';

// Register your Mahara
$string['Field'] = 'Camp';
$string['Value'] = 'Valor';
$string['datathatwillbesent'] = 'Dades que s\'enviaran';
$string['sendweeklyupdates'] = 'Voleu enviar actualitzacions setmanals?';
$string['sendweeklyupdatesdescription'] = 'Si marqueu l\'opció el vostre lloc enviarà actualitzacions setmanals a mahara.org amb algunes estadístiques d\'aquest lloc';
$string['Register'] = 'Registre';
$string['registrationfailedtrylater'] = 'El registre ha fallat a causa de l\'error %s. Si us plau, torneu-ho a provar més tard.';
$string['registrationsuccessfulthanksforregistering'] = 'S\'ha registrat el lloc sense problemes. Gràcies!';
$string['registeryourmaharasite'] = 'Registreu el vostre lloc Mahara';
$string['registeryourmaharasitesummary'] = '
<p>Si decidiu registrar el vostre lloc Mahara a <a href="http://mahara.org/">mahara.org</a> ens ajudareu a dibuixar una mapa de les instal·lacions de Mahara arreu del món.  Si us registreu desapareixerà aquest avís.</p>
<p>Podeu registrar el vostre lloc i veure quin tipus d\'informació s\'enviarà a la <strong><a href="%sadmin/registersite.php">pàgina de Registre del lloc.</a></strong></p>';
$string['registeryourmaharasitedetail'] = '
<p>Podeu registrar el vostre lloc Mahara a <a href="http://mahara.org/">mahara.org</a>. El registre és gratuït i ajuda a crear una imatge de les instal·lacions de Mahara per tot el món.</p>
<p>Podeu veure la informació que s\'enviarà a mahara.org, res que pugui identificar personalment a cap dels usuaris.</p>
<p>Si marqueu l\'opció &quot;envia actualitzacions setmanals&quot;, Mahara enviarà automàticament una actualització a mahara.org un cop a la setmana amb la vostra informació actualitzada.</p>
<p>Si us registreu aquest avís desapareixerà. Però sempre podreu canviar l\'opció d\'enviar actualitzacions setmanals des de la pàgina de les <a href="%sadmin/site/options.php">opcions del lloc</a>.</p>';
$string['siteregistered'] = 'S\'ha registrat el vostre lloc. Podeu activar o desactivar les actualitzacions setmanals a la pàgina d\'<a href="%sadmin/site/options.php">opcions del lloc</a>.</p>';

// Close site
$string['Close'] = 'Tanca';
$string['closesite'] = 'Tanca el lloc';
$string['closesitedetail'] = 'Podeu tancar l\'accés del lloc a tothom tret dels administradors. Això us pot ser útil quan hagueu d\'actualitzar la base de dades.  Només els administradors podran registrar-se fins que o bé reobriu el lloc o bé s\'hagi completat l\'actualització.';
$string['Open'] = 'Obre';
$string['reopensite'] = 'Reobre el lloc';
$string['reopensitedetail'] = 'El vostre lloc és tancat. Els administradors del lloc es mantindran registrats fins que es detecti una actualització.';

// Statistics
$string['siteinformation'] = 'Informació del lloc';
$string['viewfullsitestatistics'] = 'Veure estadístiques completes del lloc';
$string['sitestatistics'] = 'Estadístiques del lloc';
$string['siteinstalled'] = 'Data d\'instal·lació del lloc';
$string['databasesize'] = 'Mida de la base de dades';
$string['diskusage'] = 'Ús de disc';
$string['maharaversion'] = 'versió de Mahara';
$string['activeusers'] = 'Usuaris actius';
$string['loggedinsince'] = '%s avui, %s des de %s, %s des del principi';
$string['groupmemberaverage'] = 'De  mitjana cada usuari és a %s grups';
$string['viewsperuser'] = 'Els usuaris que han creat vistes tenen unes %s vistes cadascun';
$string['Cron'] = 'Cron';
$string['runningnormally'] = 'Corrent normalment';
$string['cronnotrunning'] = 'El Cron no està funcionant. <br>Trobareu instruccions per solucionar-ho a la <a href="http://wiki.mahara.org/System_Administrator\'s_Guide/Installing_Mahara">guia d\'instal·lació</a>.';
$string['Loggedin'] = 'Registrat';
$string['youraverageuser'] = 'El vostre usuari mitjà...';
$string['statsmaxfriends'] = 'Té %s amics (el que més és <a href="%s">%s</a> amb %d)';
$string['statsnofriends'] = 'Té 0 amics :(';
$string['statsmaxviews'] = 'Ha fet %s vistes (el que més és <a href="%s">%s</a> amb %d)';
$string['statsnoviews'] = 'Ha fet 0 vistes :(';
$string['statsmaxgroups'] = 'És a %s grups (el que més és <a href="%s">%s</a> amb %d)';
$string['statsnogroups'] = 'És a 0 grups :(';
$string['statsmaxquotaused'] = 'Ha usat al voltant de %s d\'espai de disc (el que més és <a href="%s">%s</a> amb %s)';
$string['groupcountsbytype'] = 'Nombre de grups per tipus de grup';
$string['groupcountsbyjointype'] = 'Nombre de grups per tipus d\'accés';
$string['blockcountsbytype'] = 'Blocs usats més sovint a les vistes:';
$string['uptodate'] = 'fins ara';
$string['latestversionis'] = 'la darrera versió és <a href="%s">%s</a>';
$string['viewsbytype'] = 'Vistes per tipus';
$string['userstatstabletitle'] = 'Estadístiques d\'usuaris per dia';
$string['groupstatstabletitle'] = 'Grups més grans';
$string['viewstatstabletitle'] = 'Vistes més populars';

// Site options
$string['adminsonly'] = 'Només administradors';
$string['adminsandstaffonly'] = 'Només Administrators i directius';
$string['advanced'] = 'Advançat';
$string['allowpublicviews'] = 'Permet vistes públiques';
$string['allowpublicviewsdescription'] = 'Si trieu l\'opció Sí els usuaris podran crear Vistes accessibles tant al públic com als usuaris identificats.';
$string['allowpublicprofiles'] = 'Permet els perfils públics';
$string['allowpublicprofilesdescription'] = 'Si marqueu que sí els usuaris podran fer que les vistes del seu perfil siguin accessibles tant al públic en general com als usuaris registrats';
$string['anonymouscomments'] = 'Comentaris anònims';
$string['anonymouscommentsdescription'] = 'Si l\'activeu aleshores els usuaris no registrats podran deixar comentaris a le spàgines públiques o a les que tinguin accés a través de la URL secreta.';
$string['antispam'] = 'Anti-spam';
$string['antispamdescription'] = 'Tipus de mesures anti-spam utilitzades en els formularis vivibles públicament.';
$string['defaultaccountinactiveexpire'] = 'Temps d\'inactivitat d\'un compte per defecte';
$string['defaultaccountinactiveexpiredescription'] = 'Quant de temps pot estar estar actiu un compte d\'usuari sense que l\'usuari entri al lloc i hi tingui activitat';
$string['defaultaccountinactivewarn'] = 'Temps d\'avís d\'expiració/inactivitat.';
$string['defaultaccountinactivewarndescription'] = 'Temps en què es rebrà un avís abans que els comptes d\'usuari no expirin o siguin inactius';
$string['defaultaccountlifetime'] = 'Durada d\'un compte per defecte';
$string['defaultaccountlifetimedescription'] = 'Si l\'activeu els comptes d\'usuari expiraran un cop passat aquest període de temps a partir del moment de la creació';
$string['embeddedcontent'] = 'Contingut incrustat';
$string['embeddedcontentdescription'] = 'A continuació podreu escollir quins llocs són de confiança. Els usuaris podran, aleshores, incrustar-ne  vídeos o altre material als seus ePortafolis.';
$string['Everyone'] = 'Tothom';
$string['homepageinfo'] = 'Mostra la informació de la pàgina d\'inici';
$string['homepageinfodescription'] = 'Si l\'activeu es mostrarà a la pàgina d\'inici del lloc informació sobre Mahara i el seu ús. Els usuaris registrats poden desactivar-ho.';
$string['institutionautosuspend'] = 'Auto-suspèn institucions caducades';
$string['institutionautosuspenddescription'] = 'Si l\'activeu les institucions caducades seran suspeses automàticament';
$string['institutionexpirynotification'] = 'Avís del temps restant per l\'expiració de la institució';
$string['institutionexpirynotificationdescription'] = 'Un temps abans que el lloc expiri s\'enviarà un missatge als administradors institucionals del lloc.';
$string['language'] = 'Llenguatge';
$string['none'] = 'Cap';
$string['country'] = 'País';
$string['pathtoclam'] = 'Camí del clam';
$string['pathtoclamdescription'] = 'Camí del sistema de fitxers per clamscan o clamdscan';
$string['registerterms'] = 'Acord per registrar-se';
$string['registertermsdescription'] = "Obliga als usuaris a accpetar els Termes i condicions abans de poder-se registrar. Hauríeu d\'editar els Termes i condicions del vostre lloc abans d\'activar aquesta opció.";
$string['allowmobileuploads'] = 'Permet les càrregues des de mòbils';
$string['allowmobileuploadsdescription'] = 'Si l\'activeu els usuaris tenen l\'opció de configurar un token d\autenticació. El contingut carregat amb aquest token es desarà com artefactes.';
$string['remoteavatars'] = 'Mostra avatars remots';
$string['remoteavatarsdescription'] = 'Si l\'activeu s\'utilitzarà el servei <a href="http://www.gravatar.com">Gravatar</a> com a proveïdeor per defecte de les imatges dels usuaris.';
$string['searchplugin'] = 'Connector de cerca';
$string['searchplugindescription'] = 'Connector de cerca a utilitzar';
$string['searchusernames'] = 'Cerca Noms d\'usuari';
$string['searchusernamesdescription'] = 'Permet que es pugui cercar per Nom d\'usuari com a part de la "Cerca d\'usuaris".';
$string['sessionlifetime'] = 'Durada de la sessió';
$string['sessionlifetimedescription'] = 'Durada en minuts abans que un usuari identificat sense activitat sigui tret automàticament del sistema';
$string['setsiteoptionsfailed'] = 'No s\'ha pogut configurar l\'opció %s ';
$string['showonlineuserssideblock'] = 'Mostra els usuaris en línia';
$string['showonlineuserssideblockdescription'] = 'Si l\'activeu els usuaris veuran un bloc lateral a la part dreta amb la llista d\'usuaris connectats.';
$string['showselfsearchsideblock'] = 'Activa la Cerca al Portafolis';
$string['showselfsearchsideblockdescription'] = 'Mostra el bloc lateral "Cerca al meu Portafolis" a la secció Portafolis del lloc';
$string['showtagssideblock'] = 'Activa el Núvol d\'etiquetes';
$string['showtagssideblockdescription'] = 'Si l\'activeu els usuaris veuran un bloc lateral a la secció Portafolis del lloc amb la llista delles etiquetes que usen més freqüentment.';
$string['simple'] = 'Simple';
$string['sitedefault'] = 'Lloc per defecte';
$string['sitelanguagedescription'] = 'Idioma del lloc per defecte';
$string['sitecountrydescription'] = 'País del lloc per defecte';
$string['sitename'] = 'Nom del lloc';
$string['sitenamedescription'] = 'El nom del lloc apareix en diferents punts del lloc i als correus enviats';
$string['siteoptionspagedescription'] = 'Aquí podreu configurar algunes opcions globals que, per defecte, s\'aplicaran a tot el lloc.';
$string['siteoptionsset'] = 'S\'han actualitzat les opcions del lloc.';
$string['sitethemedescription'] = 'El tema del lloc per defecte';
$string['smallviewheaders'] = 'Petits encapçalaments de les pàgines';
$string['smallviewheadersdescription'] = 'Si l\'activeu, es visualitzarà un petit encapçalament i un bloc de navegació pel lloc quan es vegin o s\'editin les pàgines del portafolis creades per l\'usuari.';
$string['spamhaus'] = 'Activa la Llista negra d\'URLs Spamhaus';
$string['spamhausdescription'] = 'Si l\'activeu, es comprovaran les URLs amb la Spamhaus DNSBL';
$string['surbl'] = 'Activa la Llista negra d\'URLs  SURBL';
$string['surbldescription'] = 'Si l\'activeu, es comprovaran les URLs amb la  SURBL DNSBL';
$string['disableexternalresources'] = 'Desactiva a l\'HTML de l\'usuari els recursos externs';
$string['disableexternalresourcesdescription'] = 'Desactiva la possibilitat d\'incrustar recursos externs per impedir que els usuaris incrustin material d\'altres llocs.';
$string['tagssideblockmaxtags'] = 'Nombre màxim d\'etiquetes al núvol';
$string['tagssideblockmaxtagsdescription'] = 'El número d\'etiquetes per defecte per mostrar al núvol d\'etiquetes de l\'usuari';
$string['trustedsites'] = 'Llocs de confiança';
$string['theme'] = 'Tema';
$string['updatesiteoptions'] = 'Actualitza les opcions del lloc';
$string['usersallowedmultipleinstitutions'] = 'Es permet que els usuaris pertanyin a diferents institucions';
$string['usersallowedmultipleinstitutionsdescription'] = 'Si l\'activeu, els usuaris podran ser alhora membres de diferents institucions';
$string['userscanchooseviewthemes'] = 'Els usuaris poden escollir el tema de les seves pàgines';
$string['userscanchooseviewthemesdescription'] = 'Si l\'activeu, els usuaris podran seleccionar un tema quan editin o creïn una pàgina al seu portafolis. La pàgina es mostrarà als altres usuaris amb el tema triat. ';
$string['userscanhiderealnames'] = 'Els usuaris poden amagar els noms reals';
$string['userscanhiderealnamesdescription'] = 'Si l\'activeu, els usuaris que han triat per mostrar un altre nom poden escollir el fet de ser cercats només pel nom mostrat i no se\'ls trobarà quan se\'ls busqui pel nom rea (A la secció d\'administració del lloc els usuaris sempre es poden cercar pels noms reals).';
$string['usersseenewthemeonlogin'] = 'Els usuaris veuran el tema nou la propera vegada que entrin.';
$string['viruschecking'] = 'Comprovar l\'existència de virus';
$string['viruscheckingdescription'] = 'Si l\'activeu s\'activarà la comprovació de virus als fitxers carregats amb ClamAV';
$string['whocancreatepublicgroups'] = 'Qui pot crear Grups públics';
$string['whocancreatepublicgroupsdescription'] = 'Si marqueu que sí, els usuaris podran crear grups que seran visibles pel públic en general';
$string['wysiwyg'] = 'Editor HTML global';
$string['wysiwygdescription'] = 'Defineix si s\'activa globalment l\'editor HTML o si es deixa els usuaris la tria.';
$string['wysiwyguserdefined'] = 'L\'usuari tria si vol editor HTML';


// Site content
$string['about']               = 'Sobre';
$string['discardpageedits']    = 'Voleu descartar els canvis d\'aquesta pàgina?';
$string['editsitepagespagedescription'] = 'Aquí podreu editar el contingut d\'algunes pàgines del lloc com la pàgina d\'inici dels usuaris no registrats, la dels usuaris registrats i les pàgines enllaçades des del peu.';
$string['home']                = 'Inici';
$string['loadsitepagefailed']  = 'No s\'ha pogut carregar la pàgina del lloc';
$string['loggedouthome']       = 'Pàgina d\'inici de sortida';
$string['pagename']            = 'Nom de la pàgina';
$string['pagesaved']           = 'Pàgina desada';
$string['pagetext']            = 'Text de la pàgina';
$string['privacy']             = 'Política de privacitat';
$string['savechanges']         = 'Desa els canvis';
$string['savefailed']          = 'No s\'ha pogut desar';
$string['sitepageloaded']      = 'Pàgina del lloc carregada';
$string['termsandconditions']  = 'Termes i Condicions';
$string['uploadcopyright']     = 'Carrega les condicions del Copyright';

// Links and resources menu editor
$string['sitefile']            = 'Fitxer del lloc';
$string['adminpublicdirname']  = 'public';  // Name of the directory in which to store public admin files
$string['adminpublicdirdescription'] = 'Fitxers accessibles per visitants sense registrar';
$string['badmenuitemtype']     = 'Tipus d\'ítem desconegut';
$string['confirmdeletemenuitem'] = 'Esteu segur que voleu esborrar aquest ítem?';
$string['deletingmenuitem']    = 'Esborrant ítem';
$string['deletefailed']        = 'No s\'ha pogut esborrar l\'ítem';
$string['externallink']        = 'Enllaç extern';
$string['editmenus']           = 'Edita enllaços i recursos';
$string['linkedto']            = 'Enllaçat a';
$string['linksandresourcesmenupagedescription'] = 'El menú d\'Enllaços i recursos apareix a tots els usuaris a la majoria de pàgines. Podeu afegir enllaços a altres pàgines web i a fitxers carregats a la secció  %sAdmin Files%s .';
$string['loadingmenuitems']    = 'Carregant ítems';
$string['loadmenuitemsfailed'] = 'No s\'han pogut carregar els ítems';
$string['loggedinmenu']        = 'Enllaços i recursos pels usuaris registrats';
$string['loggedoutmenu']       = 'Enllaços i recursos pels usuaris no registrats';
$string['menuitemdeleted']     = 'Ítem esborrat';
$string['menuitemsaved']       = 'Ítem desat';
$string['menuitemsloaded']     = 'Ítems carregats';
$string['name']                = 'Nom';
$string['nositefiles']         = 'No hi ha cap fitxer del lloc';
$string['public']              = 'públic';
$string['savingmenuitem']      = 'Desant ítem';
$string['type']                = 'Tipus';
$string['footermenu']          = 'Menu al peu';
$string['footermenudescription'] = 'Activa o desactiva els enllaços del peu.';
$string['footerupdated']       = 'S\'ha actualitzat el peu';

// Admin Files
$string['adminfilespagedescription'] = 'Des d\'aquí podeu carregar fitxers que poden incloure al menú %sEnllaços i recursos%s. Els fitxers del directori d\'inici es podran afegir al menú dels usuaris registrats, mentre que els del directori públic es podran afegir al menú públic dels usuaris no registrats.';

// Networking options
$string['networkingextensionsmissing'] = 'No podeu configurar la xarxa Mahara perquè a la vostra  instal·lació de PHP li manca una o més extensions requerides:';
$string['publickey'] = 'Clau pública';
$string['publickeydescription2'] = 'Aquesta clau pública es genera automàticament i canvia cada  %s dies';
$string['publickeyexpires'] = 'La clau pública expira';
$string['enablenetworkingdescription'] = 'Permeteu que el vostre servidor Mahara es comuniqui amb servidors de Moodle i altres aplicacions';
$string['enablenetworking'] = 'Activa la xarxa';
$string['networkingenabled'] = 'S\'ha activat la xarxa. ';
$string['networkingdisabled'] = 'S\'ha desactivat la xarxa. ';
$string['networkingpagedescription'] = 'Les característiques de xarxa de Mahara li permeten comunicar-se amb altres llocs Mahara o Moodle  instal·lats a la mateixa màquina o en d\'altres. Si activeu la xarxa, la podeu utilitzar per configurar una única identificació dels usuaris (SSO o single-sign-on)  provinguin bé de Mahara, bé de Moodle.';
$string['networkingunchanged'] = 'No s\'ha canviat la configuració de xarxa';
$string['promiscuousmode'] = 'Auto-registre de tots els hosts';
$string['promiscuousmodedisabled'] = 'S\'ha desactivat l\'Auto-registre. ';
$string['promiscuousmodeenabled'] = 'S\'ha activat l\'Auto-registre. ';
$string['promiscuousmodedescription'] = 'Crea un registre d\'institució per cada host que connecti al vostre i permet als seus usuaris que entrin a Mahara.';
$string['wwwroot'] = 'Arrel WWW';
$string['wwwrootdescription'] = 'Aquesta és la URL en la qual els usuaris accediran a aquesta instal·lació de Mahara i la de les claus SSL que genera';
$string['proxysettings'] = 'Configuració del servidor intermediari';
$string['proxyaddress'] = 'Adreça del servidor intermediari';
$string['proxyaddressdescription'] = 'Si el vostre lloc utilitza un servidor intermediari per accedir a Internet, especifiqueu els servidors intermediaris en notació <em>hostname:portnumber</em>';
$string['proxyaddressset'] = 'S\'ha desat l\'adreça del servidor intermediari';
$string['proxyauthmodel'] = 'Model autenticat de servidor intermediari';
$string['proxyauthmodeldescription'] = 'Si cal, seleccioneu el vostre model d\'autenticació del servidor intermediari';
$string['proxyauthmodelset'] = 'S\'ha desat el model d\'autenticació del servidor intermediari';
$string['proxyauthcredentials'] = 'Credencials del servidor intermediari';
$string['proxyauthcredentialsdescription'] = 'Escriviu les credencials del servidor intermediari necessàries per autenticar el vostre servidor en el format <em>username:password</em>';
$string['proxyauthcredntialsset'] = 'S\'han desat les credencials del servidor intermediari';

// Upload CSV
$string['csvfile'] = 'Fitxer CSV';
$string['emailusersaboutnewaccount'] = 'Envia un missatge als usuaris sobre el seu compte?';
$string['emailusersaboutnewaccountdescription'] = 'S\'ha d\'enviar un missatge als nous usuaris informant-los sobre els detalls del nou compte?';
$string['forceuserstochangepassword'] = 'Força el canvi de contrasenya?';
$string['forceuserstochangepassworddescription'] = 'S\'ha de forçar el canvi de contrasenya dels nous usuaris el primer cop que entrin al lloc?';
$string['uploadcsvinstitution'] = 'La institució i el mètode d\'autenticació dels nous usuaris';

$string['csvfiledescription'] = 'Fitxer que conté els usuaris que s\'han d\'afegir';
$string['uploadcsverrorinvalidfieldname'] = 'El nom del camp "%s" no és vàlid';
$string['uploadcsverrorrequiredfieldnotspecified'] = 'No s\'ha especificat el camp requerit "%s" a la línia de format';
$string['uploadcsverrornorecords'] = 'Sembla que el fitxer no conté cap registre tot i que la capçalera és correcta';
$string['uploadcsverrorunspecifiedproblem'] = 'No s\'han pogut afegir els registres del fitxer CSV. Si el fitxer té el format correcte significa que hi ha un bug i hauríeu de <a href="https://eduforge.org/tracker/?func=add&group_id=176&atid=739">fer un informe de bug</a>, tot afegint el fitxer CSV (recordeu-vos d\'esborrar les contrasenyes) i, si és possible, el fitxer log amb l\'error.';
$string['uploadcsverrorinvalidemail'] = 'Hi ha un error a la línia %s del vostre fitxer. L\'adreça de correu d\'aquest usuari no està en el format correcte.';
$string['uploadcsverrorincorrectnumberoffields'] = 'Hi ha un error a la línia %s del vostre fitxer. Aquesta línia no té el nombre correcte de camps.';
$string['uploadcsverrorinvalidpassword'] = 'Hi ha un error a la línia %s del vostre fitxer. La contrasenya d\'aquest usuari no està en el format correcte';
$string['uploadcsverrorinvalidusername'] = 'Hi ha un error a la línia %s del vostre fitxer. El nom d\'usuari d\'aquest usuari no està en el format correcte';
$string['uploadcsverrormandatoryfieldnotspecified'] = 'La línia %s del fitxer no té el camp requerit "%s"';
$string['uploadcsverroruseralreadyexists'] = 'La línia %s del fitxer especifica un nom d\'usuari "%s" que ja existeix';
$string['uploadcsverroremailaddresstaken'] = 'La línia %s del fitxer especifica una adreça de correu "%s" que ja és utilitzada per un altre usuari';
$string['uploadcsvpagedescription2'] = '<p>Podeu utilitzar aquesta opció per carregar usuaris amb un fitxer <acronym title="Comma Separated Values">CSV</acronym>.</p>

<p>La primera fila del fitxer CSV ha d\'especificar el format de les dades. Per exemple, ha de tenir un aspecte semblant a aquest:</p>

<pre>username,password,email,firstname,lastname,studentid</pre>

<p>Aquesta fila ha d\'incloure sempre els camps <tt>username</tt>, <tt>password</tt>, <tt>email</tt>, <tt>firstname</tt> i <tt>lastname</tt>. També ha d\'incloure els camps que heu determinat com a obligatoris pels usuaris i cada camp tancat de les institucions des d\'on carregueu usuaris. Podeu <a href="%s">configurar els camps obligatoris</a> per a totes les institucions o bé <a href="%s">configurar els camps tancats per a cada institució</a>.</p>

<p>El fitxer CSV pot incloure qualsevol altre camp del perfil que desitgeu. La llista completa de camps és:</p>

%s';
$string['uploadcsvpagedescription2institutionaladmin'] = '<p>Podeu utilitzar aquesta opció per carregar usuaris amb un fitxer <acronym title="Comma Separated Values">CSV</acronym>.</p>

<p>La primera fila del fitxer CSV ha d\'especificar el format de les dades. Per exemple, ha de tenir un aspecte semblant a aquest:</p>

<pre>username,password,email,firstname,lastname,studentid</pre>

<p>Aquesta fila ha d\'incloure sempre els camps <tt>username</tt>, <tt>password</tt>, <tt>email</tt>, <tt>firstname</tt> i <tt>lastname</tt>. També ha d\'incloure els camps que l\'administrador del lloc ha determinat que siguin obligatoris i els camps tancats per a la vostra institució. Podeu <a href="%s">configurar  els camps tancats</a> de la(les) institució(ons) que gestioneu.</p>

<p>El fitxer CSV pot incloure qualsevol altre camp del perfil que desitgeu. La llista completa de camps és:</p>

%s';
$string['uploadcsvsomeuserscouldnotbeemailed'] = 'Alguns usuaris no poden rebre correu electrònic bé perquè l\'adreça no és correcta, bé perquè Mahara corre en un servidor que no té configurat l\'enviament de correu. Però si voleu podeu contactar amb aquests usuaris manualment:';
$string['uploadcsvusersaddedsuccessfully'] = 'S\'han afegit correctament els usuaris del fitxer';
$string['uploadcsvfailedusersexceedmaxallowed'] = 'No s\'ha afegit cap usuari perquè n\'hi ha massa al vostre fitxer. El nombre d\'usuaris de la institució pot haver excedit el màxim permès.';

// Admin Users
$string['adminuserspagedescription'] = '<p>Aquí podeu escollir quins usuaris són administradors del lloc. Els administradors actuals estan llistats a la dreta i els potencials a l\'esquerra.</p><p>El sistema ha de tenir al menys un administrador.</p>';
$string['institutionadminuserspagedescription'] = 'Aquí podeu escollir quins usuaris són administradors de la institució. Els administradors actuals estan llistats a la dreta i els potencials a l\'esquerra.';
$string['potentialadmins'] = 'Administradors potencials';
$string['currentadmins'] = 'Administradors actuals';
$string['adminusersupdated'] = 'S\'han actualitzat els usuaris administradors';

// Staff Users
$string['staffuserspagedescription'] = 'Aquí podeu escollir quins usuaris formen part de la plantilla del lloc. La plantilla actual és a la dreta i la plantilla potencial a l\'esquerra.';
$string['institutionstaffuserspagedescription'] = 'Aquí podeu escollir quins usuaris formen part de la plantilla del lloc. La plantilla actual és a la dreta i la plantilla potencial a l\'esquerra.';
$string['potentialstaff'] = 'Plantilla potencial';
$string['currentstaff'] = 'Plantilla actual';
$string['staffusersupdated'] = 'S\'han actualitzat els usuaris de plantilla';

// Admin Notifications

// Suspended Users
$string['deleteusers'] = 'Esborra usuaris';
$string['confirmdeleteusers'] = 'Esteu segur que voleu esborrar els usuaris seleccionats?';
$string['exportingnotsupportedyet'] = 'No està suportada encara l\'exportació de perfils d\'usuari';
$string['exportuserprofiles'] = 'Exporta els perfils d\'usuari';
$string['nousersselected'] = 'No hi ha cap usuari seleccionat';
$string['suspenduser'] = 'Suspèn l\'usuari';
$string['suspendedusers'] = 'Usuaris suspesos';
$string['suspensionreason'] = 'Causa de la suspensió';
$string['errorwhilesuspending'] = 'S\'ha produït un error en el procés de suspensió';
$string['suspendedusersdescription'] = 'Suspèn o reactiva usuaris del lloc';
$string['unsuspendusers'] = 'Aixeca la suspensió d\'usuaris';
$string['usersdeletedsuccessfully'] = 'S\'han esborrat correctament els usuaris';
$string['usersunsuspendedsuccessfully'] = 'S\'ha aixecat correctament la suspensió dels usuaris';
$string['suspendingadmin'] = 'Suspèn administrador';
$string['usersuspended'] = 'Usuari suspès';
$string['userunsuspended'] = 'Usuari aixecat de suspensió';

// User account settings
$string['accountsettings'] = 'Configuració del compte';
$string['siteaccountsettings'] = 'Configuració dels comptes del lloc';
$string['resetpassword'] = 'Re-assigna contrasenya';
$string['resetpassworddescription'] = 'El que escriviu aquí reemplaçarà la contrasenya actual de l\'usuari.';
$string['forcepasswordchange'] = 'Força el canvi de contrasenya en la propera entrada';
$string['forcepasswordchangedescription'] = 'Es dirigirà l\'usuari a una pàgina per canviar la contrasenya la propera vegada que entri al lloc.';
$string['sitestaff'] = 'Plantilla del lloc';

$string['siteadmins'] = 'Administradors del lloc';
$string['siteadmin'] = 'Administrador del lloc';

$string['accountexpiry'] = 'El compte expira';
$string['accountexpirydescription'] = 'Data en la qual es desactiva automàticament el compte de l\'usuari.';
$string['suspended'] = 'Suspès';
$string['suspendedreason'] = 'Raó per la suspensió';
$string['suspendedreasondescription'] = 'Text que veurà l\'usuari suspès la propera vegada que intenti entrar.';
$string['unsuspenduser'] = 'Usuari aixecat de suspensió';
$string['thisuserissuspended'] = 'Aquest usuari ha estat suspès.';
$string['suspendedby'] = 'Aquest usuari ha estat suspès per %s';
$string['deleteuser'] = 'Esborra usuari';
$string['userdeletedsuccessfully'] = 'S\'ha esborrat l\'usuari';
$string['confirmdeleteuser'] = 'Esteu segur que voleu esborrar aquest usuari?';
$string['filequota'] = 'Quota d\'espai (MB)';
$string['filequotadescription'] = 'Espai total disponible a l\'àrea de fitxers de l\'usuari.';
$string['addusertoinstitution'] = 'Afegeix un usuari a la institució';
$string['removeuserfrominstitution'] = 'Esborra un usuari de la institució';
$string['confirmremoveuserfrominstitution'] = 'Esteu segur que voleu esborrar l\'usuari d\'aquesta institució?';
$string['usereditdescription'] = 'Aquí podreu veure i configurar els detalls del compte de l\'usuari. Més avall també podreu <a href="#suspend">suspendre o esborrar aquest compte</a>, o bé canviar les opcions d\'aquest usuari a les <a href="#institutions">institutions a què pertany</a>.';
$string['suspenddeleteuser'] = 'Suspèn/Esborra usuari';
$string['suspenddeleteuserdescription'] = 'Aquí podreu suspendre o esborrar completament un compte d\'usuari. Els usuaris suspesos no poden entrar al lloc fins que no se\'ls aixeca la suspensió. Observeu que mentre que una suspensió pot ser aixecada <strong>l\'eliminació d\'un usuari és irreversible</strong>.';
$string['deleteusernote'] = 'Fixeu-vos perquè <strong>aquesta operació és irreversible</strong>, no la podreu desfer.';

// Add User
$string['adduser'] = 'Afegeix usuari';
$string['adduserdescription'] = 'Crea un nou usuari';
$string['adduserpagedescription'] = '<p>Aquí podeu afegir un nou usuari al sistema. Un cop afegit, rebrà un missatge informant-lo dels detalls del nou compte, incloent els seu nom d\'usuari i contrasenya, i demanant-li que la canviï el primer cop que accedeixi al sistema.</p>';
$string['createuser'] = 'S\'ha creat l\'usuari';
$string['newuseremailnotsent'] = 'No s\'ha pogut enviar el missatge de benvinguda al nou usuari.';

// Login as
$string['loginasuser'] = 'Entra com %s';
$string['becomeadminagain'] = 'Torna a ser %s un altre cop';
// Login-as exceptions
$string['loginasdenied'] = 'Heu intentat entrar com un altre usuari i no en teniu el permís';
$string['loginastwice'] = 'Heu intentat entrar com un altre usuari quan ja estàveu identificats com un altre usuari';
$string['loginasrestorenodata'] = 'No hi ha dades d\'usuari per restaurar';
$string['loginasoverridepasswordchange'] = 'Mentre estigueu emmascarat com un altre usuari, podeu escollir %sentra de qualsevol manera%s, ignorant així la pantalla de canvi de contrasenya.';

// Institutions
$string['Add'] = 'Afegeix';
$string['admininstitutions'] = 'Administra institucions';
$string['adminauthorities'] = 'Administra autoritats';
$string['addinstitution'] = 'Afegeix institució';
$string['authplugin'] = 'Connector d\'autenticació';
$string['deleteinstitution'] = 'Esborra institució';
$string['deleteinstitutionconfirm'] = 'Esteu segur que voleu esborrar aquesta institució?';
$string['institutionaddedsuccessfully2'] = 'S\'ha afegit correctament la institució.';
$string['institutiondeletedsuccessfully'] = 'S\'ha esborrat correctament la institució.';
$string['noauthpluginforinstitution'] = 'L\'administrador del vostre lloc no ha configurat un connector d\'autenticació per a aquesta institució.';
$string['adminnoauthpluginforinstitution'] = 'Configureu un connector d\'autenticació per a aquesta institució.';
$string['institutionname'] = 'Nom de la institució';
$string['institutionnamealreadytaken'] = 'Aquest nom d\'institució ja està sent utilitzat per una altra.';
$string['institutiondisplayname'] = 'Mostra el nom de la institució';
$string['institutionupdatedsuccessfully'] = 'S\'ha actualitzat correctament la institució.';
$string['registrationallowed'] = 'Permeteu el registre?';
$string['registrationalloweddescription2'] = 'Si l\'activeu els usuaris es podran registrar al sistema en aquesta institució usant el formulari de registre.';
$string['defaultmembershipperiod'] = 'Període de pertinença per defecte';
$string['defaultmembershipperioddescription'] = 'Durant quan de temps els membres restaran associats a la institució';
$string['authenticatedby'] = 'Mètode d\'autenticació';
$string['authenticatedbydescription'] = '';
$string['remoteusername'] = 'Nom d\'usuari per l\'autenticació externa';
$string['remoteusernamedescription'] = 'Escriviu aquí el nom d\'usuari remot si l\'usuari s\'autentica per un mètode extern i voleu associar-lo amb la identitat de la seva base de dades remota.';
$string['institutionsettings'] = 'Configuració de la institució';
$string['institutionsettingsdescription'] = 'Aquí podreu canviar les opcions d\'aquest usuari pel que fa a la seva pertinença a les institucions del sistema.';

$string['changeinstitution'] = 'Canvia institució';
$string['institutionstaff'] = 'Plantilla de la institució';
$string['institutionadmins'] = 'Administradors de la institució';
$string['institutionadmin'] = 'Administrador de la institució';
$string['institutionadministrator'] = 'Administrador institucional';
$string['institutionadmindescription'] = 'Si l\'activeu, l\'usuari podrà administrar tots els usuaris d\'aquesta institució.';
$string['settingsfor'] = 'Configuració de:';
$string['institutionadministration'] = 'Administració de la institució';
$string['institutionmembers'] = 'Membres de la institució';
$string['notadminforinstitution'] = 'No sou administrador d\'aquesta institució';
$string['institutionmemberspagedescription'] = 'En aquesta pàgina podreu veure els usuaris que han demanat ser membres de la vostra institució i afegir-los-hi. També podeu esborrar usuaris de la institució i convidar-ne de nous.';

$string['institutionusersinstructionsrequesters'] = 'La llista d\'usuaris de l\'esquerra mostra tots els usuaris que han demanat unir-se a la vostra institució. Utilitzeu el formulari de cerca per reduir el nombre d\'usuaris que s\'hi mostra. Si voleu afegir usuaris a la institució o rebutjar la seva sol·licitud cal que primer mogueu alguns usuaris a mà dreta tot seleccionant un o més noms i, en acabat, clicant a la fletxa cap a la dreta. El botó "Afegeix membres" afegirà tots els usuaris cap a la institució, cap a la dreta. El botó "Rebutja sol·licituds" esborrarà les sol·licituds dels membres de la dreta.';
$string['institutionusersinstructionsnonmembers'] = 'La llista d\'usuaris de l\'esquerra mostra tots els usuaris que encara no són membres de la vostra institució. Utilitzeu el formulari de cerca per reduir el nombre d\'usuaris que s\'hi mostra. Per convidar nous usuaris a ser membres de la institució cal que primer mogueu alguns usuaris a la dreta tot seleccionant un o més noms i, en acabat, clicant la fletxa dreta per moure\'ls a la llista de mà dreta. El botó "Convida usuaris" enviarà les invitacions a tots els usuaris de mà dreta, però no estaran associats a la institució fins que acceptin la invitació';
$string['institutionusersinstructionsmembers'] = 'La llista d\'usuaris de l\'esquerra mostra tots els membres de la institució. Utilitzeu el formulari de cerca per reduir el nombre d\'usuaris que s\'hi mostra. Per esborrar usuaris de la institució cal que primer mogueu alguns usuaris a la dreta tot seleccionant un o més noms i, en acabat, clicant la fletxa dreta per moure\'ls a la llista de mà dreta.  El botó "Esborra usuaris" esborrarà de la institució tots els usuaris de la dreta, però els de l\'esquerra romandran a la institució.';

$string['editmembers'] = 'Edita membres';
$string['editstaff'] = 'Edita plantilla';
$string['editadmins'] = 'Edita administradors';
$string['membershipexpiry'] = 'La pertinença expira';
$string['membershipexpirydescription'] = 'Data en la qual l\'usuari serà automàticament esborrat d\'aquesta institució.';
$string['studentid'] = 'Número ID';
$string['institutionstudentiddescription'] = 'Identificador opcional específic de la institució. L\'usuari no pot editar aquest camp.';

$string['userstodisplay'] = 'Usuaris a mostrar:';
$string['institutionusersrequesters'] = 'Persones que han demanat ser membres de la institució';
$string['institutionusersnonmembers'] = 'Persones que encara no han demanat ser membres de la institució';
$string['institutionusersmembers'] = 'Persones que ja són membres de la institució';

$string['addnewmembers'] = 'Afegeix membres nous';
$string['addnewmembersdescription'] = '';
$string['usersrequested'] = 'Usuaris que han demanat ser membres';
$string['userstobeadded'] = 'Usuaris que s\'han d\'afegir com a membres';
$string['userstoaddorreject'] = 'Usuaris que s\'han d\'afegir/rebutjar';
$string['addmembers'] = 'Afegeix membres';
$string['inviteuserstojoin'] = 'Convida usuaris a unir-se a la institució';
$string['Non-members'] = 'No-membres';
$string['userstobeinvited'] = 'Usuaris per convidar';
$string['inviteusers'] = 'Convida usuaris';
$string['removeusersfrominstitution'] = 'Esborra usuaris de la institució';
$string['currentmembers'] = 'Membres actuals';
$string['userstoberemoved'] = 'Usuaris a esborrar';
$string['removeusers'] = 'Esborra usuaris';
$string['declinerequests'] = 'Rebutja sol·licituds';
$string['nousersupdated'] = 'No s\'ha actualitzat cap usuari';

$string['institutionusersupdated_addUserAsMember'] = 'S\'han afegit els usuaris';
$string['institutionusersupdated_declineRequestFromUser'] = 'S\'han rebutjat les sol·licituds';
$string['institutionusersupdated_removeMembers'] = 'S\'han esborrat els usuaris';
$string['institutionusersupdated_inviteUser'] = 'S\'han enviat les invitacions';

$string['maxuseraccounts'] = 'Nombre màxim d\'usuaris permès';
$string['maxuseraccountsdescription'] = 'El nombre màxim de comptes d\'usuari que es pot associar a aquesta institució. Deixeu aquest camp en blanc si no hi ha límit.';
$string['institutionuserserrortoomanyusers'] = 'No s\'han afegit els usuaris. El nombre de membres no pot excedir el màxim permès per la institució. Es poden afegir menys usuaris, esborrar-ne alguns de la institució o demanar a l\'administrador del lloc que augmenti el nombre d\'usuaris.';
$string['institutionuserserrortoomanyinvites'] = 'No s\'han enviat les invitacions. El nombre de membres existent més el nombre d\'invitacions no pot excedir el màxim d\'usuaris de la institució. Podeu convidar menys usuaris, esborrar-ne algun o demanar a l\'administrador del lloc que augmenti el nombre d\'usuaris.';

$string['Members'] = 'Membres';
$string['Maximum'] = 'Màxim';
$string['Staff'] = 'Plantilla';
$string['Admins'] = 'Administradors';

$string['noinstitutions'] = 'No hi ha institucions';
$string['noinstitutionsdescription'] = 'Heu de crear una institució abans de poder-hi associar membres.';
$string['Lockedfields'] = 'Camps tancats';
// Admin User Search
$string['Query'] = 'Consulta';
$string['Institution'] = 'Institució';
$string['confirm'] = 'confirma';
$string['invitedby'] = 'Invitat per';
$string['requestto'] = 'Demana a';
$string['useradded'] = 'S\'ha afegit l\'usuari';
$string['invitationsent'] = 'S\'ha enviat la invitació';

// general stuff
$string['notificationssaved'] = 'S\'ha desat la configuració de les notificacions';
$string['onlyshowingfirst'] = 'Mostra només el primer';
$string['resultsof'] = 'resultats de';

$string['installed'] = 'Instal·lat';
$string['errors'] = 'Errors';
$string['install'] = 'Instal·la';
$string['reinstall'] = 'Reinstal·la';
?>
