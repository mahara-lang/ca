<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */


defined('INTERNAL') || die();

// @todo<nigel>: most likely need much better descriptions here for these environment issues
$string['phpversion'] = 'Mahara no funciona amb PHP < 5.1.3. Actualitzeu la versió del PHP  o canvieu la instal·lació de Mahara a un altre host.';
$string['jsonextensionnotloaded'] = 'La configuració del servidor no inclou l\'extensió JSON. Mahara la necessita per enviar algunes dades al i des del navegador. Assegureu-vos que es carrega a php.ini o instal·leu-la si cal.';
$string['pgsqldbextensionnotloaded'] = 'La configuració del servidor no inclou l\'extensió pgsql. Mahara la necessita per emmagatzemar dades en una base de dades relacional. Assegureu-vos que es carrega a php.ini o instal·leu-la si cal.';
$string['mysqldbextensionnotloaded'] = 'La configuració del servidor no inclou l\'extensió mysql. Mahara la necessita per emmagatzemar dades en una base de dades relacional. Assegureu-vos que es carrega a php.ini o instal·leu-la si cal.';
$string['unknowndbtype'] = 'La configuració del servidor referència una tipus de base de dades desconegut. Els valors vàlids són: "postgres8" i "mysql5". Canvieu el tipus de BD al fitxer de configuració config.php.';
$string['xmlextensionnotloaded'] = 'La configuració del vostre servidor no inclou l\'extensió %s. Mahara la necessita per analitzar dades XML d\'una gran varietat de fonts. Assegureu-vos que es carrega a php.ini o instal·leu-la si cal.';
$string['gdfreetypenotloaded'] = 'La configuració de l\'extensió gd del vostre servidor no inclou suport per a Freetype support. Mahara el necessita per poder construir les imatges CAPTCHA. Si us plau, assegureu-vos que configurar correctament gd.';
$string['gdextensionnotloaded'] = 'La configuració del servidor no inclou l\'extensió gd. Mahara la necessita per realitzar operacions amb les imatges carregades. Assegureu-vos que es carrega a php.ini o instal·leu-la si cal.';
$string['curllibrarynotinstalled'] = 'La configuració del servidor no inclou l\'extensió curl. Mahara a necessita per integrar-se amb Moodle i per accedir a alimentacions RSS remotes. Assegureu-vos que curl és al fitxer php.ini, o instal·leu-la si calgués.';
$string['sessionextensionnotloaded'] = 'La configuració del servidor no inclou l\'extensió session. Mahara la necessita per donar suport al registre d\'usuaris. Assegureu-vos que es carrega a php.ini o instal·leu-la si cal.';
$string['curllibrarynotinstalled'] = 'La configuració del servidor no inclou l\'extensió curl. Mahara  la necessita per fer la integració amb Moodle i per arribar a les alimentacions RSS externes. Assegureu-vos que curl és al fitxer php.ini, o instal·leu-lo si no hi fos.';
$string['registerglobals'] = 'Teniu una configuració de PHP de risc: register_globals està activat. Mahara prova d\'ignorar-ho, però ho heu de solucionar.';
$string['magicquotesgpc'] = 'Teniu una configuració de PHP de risc: magic_quotes_gpc està activat. Mahara prova d\'ignorar-ho, però ho heu de solucionar.';
$string['magicquotesruntime'] = 'Teniu una configuració de PHP de risc: magic_quotes_runtime està activat. Mahara prova d\'ignorar-ho, però ho heu de solucionar.';
$string['magicquotessybase'] = 'Teniu una configuració de PHP de risc:, magic_quotes_sybase is on. Mahara prova d\'ignorar-ho, però ho heu de solucionar.';

$string['safemodeon'] = 'Sembla que el servidor està funcionant en safe mode. Mahara no pot funcionar en safe mode així que ho haureu de desactivar al fitxer php.ini o a la configuració d\'Apache del lloc.

Si esteu en un hosting compartit és difícil que pugueu desactivar safe_mode a no ser que contacteu amb l\'administrador del lloc. Potser caldrà que considereu canviar-vos de lloc.';
$string['apcstatoff'] = 'Sembla que el servidor està fent córrer APC amb apc.stat=0. Mahara no pot funcionar amb aquesta configuració. Heu de configurar apc.stat=1 al fitxer php.ini.

Si esteu en un hosting compartit és difícil que pugueu activar apc.stat a no ser que contacteu amb l\'administrador del lloc. Potser caldrà que considereu canviar-vos de lloc.';
$string['datarootinsidedocroot'] = 'Heu configurat el directori de dades dins del directori arrel de documents. Això és un greu problema de seguretat perquè qualsevol pot demanar dades de la sessió d\'altres usuaris o veure fitxers sobre els que no té permís. Configureu el directori de dades per a què estigui fora del directori arrel de documents.';
$string['datarootnotwritable'] = 'No es pot escriure al directori de dades, %s. Això vol dir que ni les dades de la sessió, ni els fitxers d\'usuari ni res que hagi de ser carregat es podrà desar al servidor. Heu de crear el directori si no existís o, si existeix, canvieu-ne els permisos.';
$string['couldnotmakedatadirectories'] = 'Per alguna raó no s\'han pogut crear alguns directoris del nucli de dades. Això no hauria de passar perquè prèviament Mahara ha detectat que podia escriure al directori de dades. Comproveu els permisos del directori de dades.';

$string['dbconnfailed'] = 'Mahara no es pot connectar a la base de dades.

 * Si esteu usant Mahara, espereu un minut i torneu-ho a provar.
 * Si sou l\'administrador comproveu la configuració de la BD i assegureu-vos que s\'hi pot accedir.

l\'error rebut és:
';
$string['dbnotutf8'] = 'No esteu usant una base de dades UTF-8. Mahara guarda internament totes les dades com UTF-8. Cal que recreeu la vostra base de dades usant la codificació UTF-8.';
$string['dbversioncheckfailed'] = 'La vostra versió de la base de dades no és prou nova per fer córrer Mahara. El servidor és %s %s, però Mahara requereix com a mínim la versió %s.';

// general exception error messages
$string['blocktypenametaken'] = "El tipus de bloc %s l'utilitza un altre connector (%s)";
$string['artefacttypenametaken'] = "El tipus d'artefacte %s l'utilitza un altre connector (%s)";
$string['artefacttypemismatch'] = "El tipus d'artefacte no s\'ajusta, esteu provant d\'usar aquest %s com a %s";
$string['classmissing'] = "No s'ha trobat la classe %s del tipus %s al connector %s";
$string['artefacttypeclassmissing'] = "Tots els tipus d'artefacte implementen una classe. No s'ha trobat %s";
$string['artefactpluginmethodmissing'] =  "El connector d'artefactes %s ha d'implementar %s però no ho fa";
$string['blocktypelibmissing'] = 'No s\'ha trobat lib.php pel bloc %s al connector d\'artefactes %s';
$string['blocktypemissingconfigform'] = 'El tipus de bloc %s ha d\'implementar al instància instance_config_form';
$string['versionphpmissing'] = 'No es troba version.php al connector %s %s';
$string['blocktypeprovidedbyartefactnotinstallable'] = 'S\'instal·larà com a part de la instal·lació del connector d\'artefactes %s';
$string['blockconfigdatacalledfromset'] = 'No s\'ha de configurar directament Configdata, en el seu lloc utilitzeu PluginBlocktype::instance_config_save';
$string['invaliddirection'] = 'Direcció no vàlida %s';
$string['onlyoneprofileviewallowed'] = 'Només teniu permisos per fer una Vista del perfil';
$string['onlyoneblocktypeperview'] = 'Només es pot posar un tipus de bloc %s en una vista.';

// if you change these next two , be sure to change them in libroot/errors.php
// as they are duplicated there, in the case that get_string was not available.
$string['unrecoverableerror'] = 'S\'ha produït un error irrecuperable.  Això significa que probablement heu trobat un bug al sistema.';
$string['unrecoverableerrortitle'] = '%s - No es pot trobar el lloc';
$string['parameterexception'] = 'No s\'ha trobat un paràmetre necessari';

$string['notfound'] = 'No s\'ha trobat';
$string['notfoundexception'] = 'No s\'ha trobat la pàgina que buscàveu';

$string['accessdeniedexception'] = 'No teniu accés a veure aquesta pàgina';
$string['accessdenied'] = 'Accés denegat';

$string['viewnotfoundexceptiontitle'] = 'No s\'ha trobat la vista';
$string['viewnotfoundexceptionmessage'] = 'Esteu provant d\'accedir a una vista que no existeix';
$string['viewnotfound'] = 'No s\'ha trobat la vista id %s';
$string['youcannotviewthisusersprofile'] = 'No podeu veure el perfil d\'aquest usuari';

$string['artefactnotfoundmaybedeleted'] = "No s'ha trobat l'artefacte amb id %s, potser ha estat esborrat prèviament";
$string['artefactnotfound'] = 'No s\'ha trobat l\'artefacte amb id %s';
$string['artefactnotinview'] = 'L\'artefacte %s no és a la Vista %s';
$string['artefactonlyviewableinview'] = 'Els artefactes d\'aquest tipus només es poden veure dins una Vista';
$string['notartefactowner'] = 'Aquest artefacte no us pertany';

$string['blockinstancednotfound'] = 'No s\'ha trobat la instància de bloc id %s';
$string['interactioninstancenotfound'] = 'No s\'ha trobat la instància d\'activitat amb id %s';

$string['invalidviewaction'] = 'Acció de control de vista no vàlida: %s';

$string['missingparamblocktype'] = 'Proveu primer a seleccionar el tipus de bloc per afegir';
$string['missingparamcolumn'] = 'No s\'ha trobat la columna d\'especificació';
$string['missingparamorder'] = 'No s\'ha trobat l\'ordre d\'especificació';
$string['missingparamid'] = 'No s\'ha trobat id';
?>
