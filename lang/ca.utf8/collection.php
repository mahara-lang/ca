<?php

/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */

defined('INTERNAL') || die();

$string['pluginname'] = 'Col·leccions';

$string['about'] = 'Sobre';
$string['access'] = 'Accés';
$string['accesscantbeused'] = 'No s\'ha desat el canvi en l\'accés. L\'accés de les Pàgines escollides (amb URL secreta) no es pot utilitzar en Pàgines múltiples.';
$string['accessoverride'] = 'Accés reemplaçat';
$string['accesssaved'] = 'S\'ha desat correctament l\'accés a la col·lecció.';
$string['accessignored'] = 'S\'han ignorat els accessos amb URL secreta.';
$string['add'] = 'Afegeix';
$string['addviews'] = 'Afegeix Pàgines';
$string['addviewstocollection'] = 'Afegeix Pàgines a la col·lecció';
$string['back'] = 'Enrere';
$string['cantdeletecollection'] = 'No podeu esborrar aquesta col·lecció.';
$string['canteditdontown'] = 'No podeu editar aquesta col·lecció perquè no us pertany.';
$string['collection'] = 'col·lecció';
$string['Collection'] = 'Col·lecció';
$string['collections'] = 'Col·leccions';
$string['Collections'] = 'Col·leccions';
$string['collectionaccess'] = 'Accés a la col·lecció';
$string['collectionaccesseditedsuccessfully'] = 'S\'ha desat correctament l\'accés a la col·lecció';
$string['collectioneditaccess'] = 'Esteu editant l\'accés de %d Pàgines d\'aquesta col·lecció';
$string['collectionconfirmdelete'] = 'No s\esborraran les Pàgines d\'aquesta col·lecció. Esteu segur que voleu esborrar aquesta col·lecció?';
$string['collectioncreatedsuccessfully'] = 'S\'ha creat correctament la col·lecció.';
$string['collectioncreatedsuccessfullyshare'] = 'S\'ha creat correctament la col·lecció. Compartiu-la amb altres utilitzant el següent enllaç:';
$string['collectiondeleted'] = 'S\'ha esborrat correctament la col·lecció.';
$string['collectiondescription'] = 'Una <strong>Col·lecció</strong> és un conjunt de <em>Pàgines</em> enllaçades les unes amb les altres que tenen els mateixos permisos d\'accés. Podeu crear tantes col·leccions com vulgueu, però una Pàgina només pot aparèixer en una sola col·lecció.';
$string['confirmcancelcreatingcollection'] = 'No s\'ha completat la col·lecció. Esteu segur que voleu cancel·lar?';
$string['collectionsaved'] = 'S\'ha desat correctament la col·lecció.';
$string['created'] = 'Creada';
$string['deletecollection'] = 'Esborra la col·lecció';
$string['deletespecifiedcollection'] = 'Esborra la col·lecció \'%s\'';
$string['deletingcollection'] = 'Esborrant la col·lecció';
$string['deleteview'] = 'Treu la Pàgina de la col·lecció';
$string['description'] = 'Descripció de la col·lecció';
$string['editcollection'] = 'Edita la col·lecció';
$string['editingcollection'] = 'Editant la col·lecció';
$string['edittitleanddesc'] = 'Edita el Títol i la Descripció';
$string['editviews'] = 'Edita les Pàgines de la col·lecció';
$string['editviewaccess'] = 'Edita l\'accés a les Pàgines';
$string['editaccess'] = 'Edita l\'accés a la col·lecció';
$string['emptycollectionnoeditaccess'] = 'No podeu editar l\'accés a una col·lecció buida. Afegiu-hi primer alguna Pàgina.';
$string['emptycollection'] = 'Col·lecció buida';
$string['manageviews'] = 'Gestiona les Pàgines';
$string['name'] = 'Nom de la col·lecció';
$string['newcollection'] = 'Col·lecció nova';
$string['nocollectionsaddone'] = 'Encara no teniu cap col·lecció. %sAfegiu-ne una%s!';
$string['nooverride'] = 'No reemplacis';
$string['noviewsavailable'] = 'No hi ha Pàgines disponibles per afegir.';
$string['noviewsaddsome'] = 'No hi ha Pàgines en aquesta col·lecció. %sAfegiu-ne alguna!%s';
$string['noviews'] = 'No hi ha Pàgines.';
$string['overrideaccess'] = 'Reemplaça l\'accés';
$string['potentialviews'] = 'Pàgines potencials';
$string['saveapply'] = 'Aplica i Desa';
$string['savecollection'] = 'Desa la col·lecció';
$string['update'] = 'Actualitza';
$string['usecollectionname'] = 'Voleu usar el nom de la col·lecció?';
$string['usecollectionnamedesc'] = 'Si voleu usar el nom de la col·lecció en comptes del Títol del bloc marqueu la casella.';
$string['viewaddedtocollection'] = 'S\'ha afegit la Pàgina a la col·lecció. S\'ha actualitzat la col·lecció per incloure l\'accés des de la pàgina nova.';
$string['viewcollection'] = 'Detalls de la col·lecció';
$string['viewcount'] = 'Pàgines';
$string['viewremovedsuccessfully'] = 'S\'ha esborrat correctament la pàgina.';
$string['viewnavigation'] = 'Barra de navegació a les pàgines';
$string['viewnavigationdesc'] = 'S\'afegirà una barra de navegació horitzontal a cada Pàgina de la col·lecció.';
$string['viewsaddedtocollection'] = 'S\'han afegit les Pàgines a la col·lecció. S\'ha actualitzat la col·lecció per incloure l\'accés des de les pàgines noves.';
$string['viewstobeadded'] = 'Pàgines per afegir';
$string['viewconfirmremove'] = 'Esteu segur que voleu esborrar aquesta Pàgina de la col·lecció?';
?>
