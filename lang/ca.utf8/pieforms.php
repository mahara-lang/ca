<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana: Joan Queralt Gil jqueralt a gmail.com
 */

defined('INTERNAL') || die();

$string['element.bytes.bytes'] = 'Bytes';
$string['element.bytes.kilobytes'] = 'Kilobytes';
$string['element.bytes.megabytes'] = 'Megabytes';
$string['element.bytes.gigabytes'] = 'Gigabytes';
$string['element.bytes.invalidvalue'] = 'El valor ha de ser un número';

$string['element.calendar.invalidvalue'] = 'La data o el temps especificats no són vàlids';

$string['element.date.or'] = 'o';
$string['element.date.monthnames'] = 'Gener,Febrer,Març,Abril,Maig,Juny,Juliol,Agost,Setembre,Octubre,Novembre,Desembre';
$string['element.date.notspecified'] = 'No especificat';

$string['element.expiry.days'] = 'Dies';
$string['element.expiry.weeks'] = 'Setmanes';
$string['element.expiry.months'] = 'Mesos';
$string['element.expiry.years'] = 'Anys';
$string['element.expiry.noenddate'] = 'No hi ha data límit';

$string['rule.before.before'] = 'Això no pot ser després del camp "%s"';

$string['rule.email.email'] = 'L\'adreça de correu no és vàlida';

$string['rule.integer.integer'] = 'El camp ha de contenir un valor enter';

$string['rule.maxlength.maxlength'] = 'Aquest camp ha de tenir com a molt %d caràcters';

$string['rule.minlength.minlength'] = 'Aquest camp ha de tenir com a mínim %d caràcters';

$string['rule.minvalue.minvalue'] = 'Aquest valor no pot ser menor que  %d';

$string['rule.regex.regex'] = 'Aquest camp no té un format vàlid';

$string['rule.required.required'] = 'Aquest camp és obligatori';

$string['rule.validateoptions.validateoptions'] = 'L\'opció  "%s" no és vàlida';

$string['rule.maxvalue.maxvalue'] = 'Aquest valor no pot ser més gran que  %d';
