<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage export.html
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana: Joan Queralt Gil jqueralt a gmail.com
 */

defined('INTERNAL') || die();

$string['title'] = 'Lloc web independent HTML';
$string['description'] = 'Crea un lloc web independent amb les dades del vostre eportafolis. No podreu tornar a importar-lo, però es pot visualitzar en qualsevol navegador web.';
$string['usersportfolio'] = '%s - eportafolis';

$string['preparing'] = 'S\'està preparant %s';
$string['exportingdatafor'] = 'S\'estan exportant dades de %s';
$string['buildingindexpage'] = 'S\'està la pàgina índex';
$string['copyingextrafiles'] = 'S\'estan copiant fitxers extra';

?>
