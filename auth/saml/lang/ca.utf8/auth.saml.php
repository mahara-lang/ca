<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage auth-internal
 * @author     Piers Harding <piers@catalyst.net.nz>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */

defined('INTERNAL') || die();

//$string['defaultidpidentity'] = 'Default IdP Identity Service';
$string['defaultinstitution'] = 'Institució per defecte';
$string['description'] = 'Autentiqueu-vos contra un servei SAML 2.0 IdP';
$string['errorbadinstitution'] = 'No s\'ha pogut resoldre la institució per connectar l\'usuari';
$string['errorretryexceeded'] = 'S\'ha excedit el nombre màxim d\'intents (%s) - Deu haver-hi un problema aml servei d\'autenticació';
$string['errnosamluser'] = 'No s\'ha trobat cap usuari';
$string['errorbadlib'] = 'El fitxer SimpleSAMLPHP lib del directori %s no és correcte.';
$string['errorbadconfig'] = 'El fitxer SimpleSAMLPHP config del directori %s no és correcte.';
$string['errorbadcombo'] = 'Només podeu triar creació automàtica d\'usuari si no heu seleccionat remoteuser';
$string['errormissinguserattributes'] = 'Sembla que us heu identificat però no hem rebut els atribut d\'usuari necessaris. Comproveu que el vostre Proveïdor d\'identitat envia els camps SSO pel Nom, el Cognom i l\'Email al servei proveïdor de Mahara o informeu a l\'administrador d\'aquest servei.';

//$string['idpidentity'] = 'IdP Identity Service';
$string['institutionattribute'] = 'Atribut se la Institució (conté "%s")';
$string['institutionvalue'] = 'Valor de la Institució a comprovar contra l\'atribut';
//fet fins aquí
$string['institutionregex'] = 'Fes coincidir una cadena parcial amb el nom curt de la institució';
$string['notusable'] = 'Instal·leu les biblioteques SimpleSAMLPHP SP ';
$string['samlfieldforemail'] = 'Camp SSO per Email';
$string['samlfieldforfirstname'] = 'Camp SSO per First Name';
$string['samlfieldforsurname'] = 'Camp SSO per Surname';
$string['title'] = 'SAML';
$string['updateuserinfoonlogin'] = 'Actualitza els detalls de l\'usuri en registrar-se';
$string['userattribute'] = 'Atribut de l\'usuari';
$string['simplesamlphplib'] = 'Directori SimpleSAMLPHP lib ';
$string['simplesamlphpconfig'] = 'Directori SimpleSAMLPHP config ';
$string['weautocreateusers'] = 'Es crearan els usuaris automàticment';
$string['remoteuser'] = 'Fes coincidir l\'atribut username amb l\'username remot';
?>
