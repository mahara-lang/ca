<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage auth-internal
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */

defined('INTERNAL') || die();

$string['title'] = 'LDAP';
$string['description'] = 'Us autentica contra un servidor LDAP';
$string['notusable'] = 'Cal que instal·leu l\'extensió de PHP LDAP';

$string['contexts'] = 'Contexts';
$string['distinguishedname'] = 'Nom distingit';
$string['hosturl'] = 'URL del Host ';
$string['ldapfieldforemail'] = 'Camp LDAP per l\'Email';
$string['ldapfieldforfirstname'] = 'Camp LDAP pel Nom';
$string['ldapfieldforsurname'] = 'Camp LDAP pel Cognom';
$string['ldapversion'] = 'Versió LDAP';
$string['password'] = 'Contrasenya';
$string['starttls'] = 'Encriptació TLS';
$string['searchsubcontexts'] = 'Cerca subcontexts';
$string['userattribute'] = 'Atribut de l\'usuari';
$string['usertype'] = 'Tipus d\'usuari';
$string['weautocreateusers'] = 'Es crearan els usuaris automàticament';
$string['updateuserinfoonlogin'] = 'Actualitza la informació de l\'usuari quan s\'identifiqui';
$string['cannotconnect'] = 'No es pot connectar a cap host LDAP';
$string['updateuserinfoonloginadnote'] = 'Nota: Si l\'activeu estalviareu posteriors identificacions a Mahara a altres llocs MS ActiveDirectory o usuaris.';


?>
