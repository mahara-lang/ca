<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage auth-internal
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */

defined('INTERNAL') || die();

$string['internal'] = 'Interna';
$string['title'] = 'Interna';
$string['description'] = 'Us autentica contra una base de dades de Mahara';

$string['completeregistration'] = 'Completeu el registre';
$string['emailalreadytaken'] = 'Aquesta adreça de correu ja ha estat registrada al lloc';
$string['iagreetothetermsandconditions'] = 'Estic d\'acord amb els Termes i Condicions';
$string['passwordformdescription'] = 'La contrasenya, com a mínim, ha de constar de sis caràcters i contenir, al menys, una xifra i dues lletres';
$string['passwordinvalidform'] = 'La vostra contrasenya, com a mínim, ha de constar de sis caràcters i contenir, al menys, una xifra i dues lletres';
$string['registeredemailsubject'] = 'Us heu registrat a %s';
$string['registeredemailmessagetext'] = 'Hola %s,

Gràcies per registrar-vos a %s. Si us plau, seguiu aquest enllaç per
completar el procés de registre:

%sregister.php?key=%s

--
Salutacions,
L\'equip de %s';
$string['registeredemailmessagehtml'] = '<p>Hola %s,</p>
<p>Gràcies per registrar-vos a %s. Si us plau, seguiu aquest enllaç per
completar el procés de registre:</p>
<p><a href="%sregister.php?key=%s">%sregister.php?key=%s</a></p>
<pre>--
Salutacions,
L\'equip de %s</pre>';
$string['registeredok'] = '<p>Us heu registrat correctament. Si us plau, comproveu el vostre correu electrònic per trobar instruccions sobre el procés d\'activació del vostre compte</p>';
$string['registrationnosuchkey'] = 'Sembla que no hi ha cap registre amb aquesta clau. Potser heu trigat més de 24 hores en completar el registre. En un altre cas es deu haver produït un error.';
$string['registrationunsuccessful'] = 'Lamentablement el vostre intent de registre ha fallat a causa d\'un error al nostre sistema. Si us plau, proveu-ho més tard.';
$string['usernamealreadytaken'] = 'Aquest nom d\'usuari ja està en ús.';
$string['usernameinvalidform'] = 'El nom d\'usuari només pot contenir caràcters alfanumèrics, punts, barres baixes i símbols d\'arrova. A més, ha de tenir una longitud d\'entre 3 i 30 caràcters.';
$string['usernameinvalidadminform'] = 'El nom d\'usuari pot contenir lletres, xifres i la majoria de símbols comuns i ha de tenir una longitud d\'entre 3 a 236 caràcters. No s\'hi poden posar espais en blanc.';
$string['youmaynotregisterwithouttandc'] = 'No us podeu registrar si no esteu d\'acord en acceptar els <a href="terms.php">Termes i Condicions</a>';


?>
