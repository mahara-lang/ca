<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage interaction-forum
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 * @traducció catalana Joan Queralt i Gil jqueralt a gmail punt com
 */


$string['addpostsuccess'] = 'S\'ha afegit correctament l\'entrada ';
$string['addtitle'] = 'Crea nou fòrum';
$string['addtopic'] = 'Afegeix un tema nou';
$string['addtopicsuccess'] = 'S\'ha afegit correctament el nou tema';
$string['autosubscribeusers'] = 'Voleu subscriure automàticament als usuaris?';
$string['autosubscribeusersdescription'] = 'Escolliu si els usuaris del grup quedaran subscrits automàticament a aquest fòrum';
$string['Body'] = 'Cos';
$string['cantaddposttoforum'] = 'No esteu autoritzat per escriure en aquest fòrum';
$string['cantaddposttotopic'] = 'No esteu autoritzat per escriure en aquest tema';
$string['cantaddtopic'] = 'No esteu autoritzat per afegir temes en aquest fòrum';
$string['cantdeletepost'] = 'No esteu autoritzat per esborrar entrades en aquest fòrum';
$string['cantdeletethispost'] = 'No esteu autoritzat per esborrar aquesta entrada';
$string['cantdeletetopic'] = 'No esteu autoritzat per esborrar temes en aquest fòrum';
$string['canteditpost'] = 'No esteu autoritzat per editar aquesta entrada';
$string['cantedittopic'] = 'No esteu autoritzat per editar aquest tema';
$string['cantfindforum'] = 'No s\'ha trobat el fòrum amb l\'id %s';
$string['cantfindpost'] = 'No s\'ha trobat l\'entrada amb l\'id %s';
$string['cantfindtopic'] = 'No s\'ha trobat el tema amb l\'id %s';
$string['cantviewforums'] = 'No esteu autoritzat per veure els fòrums d\'aquest grup';
$string['cantviewtopic'] = 'No esteu autoritzat per veure els temes d\'aquest fòrum';
$string['chooseanaction'] = 'Trieu una acció';
$string['clicksetsubject'] = 'Cliqueu per establir l\'assumpte';
$string['Closed'] = 'Tancat';
$string['Close'] = 'Tanca';
$string['closeddescription'] = 'Només els moderadors i el propietari del grup poden contestar els temes tancats';
$string['Count'] = 'Compta';
$string['createtopicusersdescription'] = 'Si trieu "Tots els membres del grup" qualsevol podrà crear temes nous i contestar als existents.  Si trieu "Moderadors i administradors del grup" només els moderadors i administradors del grup podran començar temes nous. Tanmateix, si el tema ja existeix tots els membres podran contestar-lo.';
$string['currentmoderators'] = 'Moderadors actuals';
$string['defaultforumtitle'] = 'Fòrum de discussió general';
$string['defaultforumdescription'] = '%s fòrum de discussió general';
$string['deleteforum'] = 'Esborra fòrum';
$string['deletepost'] = 'Esborra l\'entrada';
$string['deletepostsuccess'] = 'Entrada esborrada correctament';
$string['deletepostsure'] = 'Esteu segur que voleu esborrar l\'entrada? L\'acció no es podrà desfer';
$string['deletetopic'] = 'Esborra tema';
$string['deletetopicvariable'] = 'Esborra el tema \'%s\'';
$string['deletetopicsuccess'] = 'Tema esborrat correctament';
$string['deletetopicsure'] = 'Esteu segur que voleu esborrar el tema? L\'acció no es podrà desfer';
$string['editpost'] = 'Edita l\'entrada';
$string['editpostsuccess'] = 'S\'ha editat l\'entrada correctament';
$string['editstothispost'] = 'Edicions d\'aquesta entrada:';
$string['edittitle'] = 'Edita el fòrum';
$string['edittopic'] = 'Edita el tema';
$string['edittopicsuccess'] = 'S\'ha editat el tema correctament';
$string['forumname'] = 'Nom del fòrum';
$string['forumposthtmltemplate'] = "<div style=\"padding: 0.5em 0; border-bottom: 1px solid #999;\"><strong>%s per %s</strong><br>%s</div>

<div style=\"margin: 1em 0;\">%s</div>

<div style=\"font-size: smaller; border-top: 1px solid #999;\">
<p><a href=\"%s\">Respon en línia a aquesta entrada</a></p>
<p><a href=\"%s\">Anul·la la meva subscripció a %s</a></p>
</div>";
$string['forumposttemplate'] = "%s per %s
%s
------------------------------------------------------------------------

%s

------------------------------------------------------------------------
Per poder veure i contestar en línia a aquesta entrada seguiu aquest enllaç:
%s

Per anul·lar la vostra subscripció a %s, visiteu:
%s";
$string['forumsuccessfulsubscribe'] = 'Us heu subscrit al fòrum correctament';
$string['forumsuccessfulunsubscribe'] = 'Heu deixat d\'estar subscrit al fòrum correctament';
$string['gotoforums'] = 'Vés als fòrums';
$string['groupadmins'] = 'Administradors del grup';
$string['groupadminlist'] = 'Administradors del grup:';
$string['Key'] = 'Clau';
$string['lastpost'] = 'Darrera entrada';
$string['latestforumposts'] = 'Darreres entrades del fòrum';
$string['Moderators'] = 'Moderadors';
$string['moderatorsandgroupadminsonly'] = 'Només moderadors i administradors de grups';
$string['moderatorslist'] = 'Moderadors:';
$string['moderatorsdescription'] = 'Els moderadors poden editar i esborrar temes i entrades. També poden obrir, tancar i fixar temes com enganxosos.';
$string['name'] = 'Fòrum';
$string['nameplural'] = 'Fòrums';
$string['newforum'] = 'Crea nou fòrum';
$string['newforumpostnotificationsubject'] = '%s: %s: %s';
$string['newpost'] = 'Crea nova entrada: ';
$string['newtopic'] = 'Crea nou tema';
$string['noforumpostsyet'] = 'Encara no hi ha cap entrada en aquest grup';
$string['noforums'] = 'No hi ha fòrums en aquest grup';
$string['notopics'] = 'No hi ha temes en aquest fòrum';
$string['Open'] = 'Obre';
$string['Order'] = 'Ordena';
$string['orderdescription'] = 'Trieu on voleu que vagi el fòrum en relació amb altres fòrums';
$string['Post'] = 'Envia l\'entrada';
$string['postaftertimeout'] = 'Heu fet el  canvi un cop superat el temps d\'espera de % s minuts. El canvi no s\'ha pogut aplicar.';
$string['postbyuserwasdeleted'] = 'S\'ha esborrat l\'entrada de %s';
$string['postdelay'] = 'Retard de l\'entrada';
$string['postdelaydescription'] = 'El temps mínim en minuts que ha de passar abans que s\enviï un correu amb una entrada nova als subscriptors del fòrum.  L\'autor d\'una entrada encara la podrà editar durant aquest temps.';
$string['postedin'] = '%s ha fet una entrada a %s';
$string['Poster'] = 'Autor de l\'entrada';
$string['postreply'] = 'Resposta a l\'entrada';
$string['Posts'] = 'Entrades';
$string['allposts'] = 'Totes les entrades';
$string['postsvariable'] = 'Entrades: %s';
$string['potentialmoderators'] = 'Moderadors en potència';
$string['re'] ='Re: %s';
$string['regulartopics'] = 'Temes regulars';
$string['Reply'] = 'Respon';
$string['replyforumpostnotificationsubject'] = 'Re: %s: %s: %s';
$string['replyto'] = 'Respon a: ';
$string['Sticky'] = 'Enganxós';
$string['stickydescription'] = 'Els temes enganxosos es mostren sempre al capdamunt de la pàgina';
$string['stickytopics'] = 'Temes enganxosos';
$string['Subscribe'] = 'Subscriu-me';
$string['Subscribed'] = 'Subscrit';
$string['subscribetoforum'] = 'Subscriu-me al fòrum';
$string['subscribetotopic'] = 'Subscriu-me al tema';
$string['Subject'] = 'Assumpte';
$string['Topic'] = 'Tema';
$string['Topics'] = 'Temes';
$string['topiclower'] = 'tema';
$string['topicslower'] = 'temes';
$string['topicclosedsuccess'] = 'S\'han tancat correctament els temes';
$string['topicisclosed'] = 'Aquest tema està tancat. Només els moderadors i el propietari del grup poden donar noves respostes.';
$string['topicopenedsuccess'] = 'S\'han obert correctament els temes';
$string['topicstickysuccess'] = 'S\'han fixat correctament els temes com enganxosos';
$string['topicsubscribesuccess'] = 'Us heu subscrit correctament als temes';
$string['topicsuccessfulunsubscribe'] = 'S\'ha anul·lat satisfactòriament la vostra subscripció al tema.';
$string['topicunstickysuccess'] = 'S\'ha fixat correctament el tem com a no enganxós';
$string['topicunsubscribesuccess'] = 'Heu deixat d\'estar subscrit als temes correctament';
$string['topicupdatefailed'] = 'No s\'han pogut actualitzar els temes';
$string['typenewpost'] = 'Crea una nova entrada al fòrum';
$string['Unsticky'] = 'No enganxosos';
$string['Unsubscribe'] = 'Anul·la la subscripció';
$string['unsubscribefromforum'] = 'Anul·la la subscripció per al fòrum';
$string['unsubscribefromtopic'] = 'Anul·la la subscripció per al tema';
$string['updateselectedtopics'] = 'Actualitza els temes seleccionats';
$string['whocancreatetopics'] = 'Qui pot crear temes nous';
$string['youcannotunsubscribeotherusers'] = 'No podeu anul·lar la subscripció dels altres usuaris';
$string['youarenotsubscribedtothisforum'] = 'No esteu subscrit a aquest fòrum.';
$string['youarenotsubscribedtothistopic'] = 'No esteu subscrit a aquest tema.';

$string['today'] = 'Avui';
$string['yesterday'] = 'Ahir';
$string['strftimerecentrelative'] = '%%v, %%k:%%M';
$string['strftimerecentfullrelative'] = '%%v, %%l:%%M %%p';

$string['indentmode'] = 'Fòrum esglaonat';
$string['indentfullindent'] = 'Completament expandit';
$string['indentmaxindent'] = 'Expandeix al màxim';
$string['indentflatindent'] = 'No esglaonis';
$string['indentmodedescription'] = 'Especifiqueu quins temes d\'aquest fòrum voleu veure esglaonats.';
$string['maxindent'] = 'Nivell màxim d\'esglaonat';
$string['maxindentdescription'] = 'Configureu el nivell màxim d\'esglaonat per un tema. Només tindrà efecte si l\'opció <em>Fòrum esglaonat</em> s\'ha deixat a <b>Expandeix al màxim</b>';

$string['closetopics'] = 'Tanca temes nous';
$string['closetopicsdescription'] = 'Si marqueu, per defecte es tancaran tots els temes nous d\'aquest fòrum. Només els moderadors i els administradors del grup podran respondre als temes tancats.';

$string['activetopicsdescription'] = 'Temes actualitzats darrerament als vostres grups.';

$string['timeleftnotice'] = 'Us queden  %s minuts per acabar d\'editar.';

?>
